#!/bin/bash

input_file="sources.csv"
dest_dir="../../"
while IFS="," read -r title format url
do
    filename=$(echo "${title}" | tr ' ' '_' | tr '[:upper:]' '[:lower:]' )
    wget -q -r --tries=10 "${url}" -O "${dest_dir}/${filename}.${format}"
    #adds source as 1st line of the downloaded file
    sed -i "1 i\Taken from: ${url}\n" "${dest_dir}/${filename}.${format}"
done < "${input_file}"