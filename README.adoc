= CHM - the Knowledge repository

This is where we build our knowledge - we store here any document or media that can be useful to learn something.

If you have personal notes about anything relevant to finance and crypto or to the technologies around them, please share with us! We are greatful.

== How to use this repository (quickly)

Simply click on any file listed about and you'll have access to the file content in your browser.

== How to use this repository (at best)

Especially when you plan to upload to this repository, this is the best way:

. Install link:https://vscodium.com/#install[VSCodium] IDE (free and open source) on your pc
. Clone this repository on your pc. Open a terminal and:
+
[source,bash]
----
cd <anywhere you want>
git clone https://gitlab.com/crypto-hub-malta/knowledge.git
----
+
. Move into the directory
+
[source,bash]
----
cd knowledge
----
+
. Launch Codium:
+
[source,bash]
----
codium .
----


== How to upload your files

The steps are the usual _"GIT steps"_, like in github.com. Detailed  link:https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork[instructions].


Anyway, if you don't feel comfortable with git, send your file(s) to **damko**

WARNING: all files must be in either link:https://docs.asciidoctor.org/asciidoc/latest/:[asciidoc] (preferable) or link:https://www.markdownguide.org/:[markdown] format
