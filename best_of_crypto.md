Taken from: https://raw.githubusercontent.com/LukasMasuch/best-of-crypto/main/README.md

<!-- markdownlint-disable -->
<h1 align="center">
    Best-of Crypto
    <br>
</h1>

<p align="center">
    <strong>🏆&nbsp; A ranked list of open-source digital currency and blockchain projects. Updated weekly.</strong>
</p>

<p align="center">
    <a href="https://best-of.org" title="Best-of Badge"><img src="http://bit.ly/3o3EHNN"></a>
    <a href="#Contents" title="Project Count"><img src="https://img.shields.io/badge/projects-3.2K-blue.svg?color=5ac4bf"></a>
    <a href="#Contribution" title="Contributions are welcome"><img src="https://img.shields.io/badge/contributions-welcome-green.svg"></a>
    <a href="https://github.com/LukasMasuch/best-of-crypto/releases" title="Best-of Updates"><img src="https://img.shields.io/github/release-date/LukasMasuch/best-of-crypto?color=green&label=updated"></a>
</p>

This curated list contains 3.2K awesome open-source projects with a total of 1M stars grouped into 11 categories. All projects are ranked by a project-quality score, which is calculated based on various metrics automatically collected from GitHub and different package managers. If you like to add or update projects, feel free to open an [issue](https://github.com/LukasMasuch/best-of-crypto/issues/new/choose), submit a [pull request](https://github.com/LukasMasuch/best-of-crypto/pulls), or directly edit the [projects.yaml](https://github.com/LukasMasuch/best-of-crypto/edit/main/projects.yaml). Contributions are very welcome!

> 🧙‍♂️  Discover other [best-of lists](https://best-of.org) or [create your own](https://github.com/best-of-lists/best-of/blob/main/create-best-of-list.md).

## Contents

- [Cryptocurrencies](#cryptocurrencies) _48 projects_
- [Smart Contract Platforms](#smart-contract-platforms) _62 projects_
- [Stablecoins](#stablecoins) _3 projects_
- [Decentralized Finance (DeFi)](#decentralized-finance-defi) _120 projects_
- [Exchange-based Tokens](#exchange-based-tokens) _23 projects_
- [Meme Tokens](#meme-tokens) _1 projects_
- [Web 3.0](#web-30) _62 projects_
- [Privacy Coins](#privacy-coins) _42 projects_
- [Automated Market Maker (AMM)](#automated-market-maker-amm) _18 projects_
- [Metaverse, NFTs & Gaming](#metaverse-nfts--gaming) _27 projects_
- [Top Internet of Things (IOT)](#top-internet-of-things-iot) _13 projects_
- [Others](#others) _448 projects_

## Explanation
- 🥇🥈🥉&nbsp; Combined project-quality score
- ⭐️&nbsp; Star count from GitHub
- 🐣&nbsp; New project _(less than 6 months old)_
- 💤&nbsp; Inactive project _(6 months no activity)_
- 💀&nbsp; Dead project _(12 months no activity)_
- 📈📉&nbsp; Project is trending up or down
- ➕&nbsp; Project was recently added
- 👨‍💻&nbsp; Contributors count from GitHub
- 🔀&nbsp; Fork count from GitHub
- 📋&nbsp; Issue count from GitHub
- ⏱️&nbsp; Last update timestamp on package manager
- 📥&nbsp; Download count from package manager
- 📦&nbsp; Number of dependent projects
- <img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13">&nbsp; Deployed on Ethereum Mainnet.
- <img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13">&nbsp; Deployed on BSC Mainnet.
- <img src="https://git.io/J9cOa" style="display:inline;" width="13" height="13">&nbsp; Deployed on Solana Mainnet.
- <img src="https://git.io/J9cOb" style="display:inline;" width="13" height="13">&nbsp; Deployed on Avalanche Mainnet.
- <img src="https://git.io/J9cOx" style="display:inline;" width="13" height="13">&nbsp; Deployed on Polygon Mainnet.
- <img src="https://git.io/J9cOh" style="display:inline;" width="13" height="13">&nbsp; Deployed on Arbitrum Mainnet.
- <img src="https://git.io/J9cOi" style="display:inline;" width="13" height="13">&nbsp; Deployed on Fantom Mainnet.
- <img src="https://git.io/J9cOK" style="display:inline;" width="13" height="13">&nbsp; Deployed on Harmony Mainnet.
- <img src="https://git.io/J9c3v" style="display:inline;" width="13" height="13">&nbsp; Deployed on Polkadot Mainnet.
- <img src="https://git.io/J9cOg" style="display:inline;" width="13" height="13">&nbsp; Deployed on Terra Mainnet.
- <img src="https://cosmos.network/favicon.ico" style="display:inline;" width="13" height="13">&nbsp; Deployed on Cosmos Mainnet.

<br>

## Cryptocurrencies

<a href="#contents"><img align="right" width="15" height="15" src="https://git.io/JtehR" alt="Back to top"></a>

_Decentralized peer-to-peer currencies._

<details><summary><b><a href="https://github.com/bitcoinj">BTC - Bitcoin</a></b> (🥇46 ·  ⭐ 100K) - A Peer-to-Peer Electronic Cash System.</summary>


---
<details><summary><b><a href="https://github.com/bitcoin/bitcoin">bitcoin</a></b> (🥇34 ·  ⭐ 68K) - Bitcoin Core integration/staging tree. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/bitcoin/bitcoin) (👨‍💻 1.1K · 🔀 33K · 📋 7.7K - 10% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/bitcoin/bitcoin
	```
</details>
<details><summary><b><a href="https://github.com/btcsuite/btcd">btcd</a></b> (🥇34 ·  ⭐ 5.2K) - An alternative full node bitcoin implementation written in Go (golang). <code><a href="http://bit.ly/3hkKRql">ISC</a></code></summary>

- [GitHub](https://github.com/btcsuite/btcd) (👨‍💻 180 · 🔀 1.8K · 📥 11K · 📦 27K · 📋 780 - 29% open · ⏱️ 02.02.2023):

	```
	git clone https://github.com/btcsuite/btcd
	```
</details>
<details><summary><b><a href="https://github.com/ElementsProject/lightning">lightning</a></b> (🥇30 ·  ⭐ 2.5K) - Core Lightning Lightning Network implementation focusing on.. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/ElementsProject/lightning) (👨‍💻 230 · 🔀 790 · 📥 31K · 📦 6 · 📋 2.4K - 18% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/ElementsProject/lightning
	```
</details>
<details><summary><b><a href="https://github.com/bitcoinjs/bitcoinjs-lib">bitcoinjs-lib</a></b> (🥇29 ·  ⭐ 4.8K) - A javascript Bitcoin library for node.js and browsers. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/bitcoinjs/bitcoinjs-lib) (👨‍💻 95 · 🔀 1.9K · 📦 15K · 📋 1K - 4% open · ⏱️ 03.02.2023):

	```
	git clone https://github.com/bitcoinjs/bitcoinjs-lib
	```
</details>
<details><summary><b><a href="https://github.com/ElementsProject/elements">elements</a></b> (🥇25 ·  ⭐ 920) - Open Source implementation of advanced blockchain features extending the.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/ElementsProject/elements) (👨‍💻 1K · 🔀 330 · 📥 36K · 📋 420 - 35% open · ⏱️ 01.02.2023):

	```
	git clone https://github.com/ElementsProject/elements
	```
</details>
<details><summary><b><a href="https://github.com/btcsuite/btcwallet">btcwallet</a></b> (🥇24 ·  ⭐ 920) - A secure bitcoin wallet daemon written in Go (golang). <code><a href="http://bit.ly/3hkKRql">ISC</a></code></summary>

- [GitHub](https://github.com/btcsuite/btcwallet) (👨‍💻 56 · 🔀 460 · 📥 710 · 📦 550 · 📋 330 - 36% open · ⏱️ 06.02.2023):

	```
	git clone https://github.com/btcsuite/btcwallet
	```
</details>
<details><summary><b><a href="https://github.com/bitcoinjs/bip39">bip39</a></b> (🥇23 ·  ⭐ 920 · 💤) - JavaScript implementation of Bitcoin BIP39: Mnemonic code for generating.. <code><a href="http://bit.ly/3hkKRql">ISC</a></code></summary>

- [GitHub](https://github.com/bitcoinjs/bip39) (👨‍💻 18 · 🔀 420 · 📦 130K · 📋 90 - 14% open · ⏱️ 27.04.2022):

	```
	git clone https://github.com/bitcoinjs/bip39
	```
</details>
<details><summary><b><a href="https://github.com/bitcoinj/bitcoinj">bitcoinj</a></b> (🥇22 ·  ⭐ 4.5K) - A library for working with Bitcoin. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/bitcoinj/bitcoinj) (👨‍💻 140 · 🔀 2.4K · 📥 1.8K · 📋 1.2K - 32% open · ⏱️ 02.02.2023):

	```
	git clone https://github.com/bitcoinj/bitcoinj
	```
</details>
<details><summary><b><a href="https://github.com/bitcoin/bips">bips</a></b> (🥈19 ·  ⭐ 7.7K) - Bitcoin Improvement Proposals. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/bitcoin/bips) (👨‍💻 340 · 🔀 4.9K · ⏱️ 03.02.2023):

	```
	git clone https://github.com/bitcoin/bips
	```
</details>
<details><summary><b><a href="https://github.com/ElementsProject/libwally-core">libwally-core</a></b> (🥈19 ·  ⭐ 230) - Useful primitives for wallets. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/ElementsProject/libwally-core) (👨‍💻 42 · 🔀 130 · 📥 45K · 📋 110 - 26% open · ⏱️ 29.01.2023):

	```
	git clone https://github.com/ElementsProject/libwally-core
	```
</details>
<details><summary><b><a href="https://github.com/ElementsProject/secp256k1-zkp">secp256k1-zkp</a></b> (🥈16 ·  ⭐ 290) - Experimental fork of libsecp256k1 with support for pedersen.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/ElementsProject/secp256k1-zkp) (👨‍💻 97 · 🔀 99 · 📋 47 - 38% open · ⏱️ 23.01.2023):

	```
	git clone https://github.com/ElementsProject/secp256k1-zkp
	```
</details>
<details><summary><b><a href="https://github.com/ElementsProject/simplicity">simplicity</a></b> (🥈15 ·  ⭐ 240) - Simplicity is a blockchain programming language designed as an.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/ElementsProject/simplicity) (👨‍💻 6 · 🔀 31 · 📋 34 - 38% open · ⏱️ 17.01.2023):

	```
	git clone https://github.com/ElementsProject/simplicity
	```
</details>

<br>

 _29 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/cosmos">ATOM - Cosmos</a></b> (🥇43 ·  ⭐ 20K) - The Cosmos network consists of many independent, parallel.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/cosmos/cosmos-sdk">cosmos-sdk</a></b> (🥇38 ·  ⭐ 4.9K) - A Framework for Building High Value Public Blockchains. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/cosmos/cosmos-sdk) (👨‍💻 420 · 🔀 2K · 📥 9.5K · 📦 7.6K · 📋 5.6K - 9% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/cosmos/cosmos-sdk
	```
</details>
<details><summary><b><a href="https://github.com/tendermint/tendermint">tendermint</a></b> (🥇36 ·  ⭐ 5.4K) - Tendermint Core (BFT Consensus) in Go. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/tendermint/tendermint) (👨‍💻 260 · 🔀 1.9K · 📥 87K · 📦 9.8K · 📋 3.6K - 18% open · ⏱️ 02.02.2023):

	```
	git clone https://github.com/tendermint/tendermint
	```
</details>
<details><summary><b><a href="https://github.com/CosmWasm/cosmwasm">cosmwasm</a></b> (🥇28 ·  ⭐ 810) - Framework for building smart contracts in Wasm for the Cosmos SDK. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/CosmWasm/cosmwasm) (👨‍💻 37 · 🔀 230 · 📥 2.7K · 📦 87 · 📋 620 - 13% open · ⏱️ 30.01.2023):

	```
	git clone https://github.com/CosmWasm/cosmwasm
	```
</details>
<details><summary><b><a href="https://github.com/cosmos/ibc-go">ibc-go</a></b> (🥇28 ·  ⭐ 330) - Interblockchain Communication Protocol (IBC) implementation in Golang. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/cosmos/ibc-go) (👨‍💻 62 · 🔀 300 · 📥 550 · 📦 1.9K · 📋 970 - 24% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/cosmos/ibc-go
	```
</details>
<details><summary><b><a href="https://github.com/CosmWasm/cw-plus">cw-plus</a></b> (🥇27 ·  ⭐ 430) - Production Quality contracts under open source licenses. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/CosmWasm/cw-plus) (👨‍💻 47 · 🔀 280 · 📥 24K · 📦 460 · 📋 300 - 4% open · ⏱️ 16.01.2023):

	```
	git clone https://github.com/CosmWasm/cw-plus
	```
</details>
<details><summary><b><a href="https://github.com/cosmos/iavl">iavl</a></b> (🥇27 ·  ⭐ 350) - Merkleized IAVL+ Tree implementation in Go. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/cosmos/iavl) (👨‍💻 60 · 🔀 200 · 📦 6K · 📋 150 - 29% open · ⏱️ 02.02.2023):

	```
	git clone https://github.com/cosmos/iavl
	```
</details>
<details><summary><b><a href="https://github.com/cosmos/gaia">gaia</a></b> (🥇26 ·  ⭐ 290) - Cosmos Hub. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/cosmos/gaia) (👨‍💻 92 · 🔀 590 · 📥 67K · 📦 5 · 📋 680 - 13% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/cosmos/gaia
	```
</details>
<details><summary><b><a href="https://github.com/cosmos/cosmjs">cosmjs</a></b> (🥇25 ·  ⭐ 460) - The Swiss Army knife to power JavaScript based client solutions.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/cosmos/cosmjs) (👨‍💻 38 · 🔀 220 · 📦 4.2K · 📋 570 - 19% open · ⏱️ 31.01.2023):

	```
	git clone https://github.com/cosmos/cosmjs
	```
</details>
<details><summary><b><a href="https://github.com/cosmos/relayer">relayer</a></b> (🥇25 ·  ⭐ 280) - An IBC relayer for ibc-go. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/cosmos/relayer) (👨‍💻 110 · 🔀 1.6K · 📥 2.8K · 📦 82 · 📋 360 - 11% open · ⏱️ 02.02.2023):

	```
	git clone https://github.com/cosmos/relayer
	```
</details>
<details><summary><b><a href="https://github.com/CosmWasm/wasmd">wasmd</a></b> (🥇24 ·  ⭐ 280) - Basic cosmos-sdk app with web assembly smart contracts. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/CosmWasm/wasmd) (👨‍💻 66 · 🔀 250 · 📦 740 · 📋 410 - 10% open · ⏱️ 30.01.2023):

	```
	git clone https://github.com/CosmWasm/wasmd
	```
</details>
<details><summary><b><a href="https://github.com/cosmos/sdk-tutorials">sdk-tutorials</a></b> (🥈20 ·  ⭐ 420) - Tutorials for building modules for the Cosmos SDK. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/cosmos/sdk-tutorials) (👨‍💻 120 · 🔀 360 · 📦 62 · 📋 210 - 30% open · ⏱️ 26.01.2023):

	```
	git clone https://github.com/cosmos/sdk-tutorials
	```
</details>
<details><summary><b><a href="https://github.com/cosmos/cosmos-rust">cosmos-rust</a></b> (🥈19 ·  ⭐ 210) - The home of all shared Rust resources for the Cosmos ecosystem. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/cosmos/cosmos-rust) (👨‍💻 26 · 🔀 73 · 📦 460 · 📋 47 - 21% open · ⏱️ 06.01.2023):

	```
	git clone https://github.com/cosmos/cosmos-rust
	```
</details>
<details><summary><b><a href="https://github.com/cosmos/ibc">ibc</a></b> (🥈18 ·  ⭐ 730) - Interchain Standards (ICS) for the Cosmos network & interchain ecosystem. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/cosmos/ibc) (👨‍💻 29 · 🔀 220 · 📋 460 - 17% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/cosmos/ibc
	```
</details>
<details><summary><b><a href="https://github.com/cosmos/chain-registry">chain-registry</a></b> (🥈18 ·  ⭐ 300) -  <code>Unlicensed</code></summary>

- [GitHub](https://github.com/cosmos/chain-registry) (👨‍💻 300 · 🔀 470 · 📋 130 - 42% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/cosmos/chain-registry
	```
</details>
<details><summary><b><a href="https://github.com/CosmWasm/cw-template">cw-template</a></b> (🥈15 ·  ⭐ 210) - Quickstart template to get started writing your own cosmwasm.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/CosmWasm/cw-template) (👨‍💻 25 · 🔀 120 · 📋 45 - 6% open · ⏱️ 13.01.2023):

	```
	git clone https://github.com/CosmWasm/cw-template
	```
</details>

<br>

 _38 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/stellar">XLM - Stellar</a></b> (🥇37 ·  ⭐ 8.3K) - Open source, distributed, and community owned network used to..</summary>


---
<details><summary><b><a href="https://github.com/stellar/go">go</a></b> (🥇31 ·  ⭐ 1.2K) - Stellars public monorepo of go code. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/stellar/go) (👨‍💻 120 · 🔀 490 · 📥 160K · 📦 340 · 📋 1.8K - 21% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/stellar/go
	```
</details>
<details><summary><b><a href="https://github.com/stellar/stellar-core">stellar-core</a></b> (🥇25 ·  ⭐ 3K) - stellar-core is the reference implementation for the peer.. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/stellar/stellar-core) (👨‍💻 110 · 🔀 1K · 📋 1.4K - 17% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/stellar/stellar-core
	```
</details>
<details><summary><b><a href="https://github.com/stellar/js-stellar-sdk">js-stellar-sdk</a></b> (🥇24 ·  ⭐ 570) - Main Stellar client library for the Javascript language. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/stellar/js-stellar-sdk) (👨‍💻 71 · 🔀 290 · 📦 2.9K · 📋 340 - 16% open · ⏱️ 13.12.2022):

	```
	git clone https://github.com/stellar/js-stellar-sdk
	```
</details>
<details><summary><b><a href="https://github.com/stellar/stellar-protocol">stellar-protocol</a></b> (🥈17 ·  ⭐ 460) - Developer discussion about possible changes to the.. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/stellar/stellar-protocol) (👨‍💻 91 · 🔀 270 · 📋 380 - 19% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/stellar/stellar-protocol
	```
</details>

<br>

 _20 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/algorand">ALGO - Algorand</a></b> (🥇35 ·  ⭐ 2.8K) - Scalable, secure and decentralized digital currency and transactions..</summary>


---
<details><summary><b><a href="https://github.com/algorand/go-algorand">go-algorand</a></b> (🥇30 ·  ⭐ 1.2K) - Algorands official implementation in Go. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/algorand/go-algorand) (👨‍💻 93 · 🔀 370 · 📥 36K · 📦 72 · 📋 1.5K - 23% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/algorand/go-algorand
	```
</details>
<details><summary><b><a href="https://github.com/algorand/js-algorand-sdk">js-algorand-sdk</a></b> (🥇27 ·  ⭐ 260) - The official JavaScript SDK for Algorand. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/algorand/js-algorand-sdk) (👨‍💻 59 · 🔀 180 · 📦 6.4K · 📋 310 - 20% open · ⏱️ 27.01.2023):

	```
	git clone https://github.com/algorand/js-algorand-sdk
	```
</details>
<details><summary><b><a href="https://github.com/algorand/pyteal">pyteal</a></b> (🥇22 ·  ⭐ 250) - Algorand Smart Contracts in Python. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/algorand/pyteal) (👨‍💻 37 · 🔀 110 · 📦 530 · 📋 220 - 36% open · ⏱️ 01.02.2023):

	```
	git clone https://github.com/algorand/pyteal
	```
</details>
<details><summary><b><a href="https://github.com/algorand/py-algorand-sdk">py-algorand-sdk</a></b> (🥇22 ·  ⭐ 230) - Algorand Python SDK. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/algorand/py-algorand-sdk) (👨‍💻 43 · 🔀 120 · 📦 770 · 📋 160 - 20% open · ⏱️ 31.01.2023):

	```
	git clone https://github.com/algorand/py-algorand-sdk
	```
</details>

<br>

 _8 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/ripple">XRP</a></b> (🥇34 ·  ⭐ 6.6K) - The Best Digital Asset for Global Payments. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/XRPLF/xrpl.js">xrpl.js</a></b> (🥇29 ·  ⭐ 1.1K) - A JavaScript/TypeScript API for interacting with the XRP Ledger in.. <code><a href="http://bit.ly/3hkKRql">ISC</a></code></summary>

- [GitHub](https://github.com/XRPLF/xrpl.js) (👨‍💻 130 · 🔀 470 · 📥 11K · 📦 2.4K · 📋 520 - 12% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/XRPLF/xrpl.js
	```
</details>
<details><summary><b><a href="https://github.com/XRPLF/rippled">rippled</a></b> (🥇26 ·  ⭐ 4.3K) - Decentralized cryptocurrency blockchain daemon implementing the XRP.. <code><a href="http://bit.ly/3hkKRql">ISC</a></code></summary>

- [GitHub](https://github.com/XRPLF/rippled) (👨‍💻 120 · 🔀 1.4K · 📋 1.2K - 32% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/XRPLF/rippled
	```
</details>
<details><summary><b><a href="https://github.com/XRPLF/xrpl-dev-portal">xrpl-dev-portal</a></b> (🥈20 ·  ⭐ 400) - Source code for xrpl.org including developer.. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/XRPLF/xrpl-dev-portal) (👨‍💻 140 · 🔀 950 · 📋 360 - 38% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/XRPLF/xrpl-dev-portal
	```
</details>

<br>

 _10 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/deso-protocol">DESO - Decentralized Social</a></b> (🥈29 ·  ⭐ 1.2K) - Decentralized Social (DESO) is a cryptocurrency ...</summary>


---
<details><summary><b><a href="https://github.com/deso-protocol/core">deso-protocol/core</a></b> (🥈21 ·  ⭐ 300) - DeSo core node. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/deso-protocol/core) (👨‍💻 32 · 🔀 98 · 📦 22 · 📋 85 - 31% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/deso-protocol/core
	```
</details>
<details><summary><b><a href="https://github.com/deso-protocol/frontend">frontend</a></b> (🥈17 ·  ⭐ 260) - DeSo frontend. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/deso-protocol/frontend) (👨‍💻 46 · 🔀 130 · 📋 120 - 28% open · ⏱️ 13.12.2022):

	```
	git clone https://github.com/deso-protocol/frontend
	```
</details>

<br>

 _8 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/nanocurrency">XNO - Nano</a></b> (🥈28 ·  ⭐ 3.7K) - Nano, a low-latency cryptocurrency built on an innovative block-lattice..</summary>


---
<details><summary><b><a href="https://github.com/nanocurrency/nano-node">nano-node</a></b> (🥇27 ·  ⭐ 3.4K) - Nano is digital money. <code><a href="http://bit.ly/3aKzpTv">BSD-3</a></code></summary>

- [GitHub](https://github.com/nanocurrency/nano-node) (👨‍💻 92 · 🔀 780 · 📥 430K · 📋 1.2K - 24% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/nanocurrency/nano-node
	```
</details>

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/dogecoin">DOGE - Dogecoin</a></b> (🥈27 ·  ⭐ 15K · 💤) - Cryptocurrency based on the popular Doge Internet meme and..</summary>


---
<details><summary><b><a href="https://github.com/dogecoin/dogecoin">dogecoin</a></b> (🥇31 ·  ⭐ 14K · 💤) - very currency. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/dogecoin/dogecoin) (👨‍💻 640 · 🔀 2.6K · 📥 8.7M · 📋 1.2K - 14% open · ⏱️ 18.07.2022):

	```
	git clone https://github.com/dogecoin/dogecoin
	```
</details>

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/steemit">STEEM</a></b> (🥈25 ·  ⭐ 1.2K) - Cryptocurrency that rewards users for community building by posting and upvoting..</summary>


---
<details><summary><b><a href="https://github.com/steemit/condenser">condenser</a></b> (🥈20 ·  ⭐ 520) - The greatest application front-end to the Steem Blockchain. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/steemit/condenser) (👨‍💻 120 · 🔀 420 · 📋 2K - 12% open · ⏱️ 29.12.2022):

	```
	git clone https://github.com/steemit/condenser
	```
</details>

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/RavenProject">RVN - Ravencoin</a></b> (🥈24 ·  ⭐ 1.1K) - Blockchain specifically dedicated to the creation and peer-to-peer..</summary>


---
<details><summary><b><a href="https://github.com/RavenProject/Ravencoin">Ravencoin</a></b> (🥇27 ·  ⭐ 1K) - Ravencoin Core integration/staging tree. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/RavenProject/Ravencoin) (👨‍💻 670 · 🔀 630 · 📥 760K · 📋 500 - 31% open · ⏱️ 01.01.2023):

	```
	git clone https://github.com/RavenProject/Ravencoin
	```
</details>

<br>

 _1 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/ArkEcosystem">ARK</a></b> (🥈24 ·  ⭐ 760) - ARK provides users, developers, and startups with innovative blockchain..</summary>


---
<details><summary><b><a href="https://github.com/ArkEcosystem/core">ArkEcosystem/core</a></b> (🥈21 ·  ⭐ 330) - The ARK Core Blockchain Framework. Check.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/ArkEcosystem/core) (👨‍💻 54 · 🔀 300 · 📋 990 - 0% open · ⏱️ 16.11.2022):

	```
	git clone https://github.com/ArkEcosystem/core
	```
</details>

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/peercoin">PPC - Peercoin</a></b> (🥈24 ·  ⭐ 550) - The Peercoin network activated in 2012 and is one of the first.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/peercoin/peercoin">peercoin</a></b> (🥇27 ·  ⭐ 550) - Reference implementation of the Peercoin protocol. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/peercoin/peercoin) (👨‍💻 1.1K · 🔀 560 · 📥 91K · 📋 230 - 7% open · ⏱️ 30.01.2023):

	```
	git clone https://github.com/peercoin/peercoin
	```
</details>

---
</details>
<details><summary><b><a href="https://github.com/fetchai">FET - Fetch.ai</a></b> (🥈24 ·  ⭐ 380) - Fetch.ai is delivering AI to the crypto economy. Digital Twins,.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/bananocoin">BAN - Banano</a></b> (🥈23 ·  ⭐ 540) - BANANO was forked in April 2018 from NANO. BANANO offers.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOx" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/gcash">BCH - Bitcoin Cash</a></b> (🥈23 ·  ⭐ 350) - Hard fork of Bitcoin with a protocol upgrade to fix on-chain..</summary>


---
<details><summary><b><a href="https://github.com/gcash/bchd">bchd</a></b> (🥇24 ·  ⭐ 270) - An alternative full node bitcoin cash implementation written in Go (golang). <code><a href="http://bit.ly/3hkKRql">ISC</a></code></summary>

- [GitHub](https://github.com/gcash/bchd) (👨‍💻 130 · 🔀 95 · 📥 14K · 📦 220 · 📋 180 - 18% open · ⏱️ 22.12.2022):

	```
	git clone https://github.com/gcash/bchd
	```
</details>

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/litecoin-project">LTC - Litecoin</a></b> (🥈22 ·  ⭐ 4.3K · 💤) - The Future of Money.</summary>


---
<details><summary><b><a href="https://github.com/litecoin-project/litecoin">litecoin</a></b> (🥇25 ·  ⭐ 4.2K · 💤) - Litecoin source tree. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/litecoin-project/litecoin) (👨‍💻 940 · 🔀 3K · 📥 4.7K · 📋 520 - 14% open · ⏱️ 17.07.2022):

	```
	git clone https://github.com/litecoin-project/litecoin
	```
</details>

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/theQRL">QRL - Quantum Resistant Ledger</a></b> (🥈22 ·  ⭐ 480) - Cryptocurrency based on the Python programming..</summary>


---
<details><summary><b><a href="https://github.com/theQRL/QRL">QRL</a></b> (🥈21 ·  ⭐ 390) - Quantum Resistant Ledger. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/theQRL/QRL) (👨‍💻 34 · 🔀 99 · 📋 220 - 9% open · ⏱️ 18.11.2022):

	```
	git clone https://github.com/theQRL/QRL
	```
</details>

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/BTCGPU">BTG - Bitcoin Gold</a></b> (🥈21 ·  ⭐ 630) - Bitcoin Gold hopes to change the paradigm around mining on the..</summary>


---
<details><summary><b><a href="https://github.com/BTCGPU/BTCGPU">BTCGPU</a></b> (🥇24 ·  ⭐ 630) - Current work on BTCGPU. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/BTCGPU/BTCGPU) (👨‍💻 740 · 🔀 320 · 📥 280K · 📋 270 - 7% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/BTCGPU/BTCGPU
	```
</details>

---
</details>
<details><summary><b><a href="https://github.com/soramitsu">XOR - Sora</a></b> (🥈21 ·  ⭐ 390) - Supranational world economic system and decentralized and democratic.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/mintme-com">MINTME - MintMe.com Coin</a></b> (🥈21 ·  ⭐ 220) - Cryptocurrency launched in 2018. Users are able to.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/XDagger">XDAG - Dagger</a></b> (🥉18 ·  ⭐ 410 · 📈) - Novel application of Directed Acyclic Graph (DAG) technology that..</summary>


---
<details><summary><b><a href="https://github.com/XDagger/xdag">xdag</a></b> (🥈18 ·  ⭐ 320 · 💤) - XDAG (Dagger Coin) Official Main Repository. XDAG is a novel DAG based.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/XDagger/xdag) (👨‍💻 50 · 🔀 130 · 📋 120 - 29% open · ⏱️ 04.02.2022):

	```
	git clone https://github.com/XDagger/xdag
	```
</details>

<br>

 _1 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/MinterTeam">BIP - Minter Network</a></b> (🥉17 ·  ⭐ 390) - What is Minter Network (BIP)? Minter is a DeFi platform.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/MinterTeam/minter-go-node">minter-go-node</a></b> (🥈21 ·  ⭐ 360 · 💤) - Official Minter Blockchain Implementation in Go. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/MinterTeam/minter-go-node) (👨‍💻 19 · 🔀 69 · 📥 19K · 📦 17 · 📋 96 - 20% open · ⏱️ 29.04.2022):

	```
	git clone https://github.com/MinterTeam/minter-go-node
	```
</details>

<br>

 _1 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/mvs-org">ETP - Metaverse ETP</a></b> (🥉16 ·  ⭐ 390 · 💤) - Leading public blockchain based in China. Designed to..</summary>


---
<details><summary><b><a href="https://github.com/mvs-org/metaverse">metaverse</a></b> (🥈15 ·  ⭐ 310 · 💤) - The Metaverse individual chain service ETP coin and immutable.. <code><a href="http://bit.ly/3pwmjO5">AGPL-3.0</a></code></summary>

- [GitHub](https://github.com/mvs-org/metaverse) (👨‍💻 31 · 🔀 110 · 📥 73 · 📋 260 - 21% open · ⏱️ 06.06.2022):

	```
	git clone https://github.com/mvs-org/metaverse
	```
</details>

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/chainx-org">PCX - ChainX</a></b> (🥉16 ·  ⭐ 260) - ChainX aims to facilitate asset interoperability with other.. <code><img src="https://git.io/J9c3v" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/chainx-org/ChainX">ChainX</a></b> (🥈18 ·  ⭐ 260) - Bitcoins layer2 smart contract network has already supported WASM and.. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/chainx-org/ChainX) (👨‍💻 16 · 🔀 67 · 📥 5.4K · 📋 170 - 6% open · ⏱️ 18.10.2022):

	```
	git clone https://github.com/chainx-org/ChainX
	```
</details>

---
</details>
<details><summary>Show 24 hidden projects...</summary>

- <b><a href="https://github.com/Constellation-Labs">DAG - Constellation</a></b> (🥈22 ·  ⭐ 180) - What is DAG? In more traditional blockchains, the host provides..
- <b><a href="https://github.com/muntorg">MUNT</a></b> (🥈21 ·  ⭐ 140) - Cryptocurrency . Users are able to generate MUNT through the process of mining...
- <b><a href="https://github.com/ethereumcommonwealth">CLO - Callisto Network</a></b> (🥉19 ·  ⭐ 140) - Blockchain as seen by Security Experts. Since its.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/aionnetwork">AION</a></b> (🥉19 ·  ⭐ 100 · 💤) - Revolutionary multi-tier blockchain platform that has been created to solve the..
- <b><a href="https://github.com/runonflux">FLUX</a></b> (🥉18 ·  ⭐ 72) - Cryptocurrency launched in 2018. Users are able to generate FLUX through the.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/sugarchain-project">SUGAR - Sugarchain</a></b> (🥉17 ·  ⭐ 120 · 💀) - One-CPU-one-vote, the worlds fastest PoW blockchain.
- <b><a href="https://github.com/rapidsofficial">RPD - Rapids</a></b> (🥉17 ·  ⭐ 60) - We are a creative agency with a passion for innovation & the design of..
- <b><a href="https://github.com/uPlexa">UPX - uPlexa</a></b> (🥉15 ·  ⭐ 87) - UPlexa coin aims to incentivize the mass compute power of IoT devices..
- <b><a href="https://github.com/dogecash">DOGEC - DogeCash</a></b> (🥉15 ·  ⭐ 31 · 💀) - Transparent, community-governed cryptocurrency aimed at..
- <b><a href="https://github.com/WaykiChain">WICC - WaykiChain</a></b> (🥉14 ·  ⭐ 1.1K) - Blockchain-based gambling platform that supports Turing devices...
- <b><a href="https://github.com/ldoge">LDOGE - LiteDoge</a></b> (🥉14 ·  ⭐ 38) - What Is LiteDoge? LiteDoge (LDOGE) is an open-source peer to peer..
- <b><a href="https://github.com/devaultcrypto">DVT - DeVault</a></b> (🥉14 ·  ⭐ 33 · 💀) - Community governed and driven digital economy designed around the..
- <b><a href="https://github.com/arqma">ARQ - ArQmA</a></b> (🥉13 ·  ⭐ 43 · 💤) - Decentralized public project of block chains, crypto currencies, and is..
- <b><a href="https://github.com/tenup-coin">TUP - Tenup</a></b> (🥉12 ·  ⭐ 130 · 💤) - Masternode based POS coin. TenUp believes that every individual.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/tokencard">TKN - Monolith</a></b> (🥉11 ·  ⭐ 91 · 💀) - Non-custodial contract wallet paired with a debit card to.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/hpb-project">HPB - High Performance Blockchain</a></b> (🥉11 ·  ⭐ 56) - High-Performance Blockchain tackles the concept of..
- <b><a href="https://github.com/0xbitcoin">0XBTC - 0xBitcoin</a></b> (🥉9 ·  ⭐ 170 · 💤) - The first pure mined ERC20 Token for Ethereum, using the.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/citycoins">NYC - NewYorkCoin</a></b> (🥉9 ·  ⭐ 70) - Support, improve and program the city that never sleeps.
- <b><a href="https://github.com/stakecube">SCC - StakeCubeCoin</a></b> (🥉9 ·  ⭐ 45) - All-purpose coin when it comes to staking, masternode hosting,..
- <b><a href="https://github.com/HcashOrg">HC - HyperCash</a></b> (🥉9 ·  ⭐ 40 · 💀) - New cryptocurrency designed to allow value transfer among..
- <b><a href="https://github.com/joinseeds">SEEDS</a></b> (🥉9 ·  ⭐ 40) - SEEDS isnt just a better way to pay. Its a solution to the greatest crises of our..
- <b><a href="https://github.com/BitcoinHEX">HEX</a></b> (🥉7 ·  ⭐ 66 · 💀) - Launched on December 2, 2019 by Richard Heart and team, HEX is the first.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/pantos-io">PAN - Pantos</a></b> (🥉7 ·  ⭐ 34 · 💤) - Cryptocurrency launched in 2018and operates on the Ethereum.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/ltonetwork">LTO - LTO Network</a></b> (🥉6 ·  ⭐ 51 · 💀) - Europes leading blockc. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
</details>
<br>

## Smart Contract Platforms

<a href="#contents"><img align="right" width="15" height="15" src="https://git.io/JtehR" alt="Back to top"></a>

_Blockchains that host smart contracts or decentralized applications._

<details><summary><b><a href="https://github.com/vyperlang">ETH - Ethereum</a></b> (🥇52 ·  ⭐ 210K) - The foundation for our digital future.</summary>


---
<details><summary><b><a href="https://github.com/OpenZeppelin/openzeppelin-contracts">openzeppelin-contracts</a></b> (🥇38 ·  ⭐ 21K) - OpenZeppelin Contracts is a library for secure smart.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/OpenZeppelin/openzeppelin-contracts) (👨‍💻 390 · 🔀 11K · 📦 120K · 📋 1.6K - 11% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/OpenZeppelin/openzeppelin-contracts
	```
</details>
<details><summary><b><a href="https://github.com/ethereum/go-ethereum">go-ethereum</a></b> (🥇37 ·  ⭐ 41K) - Official Go implementation of the Ethereum protocol. <code><a href="http://bit.ly/37RvQcA">LGPL-3.0</a></code></summary>

- [GitHub](https://github.com/ethereum/go-ethereum) (👨‍💻 810 · 🔀 16K · 📦 16K · 📋 7.1K - 4% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/ethereum/go-ethereum
	```
</details>
<details><summary><b><a href="https://github.com/ethereum/solidity">solidity</a></b> (🥇34 ·  ⭐ 19K) - Solidity, the Smart Contract Programming Language. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/ethereum/solidity) (👨‍💻 640 · 🔀 4.6K · 📥 2.6M · 📋 5.7K - 18% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/ethereum/solidity
	```
</details>
<details><summary><b><a href="https://github.com/vyperlang/vyper">vyper</a></b> (🥇33 ·  ⭐ 4.4K) - Pythonic Smart Contract Language for the EVM. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/vyperlang/vyper) (👨‍💻 210 · 🔀 750 · 📥 270K · 📦 940 · 📋 1.3K - 21% open · ⏱️ 01.02.2023):

	```
	git clone https://github.com/vyperlang/vyper
	```
</details>
<details><summary><b><a href="https://github.com/ethereum/ethereum-org-website">ethereum-org-website</a></b> (🥇32 ·  ⭐ 3.8K) - Ethereum.org is a primary online resource for the.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/ethereum/ethereum-org-website) (👨‍💻 1.2K · 🔀 3.6K · 📋 2.4K - 16% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/ethereum/ethereum-org-website
	```
</details>
<details><summary><b><a href="https://github.com/ethereum/web3.py">web3.py</a></b> (🥇31 ·  ⭐ 4K) - A python interface for interacting with the Ethereum blockchain and.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/ethereum/web3.py) (👨‍💻 220 · 🔀 1.3K · 📦 11K · 📋 1.3K - 17% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/ethereum/web3.py
	```
</details>
<details><summary><b><a href="https://github.com/ethereumjs/ethereumjs-monorepo">ethereumjs-monorepo</a></b> (🥇31 ·  ⭐ 2.2K) - Monorepo for the Ethereum VM TypeScript Implementation. <code><a href="http://bit.ly/3postzC">MPL-2.0</a></code></summary>

- [GitHub](https://github.com/ethereumjs/ethereumjs-monorepo) (👨‍💻 180 · 🔀 630 · 📦 76K · 📋 870 - 17% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/ethereumjs/ethereumjs-monorepo
	```
</details>
<details><summary><b><a href="https://github.com/ConsenSys/quorum">quorum</a></b> (🥇30 ·  ⭐ 4.3K) - A permissioned implementation of Ethereum supporting data privacy. <code><a href="http://bit.ly/37RvQcA">LGPL-3.0</a></code></summary>

- [GitHub](https://github.com/ConsenSys/quorum) (👨‍💻 640 · 🔀 1.2K · 📥 18K · 📦 21 · 📋 800 - 3% open · ⏱️ 03.02.2023):

	```
	git clone https://github.com/ConsenSys/quorum
	```
</details>
<details><summary><b><a href="https://github.com/ethereum/remix-project">remix-project</a></b> (🥇30 ·  ⭐ 1.8K) - Remix is a browser-based compiler and IDE that enables users to.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/ethereum/remix-project) (👨‍💻 180 · 🔀 640 · 📥 140K · 📋 1.6K - 40% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/ethereum/remix-project
	```
</details>
<details><summary><b><a href="https://github.com/ethereum/consensus-specs">consensus-specs</a></b> (🥇28 ·  ⭐ 3K) - Ethereum Proof-of-Stake Consensus Specifications. <code><a href="https://tldrlegal.com/search?q=CC0-1.0">CC0-1.0</a></code></summary>

- [GitHub](https://github.com/ethereum/consensus-specs) (👨‍💻 130 · 🔀 770 · 📦 31 · 📋 900 - 23% open · ⏱️ 27.01.2023):

	```
	git clone https://github.com/ethereum/consensus-specs
	```
</details>
<details><summary><b><a href="https://github.com/dethcrypto/TypeChain">TypeChain</a></b> (🥇28 ·  ⭐ 2.4K) - TypeScript bindings for Ethereum smart contracts. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/dethcrypto/TypeChain) (👨‍💻 68 · 🔀 310 · 📦 110K · 📋 370 - 28% open · ⏱️ 12.12.2022):

	```
	git clone https://github.com/dethcrypto/TypeChain
	```
</details>
<details><summary><b><a href="https://github.com/OpenZeppelin/openzeppelin-contracts-upgradeable">openzeppelin-contracts-upgradeable</a></b> (🥇27 ·  ⭐ 780) - Upgradeable variant of OpenZeppelin Contracts, meant.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/OpenZeppelin/openzeppelin-contracts-upgradeable) (👨‍💻 220 · 🔀 350 · 📦 13K · 📋 98 - 5% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/OpenZeppelin/openzeppelin-contracts-upgradeable
	```
</details>
<details><summary><b><a href="https://github.com/ConsenSys/mythril">mythril</a></b> (🥇26 ·  ⭐ 3K) - Security analysis tool for EVM bytecode. Supports smart contracts built for.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/ConsenSys/mythril) (👨‍💻 84 · 🔀 610 · 📦 88 · 📋 800 - 11% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/ConsenSys/mythril
	```
</details>
<details><summary><b><a href="https://github.com/ethereum/EIPs">EIPs</a></b> (🥇25 ·  ⭐ 11K) - The Ethereum Improvement Proposal repository. <code><a href="https://tldrlegal.com/search?q=CC0-1.0">CC0-1.0</a></code></summary>

- [GitHub](https://github.com/ethereum/EIPs) (👨‍💻 560 · 🔀 4.2K · 📋 1.1K - 11% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/ethereum/EIPs
	```
</details>
<details><summary><b><a href="https://github.com/Nethereum/Nethereum">Nethereum</a></b> (🥇25 ·  ⭐ 1.9K) - Ethereum .Net cross platform integration library. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/Nethereum/Nethereum) (👨‍💻 77 · 🔀 680 · 📥 9.6K · 📋 740 - 10% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/Nethereum/Nethereum
	```
</details>
<details><summary><b><a href="https://github.com/ethereumjs/ethereumjs-wallet">ethereumjs-wallet</a></b> (🥇25 ·  ⭐ 910) - Utilities for handling Ethereum keys. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/ethereumjs/ethereumjs-wallet) (👨‍💻 18 · 🔀 290 · 📦 120K · 📋 98 - 27% open · ⏱️ 22.10.2022):

	```
	git clone https://github.com/ethereumjs/ethereumjs-wallet
	```
</details>
<details><summary><b><a href="https://github.com/ethereum/sourcify">sourcify</a></b> (🥇25 ·  ⭐ 540) - Decentralized Solidity contract source code verification service. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/ethereum/sourcify) (👨‍💻 49 · 🔀 220 · 📦 11 · 📋 380 - 11% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/ethereum/sourcify
	```
</details>
<details><summary><b><a href="https://github.com/ConsenSys/teku">teku</a></b> (🥇25 ·  ⭐ 500) - Java Implementation of the Ethereum 2.0 Beacon Chain. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/ConsenSys/teku) (👨‍💻 84 · 🔀 190 · 📋 2.1K - 6% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/ConsenSys/teku
	```
</details>
<details><summary><b><a href="https://github.com/ethereum/py-evm">py-evm</a></b> (🥇24 ·  ⭐ 1.9K) - A Python implementation of the Ethereum Virtual Machine. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/ethereum/py-evm) (👨‍💻 90 · 🔀 580 · 📦 560 · 📋 670 - 16% open · ⏱️ 16.12.2022):

	```
	git clone https://github.com/ethereum/py-evm
	```
</details>
<details><summary><b><a href="https://github.com/openethereum/openethereum">openethereum</a></b> (🥇24 ·  ⭐ 1.6K · 💤) - (deprecated) The fast, light, and robust client for the.. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/openethereum/openethereum) (👨‍💻 230 · 🔀 350 · 📥 47K · 📋 380 - 41% open · ⏱️ 05.04.2022):

	```
	git clone https://github.com/openethereum/openethereum
	```
</details>
<details><summary><b><a href="https://github.com/ethereum/remix-desktop">remix-desktop</a></b> (🥇24 ·  ⭐ 810) - Remix IDE desktop. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/ethereum/remix-desktop) (👨‍💻 6 · 🔀 210 · 📥 4.6M · 📦 170 · 📋 110 - 43% open · ⏱️ 08.11.2022):

	```
	git clone https://github.com/ethereum/remix-desktop
	```
</details>
<details><summary><b><a href="https://github.com/ConsenSys/gnark">gnark</a></b> (🥇23 ·  ⭐ 660 · 📈) - gnark is a fast zk-SNARK library that offers a high-level API to.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/ConsenSys/gnark) (👨‍💻 18 · 🔀 140 · 📦 170 · 📋 220 - 25% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/ConsenSys/gnark
	```
</details>
<details><summary><b><a href="https://github.com/ethereum/evmone">evmone</a></b> (🥇23 ·  ⭐ 580) - Fast Ethereum Virtual Machine implementation. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/ethereum/evmone) (👨‍💻 14 · 🔀 150 · 📥 36K · 📋 95 - 43% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/ethereum/evmone
	```
</details>
<details><summary><b><a href="https://github.com/OpenZeppelin/openzeppelin-upgrades">openzeppelin-upgrades</a></b> (🥇23 ·  ⭐ 480) - Plugins for Hardhat and Truffle to deploy and manage.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/OpenZeppelin/openzeppelin-upgrades) (👨‍💻 35 · 🔀 210 · 📦 210 · 📋 340 - 25% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/OpenZeppelin/openzeppelin-upgrades
	```
</details>
<details><summary><b><a href="https://github.com/ethereum/js-ethereum-cryptography">js-ethereum-cryptography</a></b> (🥇23 ·  ⭐ 440) - Every cryptographic primitive needed to work on.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/ethereum/js-ethereum-cryptography) (👨‍💻 6 · 🔀 72 · 📦 230K · 📋 14 - 14% open · ⏱️ 06.02.2023):

	```
	git clone https://github.com/ethereum/js-ethereum-cryptography
	```
</details>
<details><summary><b><a href="https://github.com/OpenZeppelin/openzeppelin-test-helpers">openzeppelin-test-helpers</a></b> (🥇23 ·  ⭐ 390) - Assertion library for Ethereum smart contract testing. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/OpenZeppelin/openzeppelin-test-helpers) (👨‍💻 30 · 🔀 120 · 📦 1.8K · 📋 100 - 35% open · ⏱️ 06.09.2022):

	```
	git clone https://github.com/OpenZeppelin/openzeppelin-test-helpers
	```
</details>
<details><summary><b><a href="https://github.com/ConsenSys/gnark-crypto">gnark-crypto</a></b> (🥇23 ·  ⭐ 260) - gnark-crypto provides elliptic curve and pairing-based.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/ConsenSys/gnark-crypto) (👨‍💻 9 · 🔀 67 · 📦 2.2K · 📋 120 - 33% open · ⏱️ 05.01.2023):

	```
	git clone https://github.com/ConsenSys/gnark-crypto
	```
</details>
<details><summary><b><a href="https://github.com/ethereum/tests">tests</a></b> (🥇22 ·  ⭐ 410) - Common tests for all Ethereum implementations. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/ethereum/tests) (👨‍💻 75 · 🔀 290 · 📋 310 - 10% open · ⏱️ 06.02.2023):

	```
	git clone https://github.com/ethereum/tests
	```
</details>
<details><summary><b><a href="https://github.com/ethereum/wiki">wiki</a></b> (🥈21 ·  ⭐ 15K · 💤) - The Ethereum Wiki. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/ethereum/wiki) (👨‍💻 200 · 🔀 2.6K · 📋 350 - 11% open · ⏱️ 18.05.2022):

	```
	git clone https://github.com/ethereum/wiki
	```
</details>
<details><summary><b><a href="https://github.com/OpenZeppelin/ethernaut">ethernaut</a></b> (🥈21 ·  ⭐ 1.5K) - Web3/Solidity based wargame. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/OpenZeppelin/ethernaut) (👨‍💻 90 · 🔀 450 · 📋 190 - 13% open · ⏱️ 06.02.2023):

	```
	git clone https://github.com/OpenZeppelin/ethernaut
	```
</details>
<details><summary><b><a href="https://github.com/ethereum/staking-deposit-cli">staking-deposit-cli</a></b> (🥈21 ·  ⭐ 390) - Secure key generation for deposits. <code><a href="https://tldrlegal.com/search?q=CC0-1.0">CC0-1.0</a></code></summary>

- [GitHub](https://github.com/ethereum/staking-deposit-cli) (👨‍💻 20 · 🔀 190 · 📥 74K · 📋 120 - 55% open · ⏱️ 31.01.2023):

	```
	git clone https://github.com/ethereum/staking-deposit-cli
	```
</details>
<details><summary><b><a href="https://github.com/OpenZeppelin/solidity-docgen">solidity-docgen</a></b> (🥈21 ·  ⭐ 350) - Documentation generator for Solidity projects. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/OpenZeppelin/solidity-docgen) (👨‍💻 15 · 🔀 94 · 📦 2.8K · 📋 140 - 20% open · ⏱️ 02.01.2023):

	```
	git clone https://github.com/OpenZeppelin/solidity-docgen
	```
</details>
<details><summary><b><a href="https://github.com/ethereum/eth-tester">eth-tester</a></b> (🥈21 ·  ⭐ 280) - Tool suite for testing ethereum applications. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/ethereum/eth-tester) (👨‍💻 35 · 🔀 120 · 📦 910 · 📋 91 - 42% open · ⏱️ 16.12.2022):

	```
	git clone https://github.com/ethereum/eth-tester
	```
</details>
<details><summary><b><a href="https://github.com/ethereum/remix-ide">remix-ide</a></b> (🥈20 ·  ⭐ 2.1K) - Documentation for Remix IDE. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/ethereum/remix-ide) (👨‍💻 140 · 🔀 940 · 📥 5.7K · ⏱️ 31.01.2023):

	```
	git clone https://github.com/ethereum/remix-ide
	```
</details>
<details><summary><b><a href="https://github.com/ethereum/pm">pm</a></b> (🥈20 ·  ⭐ 1.2K) - Project Management: Meeting notes and agenda items. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/ethereum/pm) (👨‍💻 79 · 🔀 240 · 📋 380 - 5% open · ⏱️ 11.01.2023):

	```
	git clone https://github.com/ethereum/pm
	```
</details>
<details><summary><b><a href="https://github.com/OpenZeppelin/cairo-contracts">cairo-contracts</a></b> (🥈20 ·  ⭐ 600) - OpenZeppelin Contracts written in Cairo for StarkNet, a.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/OpenZeppelin/cairo-contracts) (👨‍💻 36 · 🔀 200 · 📋 260 - 23% open · ⏱️ 20.01.2023):

	```
	git clone https://github.com/OpenZeppelin/cairo-contracts
	```
</details>
<details><summary><b><a href="https://github.com/ethereumjs/keythereum">keythereum</a></b> (🥈20 ·  ⭐ 600) - Create, import and export Ethereum keys. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/ethereumjs/keythereum) (👨‍💻 15 · 🔀 180 · 📦 2.7K · 📋 50 - 22% open · ⏱️ 03.08.2022):

	```
	git clone https://github.com/ethereumjs/keythereum
	```
</details>
<details><summary><b><a href="https://github.com/ethereum/beacon-APIs">beacon-APIs</a></b> (🥈20 ·  ⭐ 230) - Collection of RESTful APIs provided by Ethereum Beacon nodes. <code><a href="https://tldrlegal.com/search?q=CC0-1.0">CC0-1.0</a></code></summary>

- [GitHub](https://github.com/ethereum/beacon-APIs) (👨‍💻 36 · 🔀 110 · 📥 35K · 📋 100 - 39% open · ⏱️ 05.02.2023):

	```
	git clone https://github.com/ethereum/beacon-APIs
	```
</details>
<details><summary><b><a href="https://github.com/ethereum/fe">fe</a></b> (🥈19 ·  ⭐ 1.4K) - Emerging smart contract language for the Ethereum blockchain. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/ethereum/fe) (👨‍💻 29 · 🔀 110 · 📥 730 · 📋 370 - 29% open · ⏱️ 05.02.2023):

	```
	git clone https://github.com/ethereum/fe
	```
</details>
<details><summary><b><a href="https://github.com/ConsenSys/cakeshop">cakeshop</a></b> (🥈19 ·  ⭐ 510 · 💤) - An integrated development environment and SDK for Ethereum-like.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/ConsenSys/cakeshop) (👨‍💻 29 · 🔀 200 · 📥 14K · 📋 120 - 20% open · ⏱️ 26.05.2022):

	```
	git clone https://github.com/ConsenSys/cakeshop
	```
</details>
<details><summary><b><a href="https://github.com/ethereum/eth-utils">eth-utils</a></b> (🥈19 ·  ⭐ 270) - Utility functions for working with ethereum related codebases. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/ethereum/eth-utils) (👨‍💻 31 · 🔀 140 · 📦 6.7K · 📋 75 - 32% open · ⏱️ 17.11.2022):

	```
	git clone https://github.com/ethereum/eth-utils
	```
</details>
<details><summary><b><a href="https://github.com/ConsenSys/smart-contract-best-practices">smart-contract-best-practices</a></b> (🥈18 ·  ⭐ 6.5K) - A guide to smart contract security best practices. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/ConsenSys/smart-contract-best-practices) (👨‍💻 110 · 🔀 1.2K · 📋 100 - 22% open · ⏱️ 12.01.2023):

	```
	git clone https://github.com/ConsenSys/smart-contract-best-practices
	```
</details>
<details><summary><b><a href="https://github.com/ConsenSys/ethereum-developer-tools-list">ethereum-developer-tools-list</a></b> (🥈18 ·  ⭐ 4.6K) - A guide to available tools and platforms for.. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/ConsenSys/ethereum-developer-tools-list) (👨‍💻 220 · 🔀 1K · 📋 49 - 26% open · ⏱️ 31.01.2023):

	```
	git clone https://github.com/ConsenSys/ethereum-developer-tools-list
	```
</details>
<details><summary><b><a href="https://github.com/ethereum/yellowpaper">yellowpaper</a></b> (🥈18 ·  ⭐ 1.5K) - The Yellow Paper: Ethereums formal specification. <code><a href="http://bit.ly/3mSooSG">CC-BY-SA-4.0</a></code></summary>

- [GitHub](https://github.com/ethereum/yellowpaper) (👨‍💻 84 · 🔀 490 · 📋 250 - 42% open · ⏱️ 24.10.2022):

	```
	git clone https://github.com/ethereum/yellowpaper
	```
</details>
<details><summary><b><a href="https://github.com/ethereum/solc-js">solc-js</a></b> (🥈18 ·  ⭐ 1.2K) - Javascript bindings for the Solidity compiler. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/ethereum/solc-js) (👨‍💻 39 · 🔀 290 · 📋 250 - 25% open · ⏱️ 01.02.2023):

	```
	git clone https://github.com/ethereum/solc-js
	```
</details>
<details><summary><b><a href="https://github.com/ConsenSys/surya">surya</a></b> (🥈18 ·  ⭐ 840 · 💤) - A set of utilities for exploring Solidity contracts. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/ConsenSys/surya) (👨‍💻 18 · 🔀 89 · 📦 390 · 📋 63 - 9% open · ⏱️ 07.04.2022):

	```
	git clone https://github.com/ConsenSys/surya
	```
</details>
<details><summary><b><a href="https://github.com/ethereum/execution-specs">execution-specs</a></b> (🥈18 ·  ⭐ 470) - Specification for the Execution Layer. Tracking network.. <code><a href="https://tldrlegal.com/search?q=CC0-1.0">CC0-1.0</a></code></summary>

- [GitHub](https://github.com/ethereum/execution-specs) (👨‍💻 55 · 🔀 140 · 📋 330 - 16% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/ethereum/execution-specs
	```
</details>
<details><summary><b><a href="https://github.com/OpenZeppelin/nile">nile</a></b> (🥈18 ·  ⭐ 310) - CLI tool to develop StarkNet projects written in Cairo. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/OpenZeppelin/nile) (👨‍💻 23 · 🔀 66 · 📋 200 - 30% open · ⏱️ 06.02.2023):

	```
	git clone https://github.com/OpenZeppelin/nile
	```
</details>
<details><summary><b><a href="https://github.com/ethereum/research">research</a></b> (🥈17 ·  ⭐ 1.6K) -  <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/ethereum/research) (👨‍💻 28 · 🔀 380 · 📋 63 - 90% open · ⏱️ 23.01.2023):

	```
	git clone https://github.com/ethereum/research
	```
</details>
<details><summary><b><a href="https://github.com/ethereum/execution-apis">execution-apis</a></b> (🥈17 ·  ⭐ 580) - Collection of APIs provided by Ethereum execution layer.. <code><a href="https://tldrlegal.com/search?q=CC0-1.0">CC0-1.0</a></code></summary>

- [GitHub](https://github.com/ethereum/execution-apis) (👨‍💻 33 · 🔀 200 · 📋 100 - 50% open · ⏱️ 03.02.2023):

	```
	git clone https://github.com/ethereum/execution-apis
	```
</details>
<details><summary><b><a href="https://github.com/ConsenSys/UniversalToken">UniversalToken</a></b> (🥈17 ·  ⭐ 300) - Implementation of Universal Token for Assets and Payments. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/ConsenSys/UniversalToken) (👨‍💻 26 · 🔀 160 · 📋 41 - 31% open · ⏱️ 14.10.2022):

	```
	git clone https://github.com/ConsenSys/UniversalToken
	```
</details>
<details><summary><b><a href="https://github.com/ethereum/evmc">evmc</a></b> (🥈17 ·  ⭐ 260) - EVMC Ethereum Client-VM Connector API. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/ethereum/evmc) (👨‍💻 14 · 🔀 110 · 📥 17 · 📦 12 · 📋 180 - 26% open · ⏱️ 27.01.2023):

	```
	git clone https://github.com/ethereum/evmc
	```
</details>
<details><summary><b><a href="https://github.com/ConsenSys/scribble">scribble</a></b> (🥈17 ·  ⭐ 250) - Scribble instrumentation tool. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/ConsenSys/scribble) (👨‍💻 4 · 🔀 24 · 📦 9 · 📋 88 - 21% open · ⏱️ 30.11.2022):

	```
	git clone https://github.com/ConsenSys/scribble
	```
</details>
<details><summary><b><a href="https://github.com/dethcrypto/eth-sdk">eth-sdk</a></b> (🥈16 ·  ⭐ 370 · 💤) - Type-safe, lightweight SDKs for Ethereum smart contracts. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/dethcrypto/eth-sdk) (👨‍💻 8 · 🔀 36 · 📦 380 · 📋 50 - 46% open · ⏱️ 15.06.2022):

	```
	git clone https://github.com/dethcrypto/eth-sdk
	```
</details>
<details><summary><b><a href="https://github.com/ethereum/solc-bin">solc-bin</a></b> (🥈16 ·  ⭐ 330) - This repository contains current and historical builds of the.. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/ethereum/solc-bin) (👨‍💻 16 · 🔀 230 · 📦 1 · ⏱️ 08.02.2023):

	```
	git clone https://github.com/ethereum/solc-bin
	```
</details>
<details><summary><b><a href="https://github.com/ethereum/staking-launchpad">staking-launchpad</a></b> (🥈16 ·  ⭐ 220) - The deposit launchpad for staking on Ethereum. <code><a href="https://tldrlegal.com/search?q=CC0-1.0">CC0-1.0</a></code></summary>

- [GitHub](https://github.com/ethereum/staking-launchpad) (👨‍💻 38 · 🔀 130 · 📋 180 - 49% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/ethereum/staking-launchpad
	```
</details>
<details><summary><b><a href="https://github.com/ConsenSys/quorum-examples">quorum-examples</a></b> (🥈15 ·  ⭐ 310) - Examples for Quorum. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/ConsenSys/quorum-examples) (👨‍💻 36 · 🔀 340 · 📋 130 - 5% open · ⏱️ 28.10.2022):

	```
	git clone https://github.com/ConsenSys/quorum-examples
	```
</details>

<br>

 _130 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/w3f">DOT - Polkadot</a></b> (🥇46 ·  ⭐ 34K) - Platform that allows diverse blockchains to transfer messages,..</summary>


---
<details><summary><b><a href="https://github.com/paritytech/substrate">substrate</a></b> (🥇33 ·  ⭐ 7.9K) - Substrate: The platform for blockchain innovators. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/paritytech/substrate) (👨‍💻 410 · 🔀 2.5K · 📥 1.5K · 📦 320 · 📋 4.2K - 26% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/paritytech/substrate
	```
</details>
<details><summary><b><a href="https://github.com/polkadot-js/apps">apps</a></b> (🥇32 ·  ⭐ 1.6K) - Basic Polkadot/Substrate UI for interacting with a Polkadot and.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/polkadot-js/apps) (👨‍💻 400 · 🔀 1K · 📥 99K · 📦 8 · 📋 2.8K - 5% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/polkadot-js/apps
	```
</details>
<details><summary><b><a href="https://github.com/paritytech/polkadot">polkadot</a></b> (🥇31 ·  ⭐ 6.6K) - Polkadot Node Implementation. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/paritytech/polkadot) (👨‍💻 220 · 🔀 1.4K · 📥 180K · 📋 1.9K - 28% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/paritytech/polkadot
	```
</details>
<details><summary><b><a href="https://github.com/polkadot-js/api">api</a></b> (🥇31 ·  ⭐ 1K) - Promise and RxJS APIs around Polkadot and Substrate based chains via RPC.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/polkadot-js/api) (👨‍💻 120 · 🔀 280 · 📦 17K · 📋 1.7K - 1% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/polkadot-js/api
	```
</details>
<details><summary><b><a href="https://github.com/paritytech/wasmi">wasmi</a></b> (🥇28 ·  ⭐ 1K) - WebAssembly (Wasm) interpreter. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/paritytech/wasmi) (👨‍💻 41 · 🔀 200 · 📦 20K · 📋 160 - 13% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/paritytech/wasmi
	```
</details>
<details><summary><b><a href="https://github.com/paritytech/cumulus">cumulus</a></b> (🥇28 ·  ⭐ 550) - Write Parachains on Substrate. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/paritytech/cumulus) (👨‍💻 120 · 🔀 340 · 📥 5K · 📦 1.7K · 📋 530 - 32% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/paritytech/cumulus
	```
</details>
<details><summary><b><a href="https://github.com/paritytech/ink">ink</a></b> (🥇27 ·  ⭐ 1.1K) - Paritys ink! to write smart contracts. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/paritytech/ink) (👨‍💻 60 · 🔀 360 · 📦 77 · 📋 620 - 24% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/paritytech/ink
	```
</details>
<details><summary><b><a href="https://github.com/polkadot-js/extension">extension</a></b> (🥇27 ·  ⭐ 860) - Simple browser extension for managing Polkadot and Substrate.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/polkadot-js/extension) (👨‍💻 40 · 🔀 320 · 📥 2.5K · 📦 91 · 📋 430 - 6% open · ⏱️ 05.02.2023):

	```
	git clone https://github.com/polkadot-js/extension
	```
</details>
<details><summary><b><a href="https://github.com/paritytech/jsonrpsee">jsonrpsee</a></b> (🥇25 ·  ⭐ 360) - Rust JSON-RPC library on top of async/await. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/paritytech/jsonrpsee) (👨‍💻 46 · 🔀 89 · 📦 4K · 📋 370 - 12% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/paritytech/jsonrpsee
	```
</details>
<details><summary><b><a href="https://github.com/polkadot-js/common">common</a></b> (🥇25 ·  ⭐ 240) - Utilities and base libraries for use across polkadot-js for Polkadot.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/polkadot-js/common) (👨‍💻 60 · 🔀 120 · 📦 550 · 📋 250 - 1% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/polkadot-js/common
	```
</details>
<details><summary><b><a href="https://github.com/paritytech/parity-signer">parity-signer</a></b> (🥇24 ·  ⭐ 480) - Air-gapped crypto wallet. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/paritytech/parity-signer) (👨‍💻 42 · 🔀 150 · 📥 900 · 📦 78 · 📋 580 - 17% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/paritytech/parity-signer
	```
</details>
<details><summary><b><a href="https://github.com/paritytech/subxt">subxt</a></b> (🥇24 ·  ⭐ 290) - Submit extrinsics (transactions) to a substrate node via RPC. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/paritytech/subxt) (👨‍💻 53 · 🔀 170 · 📦 290 · 📋 330 - 14% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/paritytech/subxt
	```
</details>
<details><summary><b><a href="https://github.com/w3f/Grants-Program">Grants-Program</a></b> (🥇22 ·  ⭐ 730) - Web3 Foundation Grants Program. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/w3f/Grants-Program) (👨‍💻 350 · 🔀 1.5K · 📦 3 · 📋 40 - 50% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/w3f/Grants-Program
	```
</details>
<details><summary><b><a href="https://github.com/paritytech/parity-wasm">parity-wasm</a></b> (🥈21 ·  ⭐ 390) - WebAssembly serialization/deserialization in rust. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/paritytech/parity-wasm) (👨‍💻 41 · 🔀 98 · 📦 25K · 📋 150 - 23% open · ⏱️ 01.10.2022):

	```
	git clone https://github.com/paritytech/parity-wasm
	```
</details>
<details><summary><b><a href="https://github.com/w3f/polkadot-wiki">polkadot-wiki</a></b> (🥈21 ·  ⭐ 260) - The source of truth for Polkadot. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/w3f/polkadot-wiki) (👨‍💻 230 · 🔀 450 · 📋 1.3K - 3% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/w3f/polkadot-wiki
	```
</details>
<details><summary><b><a href="https://github.com/polkadot-js/tools">tools</a></b> (🥈20 ·  ⭐ 260) - Various cli tools for Polkadot and Substrate chains, including basic.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/polkadot-js/tools) (👨‍💻 19 · 🔀 63 · 📦 13 · 📋 50 - 10% open · ⏱️ 05.02.2023):

	```
	git clone https://github.com/polkadot-js/tools
	```
</details>
<details><summary><b><a href="https://github.com/paritytech/parity-bridges-common">parity-bridges-common</a></b> (🥈19 ·  ⭐ 220) - Collection of Useful Bridge Building Tools. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/paritytech/parity-bridges-common) (👨‍💻 45 · 🔀 87 · 📋 570 - 17% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/paritytech/parity-bridges-common
	```
</details>
<details><summary><b><a href="https://github.com/paritytech/parity-common">parity-common</a></b> (🥈19 ·  ⭐ 220) - Collection of crates used in Parity projects. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/paritytech/parity-common) (👨‍💻 100 · 🔀 180 · 📦 6 · 📋 170 - 16% open · ⏱️ 01.02.2023):

	```
	git clone https://github.com/paritytech/parity-common
	```
</details>
<details><summary><b><a href="https://github.com/paritytech/jsonrpc">jsonrpc</a></b> (🥈18 ·  ⭐ 720 · 💤) - Rust JSON-RPC implementation. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/paritytech/jsonrpc) (👨‍💻 74 · 🔀 190 · 📋 240 - 28% open · ⏱️ 17.06.2022):

	```
	git clone https://github.com/paritytech/jsonrpc
	```
</details>
<details><summary><b><a href="https://github.com/paritytech/polkadot-launch">polkadot-launch</a></b> (🥈18 ·  ⭐ 450 · 💤) - Simple CLI tool to launch a local Polkadot test network. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/paritytech/polkadot-launch) (👨‍💻 23 · 🔀 82 · 📦 56 · 📋 92 - 45% open · ⏱️ 27.07.2022):

	```
	git clone https://github.com/paritytech/polkadot-launch
	```
</details>
<details><summary><b><a href="https://github.com/paritytech/frontier">frontier</a></b> (🥈18 ·  ⭐ 410) - Ethereum compatibility layer for Substrate. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/paritytech/frontier) (👨‍💻 61 · 🔀 330 · 📋 210 - 57% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/paritytech/frontier
	```
</details>
<details><summary><b><a href="https://github.com/paritytech/smoldot">smoldot</a></b> (🥈18 ·  ⭐ 280) - Alternative client for Substrate-based chains. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/paritytech/smoldot) (👨‍💻 19 · 🔀 67 · 📦 39 · 📋 440 - 26% open · ⏱️ 01.12.2022):

	```
	git clone https://github.com/paritytech/smoldot
	```
</details>

<br>

 _68 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/solana-labs">SOL - Solana</a></b> (🥇44 ·  ⭐ 21K) - About Solana (SOL)? Solana (SOL) is a high throughput cryptocurrency..</summary>


---
<details><summary><b><a href="https://github.com/solana-labs/solana">solana</a></b> (🥇37 ·  ⭐ 10K) - Web-Scale Blockchain for fast, secure, scalable, decentralized.. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/solana-labs/solana) (👨‍💻 450 · 🔀 2.8K · 📥 500K · 📦 50 · 📋 4.9K - 20% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/solana-labs/solana
	```
</details>
<details><summary><b><a href="https://github.com/solana-labs/token-list">token-list</a></b> (🥇33 ·  ⭐ 1.3K · 💤) - The community maintained Solana token registry. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/solana-labs/token-list) (👨‍💻 9K · 🔀 12K · 📦 2.9K · 📋 6.1K - 80% open · ⏱️ 03.07.2022):

	```
	git clone https://github.com/solana-labs/token-list
	```
</details>
<details><summary><b><a href="https://github.com/solana-labs/solana-web3.js">solana-web3.js</a></b> (🥇30 ·  ⭐ 1.4K) - Solana JavaScript SDK. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/solana-labs/solana-web3.js) (👨‍💻 96 · 🔀 470 · 📦 45K · 📋 160 - 38% open · ⏱️ 06.02.2023):

	```
	git clone https://github.com/solana-labs/solana-web3.js
	```
</details>
<details><summary><b><a href="https://github.com/solana-labs/solana-program-library">solana-program-library</a></b> (🥇28 ·  ⭐ 2.2K) - A collection of Solana programs maintained by Solana.. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/solana-labs/solana-program-library) (👨‍💻 150 · 🔀 1.3K · 📥 160K · 📦 15 · 📋 680 - 28% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/solana-labs/solana-program-library
	```
</details>
<details><summary><b><a href="https://github.com/solana-labs/wallet-adapter">wallet-adapter</a></b> (🥇27 ·  ⭐ 1.1K) - Modular TypeScript wallet adapters and components for.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/solana-labs/wallet-adapter) (👨‍💻 98 · 🔀 650 · 📦 1.4K · 📋 340 - 5% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/solana-labs/wallet-adapter
	```
</details>
<details><summary><b><a href="https://github.com/solana-labs/solana-pay">solana-pay</a></b> (🥇22 ·  ⭐ 1.1K) - A new standard for decentralized payments. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/solana-labs/solana-pay) (👨‍💻 27 · 🔀 330 · 📦 530 · 📋 74 - 29% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/solana-labs/solana-pay
	```
</details>
<details><summary><b><a href="https://github.com/solana-labs/dapp-scaffold">dapp-scaffold</a></b> (🥈17 ·  ⭐ 1.5K) - Scaffolding for a dapp built on Solana. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/solana-labs/dapp-scaffold) (👨‍💻 23 · 🔀 670 · 📋 44 - 6% open · ⏱️ 31.01.2023):

	```
	git clone https://github.com/solana-labs/dapp-scaffold
	```
</details>
<details><summary><b><a href="https://github.com/solana-labs/example-helloworld">example-helloworld</a></b> (🥈16 ·  ⭐ 760) - Hello world on Solana. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/solana-labs/example-helloworld) (👨‍💻 39 · 🔀 650 · 📋 140 - 24% open · ⏱️ 20.01.2023):

	```
	git clone https://github.com/solana-labs/example-helloworld
	```
</details>

<br>

 _18 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/input-output-hk">ADA - Cardano</a></b> (🥇39 ·  ⭐ 15K) - Decentralised platform that will allow complex programmable transfers..</summary>


---
<details><summary><b><a href="https://github.com/input-output-hk/cardano-node">cardano-node</a></b> (🥇28 ·  ⭐ 2.9K) - The core component that is used to participate in a Cardano.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/input-output-hk/cardano-node) (👨‍💻 130 · 🔀 670 · 📥 890 · 📋 1.8K - 18% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/input-output-hk/cardano-node
	```
</details>
<details><summary><b><a href="https://github.com/input-output-hk/daedalus">daedalus</a></b> (🥇26 ·  ⭐ 1.2K) - The open source cryptocurrency wallet for ada, built to grow with.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/input-output-hk/daedalus) (👨‍💻 69 · 🔀 290 · 📥 8.1K · 📋 580 - 18% open · ⏱️ 17.01.2023):

	```
	git clone https://github.com/input-output-hk/daedalus
	```
</details>
<details><summary><b><a href="https://github.com/input-output-hk/cardano-wallet">cardano-wallet</a></b> (🥇26 ·  ⭐ 700) - HTTP server & command-line for managing UTxOs and HD wallets.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/input-output-hk/cardano-wallet) (👨‍💻 47 · 🔀 210 · 📥 15K · 📋 850 - 13% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/input-output-hk/cardano-wallet
	```
</details>
<details><summary><b><a href="https://github.com/input-output-hk/plutus-apps">plutus-apps</a></b> (🥇25 ·  ⭐ 290) - The Plutus application platform. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/input-output-hk/plutus-apps) (👨‍💻 150 · 🔀 190 · 📋 290 - 29% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/input-output-hk/plutus-apps
	```
</details>
<details><summary><b><a href="https://github.com/input-output-hk/plutus">plutus</a></b> (🥇23 ·  ⭐ 1.4K) - The Plutus language implementation and tools. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/input-output-hk/plutus) (👨‍💻 140 · 🔀 440 · 📋 980 - 12% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/input-output-hk/plutus
	```
</details>
<details><summary><b><a href="https://github.com/input-output-hk/jormungandr">jormungandr</a></b> (🥇22 ·  ⭐ 360) - privacy voting blockchain node. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/input-output-hk/jormungandr) (👨‍💻 66 · 🔀 140 · 📥 76K · 📋 1K - 25% open · ⏱️ 06.11.2022):

	```
	git clone https://github.com/input-output-hk/jormungandr
	```
</details>
<details><summary><b><a href="https://github.com/input-output-hk/cardano-graphql">cardano-graphql</a></b> (🥈21 ·  ⭐ 250) - GraphQL API for Cardano. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/input-output-hk/cardano-graphql) (👨‍💻 30 · 🔀 100 · 📥 10 · 📦 8 · 📋 380 - 12% open · ⏱️ 03.02.2023):

	```
	git clone https://github.com/input-output-hk/cardano-graphql
	```
</details>
<details><summary><b><a href="https://github.com/input-output-hk/cardano-db-sync">cardano-db-sync</a></b> (🥈21 ·  ⭐ 240) - A component that follows the Cardano chain and stores.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/input-output-hk/cardano-db-sync) (👨‍💻 56 · 🔀 140 · 📋 600 - 20% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/input-output-hk/cardano-db-sync
	```
</details>
<details><summary><b><a href="https://github.com/input-output-hk/haskell.nix">haskell.nix</a></b> (🥈20 ·  ⭐ 460) - Alternative Haskell Infrastructure for Nixpkgs. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/input-output-hk/haskell.nix) (👨‍💻 130 · 🔀 200 · 📋 820 - 36% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/input-output-hk/haskell.nix
	```
</details>
<details><summary><b><a href="https://github.com/input-output-hk/ouroboros-network">ouroboros-network</a></b> (🥈20 ·  ⭐ 240) - An implementation of the Ouroboros family of consensus.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/input-output-hk/ouroboros-network) (👨‍💻 76 · 🔀 75 · 📋 1.7K - 38% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/input-output-hk/ouroboros-network
	```
</details>
<details><summary><b><a href="https://github.com/input-output-hk/essential-cardano">essential-cardano</a></b> (🥈17 ·  ⭐ 740) - Repository for the Essential Cardano list. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/input-output-hk/essential-cardano) (👨‍💻 190 · 🔀 290 · 📋 14 - 7% open · ⏱️ 12.12.2022):

	```
	git clone https://github.com/input-output-hk/essential-cardano
	```
</details>
<details><summary><b><a href="https://github.com/input-output-hk/cardano-documentation">cardano-documentation</a></b> (🥈17 ·  ⭐ 230) -  <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/input-output-hk/cardano-documentation) (👨‍💻 63 · 🔀 130 · 📋 40 - 12% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/input-output-hk/cardano-documentation
	```
</details>
<details><summary><b><a href="https://github.com/input-output-hk/rust-byron-cardano">rust-byron-cardano</a></b> (🥈15 ·  ⭐ 280 · 💤) - rust client libraries to deal with the current cardano.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/input-output-hk/rust-byron-cardano) (👨‍💻 26 · 🔀 78 · 📦 1 · ⏱️ 11.06.2022):

	```
	git clone https://github.com/input-output-hk/rust-byron-cardano
	```
</details>

<br>

 _46 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/near">NEAR - NEAR Protocol</a></b> (🥇38 ·  ⭐ 6K) - Infrastructure for innovation. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/near/nearcore">nearcore</a></b> (🥇30 ·  ⭐ 2K) - Reference client for NEAR Protocol. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/near/nearcore) (👨‍💻 120 · 🔀 420 · 📦 1.5K · 📋 2.8K - 22% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/near/nearcore
	```
</details>
<details><summary><b><a href="https://github.com/near/near-api-js">near-api-js</a></b> (🥇27 ·  ⭐ 320) - JavaScript library to interact with NEAR Protocol via RPC API. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/near/near-api-js) (👨‍💻 64 · 🔀 190 · 📦 11K · 📋 310 - 29% open · ⏱️ 25.01.2023):

	```
	git clone https://github.com/near/near-api-js
	```
</details>
<details><summary><b><a href="https://github.com/near/near-sdk-rs">near-sdk-rs</a></b> (🥇25 ·  ⭐ 390) - Rust library for writing NEAR smart contracts. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/near/near-sdk-rs) (👨‍💻 57 · 🔀 180 · 📦 1.9K · 📋 450 - 34% open · ⏱️ 23.01.2023):

	```
	git clone https://github.com/near/near-sdk-rs
	```
</details>
<details><summary><b><a href="https://github.com/near/create-near-app">create-near-app</a></b> (🥈19 ·  ⭐ 290) - Create a starter app hooked up to the NEAR blockchain. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/near/create-near-app) (👨‍💻 27 · 🔀 120 · 📦 8 · 📋 96 - 22% open · ⏱️ 10.11.2022):

	```
	git clone https://github.com/near/create-near-app
	```
</details>
<details><summary><b><a href="https://github.com/near/borsh">borsh</a></b> (🥈17 ·  ⭐ 340) - Binary Object Representation Serializer for Hashing. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/near/borsh) (👨‍💻 22 · 🔀 35 · 📦 28K · 📋 61 - 55% open · ⏱️ 30.08.2022):

	```
	git clone https://github.com/near/borsh
	```
</details>

<br>

 _30 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/Chia-Network">XCH - Chia</a></b> (🥇37 ·  ⭐ 13K) - Founded by Bram Cohen, the inventor of the BitTorrent network, Chia Network..</summary>


---
<details><summary><b><a href="https://github.com/Chia-Network/chia-blockchain">chia-blockchain</a></b> (🥇37 ·  ⭐ 11K) - Chia blockchain python implementation (full node, farmer,.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/Chia-Network/chia-blockchain) (👨‍💻 150 · 🔀 2K · 📥 800K · 📦 76 · 📋 4.6K - 6% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/Chia-Network/chia-blockchain
	```
</details>
<details><summary><b><a href="https://github.com/Chia-Network/bls-signatures">Chia-Network/bls-signatures</a></b> (🥇23 ·  ⭐ 270) - BLS signatures in C++, using the relic toolkit.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/Chia-Network/bls-signatures) (👨‍💻 44 · 🔀 200 · 📦 270 · 📋 110 - 14% open · ⏱️ 19.01.2023):

	```
	git clone https://github.com/Chia-Network/bls-signatures
	```
</details>
<details><summary><b><a href="https://github.com/Chia-Network/chiapos">chiapos</a></b> (🥇23 ·  ⭐ 270) - Chia Proof of Space library. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/Chia-Network/chiapos) (👨‍💻 42 · 🔀 270 · 📦 440 · 📋 100 - 24% open · ⏱️ 19.01.2023):

	```
	git clone https://github.com/Chia-Network/chiapos
	```
</details>
<details><summary><b><a href="https://github.com/Chia-Network/chia-blockchain-gui">chia-blockchain-gui</a></b> (🥈21 ·  ⭐ 320) - Chia blockchain GUI in electron/react. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/Chia-Network/chia-blockchain-gui) (👨‍💻 40 · 🔀 220 · 📋 530 - 10% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/Chia-Network/chia-blockchain-gui
	```
</details>
<details><summary><b><a href="https://github.com/Chia-Network/bladebit">bladebit</a></b> (🥈20 ·  ⭐ 250) - A high-performance k32-only, Chia (XCH) plotter supporting in-RAM.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/Chia-Network/bladebit) (👨‍💻 7 · 🔀 82 · 📥 80K · 📋 93 - 29% open · ⏱️ 06.02.2023):

	```
	git clone https://github.com/Chia-Network/bladebit
	```
</details>
<details><summary><b><a href="https://github.com/Chia-Network/pool-reference">pool-reference</a></b> (🥈17 ·  ⭐ 450) - Reference python implementation of Chia pool operations for.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/Chia-Network/pool-reference) (👨‍💻 37 · 🔀 190 · 📋 180 - 4% open · ⏱️ 19.01.2023):

	```
	git clone https://github.com/Chia-Network/pool-reference
	```
</details>
<details><summary><b><a href="https://github.com/Chia-Network/chia-docker">chia-docker</a></b> (🥈17 ·  ⭐ 200) -  <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/Chia-Network/chia-docker) (👨‍💻 32 · 🔀 340 · 📋 110 - 25% open · ⏱️ 19.01.2023):

	```
	git clone https://github.com/Chia-Network/chia-docker
	```
</details>

<br>

 _9 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/eosio">EOS</a></b> (🥇37 ·  ⭐ 7.2K) - Blockchain built and run by the EOS Community. The EOSIO core development is now in..</summary>


---
<details><summary><b><a href="https://github.com/EOSIO/eosjs">eosjs</a></b> (🥇28 ·  ⭐ 1.4K · 💤) - General purpose library for the EOSIO blockchain. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/EOSIO/eosjs) (👨‍💻 63 · 🔀 490 · 📥 11K · 📦 5.3K · 📋 590 - 8% open · ⏱️ 07.03.2022):

	```
	git clone https://github.com/EOSIO/eosjs
	```
</details>

<br>

 _31 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/LiskHQ">LSK - Lisk</a></b> (🥈35 ·  ⭐ 3.7K) - Cryptocurrency launched in 2016. Lisk has a current supply of 144,818,773..</summary>


---
<details><summary><b><a href="https://github.com/LiskHQ/lisk-sdk">lisk-sdk</a></b> (🥇30 ·  ⭐ 2.8K) - Lisk software development kit. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/LiskHQ/lisk-sdk) (👨‍💻 98 · 🔀 480 · 📦 190 · 📋 4.6K - 6% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/LiskHQ/lisk-sdk
	```
</details>
<details><summary><b><a href="https://github.com/LiskHQ/lisk-desktop">lisk-desktop</a></b> (🥇26 ·  ⭐ 580) - Lisk graphical user interface for desktop. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/LiskHQ/lisk-desktop) (👨‍💻 68 · 🔀 92 · 📥 530K · 📋 2.7K - 5% open · ⏱️ 29.12.2022):

	```
	git clone https://github.com/LiskHQ/lisk-desktop
	```
</details>

<br>

 _4 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/tronprotocol">TRX - TRON</a></b> (🥈34 ·  ⭐ 5.6K) - What is Tron? Trons mission is to build a truly decentralized internet and..</summary>


---
<details><summary><b><a href="https://github.com/tronprotocol/java-tron">java-tron</a></b> (🥇30 ·  ⭐ 3.3K) - Java implementation of the Tron whitepaper. <code><a href="http://bit.ly/37RvQcA">LGPL-3.0</a></code></summary>

- [GitHub](https://github.com/tronprotocol/java-tron) (👨‍💻 240 · 🔀 1.2K · 📥 92K · 📋 1.1K - 3% open · ⏱️ 02.02.2023):

	```
	git clone https://github.com/tronprotocol/java-tron
	```
</details>
<details><summary><b><a href="https://github.com/tronprotocol/tronweb">tronweb</a></b> (🥇28 ·  ⭐ 260) - Javascript API Library for interacting with the TRON Network. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/tronprotocol/tronweb) (👨‍💻 47 · 🔀 200 · 📦 2.7K · 📋 160 - 3% open · ⏱️ 30.01.2023):

	```
	git clone https://github.com/tronprotocol/tronweb
	```
</details>
<details><summary><b><a href="https://github.com/tronprotocol/wallet-cli">wallet-cli</a></b> (🥈20 ·  ⭐ 420) - Wallet CLI. <code><a href="http://bit.ly/37RvQcA">LGPL-3.0</a></code></summary>

- [GitHub](https://github.com/tronprotocol/wallet-cli) (👨‍💻 86 · 🔀 340 · 📥 4K · 📋 130 - 8% open · ⏱️ 16.01.2023):

	```
	git clone https://github.com/tronprotocol/wallet-cli
	```
</details>
<details><summary><b><a href="https://github.com/tronprotocol/documentation">tronprotocol/documentation</a></b> (🥈16 ·  ⭐ 440 · 💤) - Documentations of project TRON. <code><a href="http://bit.ly/37RvQcA">LGPL-3.0</a></code></summary>

- [GitHub](https://github.com/tronprotocol/documentation) (👨‍💻 57 · 🔀 480 · 📋 94 - 25% open · ⏱️ 28.06.2022):

	```
	git clone https://github.com/tronprotocol/documentation
	```
</details>
<details><summary><b><a href="https://github.com/tronprotocol/sun-network">sun-network</a></b> (🥈15 ·  ⭐ 300 · 💤) -  <code><a href="http://bit.ly/37RvQcA">LGPL-3.0</a></code></summary>

- [GitHub](https://github.com/tronprotocol/sun-network) (👨‍💻 27 · 🔀 110 · 📥 670 · 📋 89 - 42% open · ⏱️ 29.03.2022):

	```
	git clone https://github.com/tronprotocol/sun-network
	```
</details>

<br>

 _9 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/maticnetwork">MATIC - Polygon</a></b> (🥈34 ·  ⭐ 3.1K) - Hybrid POS-Plasma Layer 2, bringing scale to Ethereum. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/maticnetwork/bor">bor</a></b> (🥇29 ·  ⭐ 530) - Official repository for the Matic Blockchain. <code><a href="http://bit.ly/37RvQcA">LGPL-3.0</a></code></summary>

- [GitHub](https://github.com/maticnetwork/bor) (👨‍💻 680 · 🔀 320 · 📥 23K · 📦 63 · 📋 260 - 13% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/maticnetwork/bor
	```
</details>
<details><summary><b><a href="https://github.com/maticnetwork/matic.js">matic.js</a></b> (🥇25 ·  ⭐ 440) - Javascript developer library to interact with Matic Network. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/maticnetwork/matic.js) (👨‍💻 39 · 🔀 200 · 📦 1.2K · 📋 80 - 11% open · ⏱️ 31.01.2023):

	```
	git clone https://github.com/maticnetwork/matic.js
	```
</details>
<details><summary><b><a href="https://github.com/maticnetwork/pos-portal">pos-portal</a></b> (🥈17 ·  ⭐ 230) - Smart contracts that powers the PoS (proof-of-stake) based bridge.. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/maticnetwork/pos-portal) (👨‍💻 14 · 🔀 190 · 📦 28 · 📋 33 - 78% open · ⏱️ 10.11.2022):

	```
	git clone https://github.com/maticnetwork/pos-portal
	```
</details>

<br>

 _15 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/neo-project">NEO</a></b> (🥈34 ·  ⭐ 2.7K) - Next generation smart economy platform (formerly Antshares) and Chinas first open..</summary>


---
<details><summary><b><a href="https://github.com/CityOfZion/neon-wallet">neon-wallet</a></b> (🥇27 ·  ⭐ 1.1K) - Light wallet for the NEO blockchain. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/CityOfZion/neon-wallet) (👨‍💻 82 · 🔀 310 · 📥 3M · 📋 1.4K - 4% open · ⏱️ 18.01.2023):

	```
	git clone https://github.com/CityOfZion/neon-wallet
	```
</details>
<details><summary><b><a href="https://github.com/neo-project/neo-node">neo-node</a></b> (🥇24 ·  ⭐ 220) -  <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/neo-project/neo-node) (👨‍💻 45 · 🔀 220 · 📥 100K · 📦 19 · 📋 470 - 16% open · ⏱️ 02.02.2023):

	```
	git clone https://github.com/neo-project/neo-node
	```
</details>

<br>

 _11 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/harmony-one">ONE - Harmony</a></b> (🥈31 ·  ⭐ 2K) - Harmonys open, decentralized network is enabled through the use of the..</summary>


---
<details><summary><b><a href="https://github.com/harmony-one/harmony">harmony</a></b> (🥇29 ·  ⭐ 1.5K) - The core protocol of harmony. <code><a href="http://bit.ly/37RvQcA">LGPL-3.0</a></code></summary>

- [GitHub](https://github.com/harmony-one/harmony) (👨‍💻 92 · 🔀 280 · 📥 110K · 📦 76 · 📋 1.2K - 18% open · ⏱️ 30.01.2023):

	```
	git clone https://github.com/harmony-one/harmony
	```
</details>

<br>

 _11 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/multiversx">EGLD - MultiversX</a></b> (🥈31 ·  ⭐ 1.6K) - MultiversX (formerly Elrond) is a technology ecosystem for.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/multiversx/mx-chain-go">mx-chain-go</a></b> (🥇25 ·  ⭐ 880) - The official implementation of the MultiversX blockchain.. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/multiversx/mx-chain-go) (👨‍💻 52 · 🔀 180 · 📥 380 · 📦 110 · 📋 460 - 12% open · ⏱️ 01.02.2023):

	```
	git clone https://github.com/multiversx/mx-chain-go
	```
</details>

<br>

 _12 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/hashgraph">HBAR - Hedera</a></b> (🥈31 ·  ⭐ 1.2K) - Decentralized public network where developers can build secure, fair..</summary>


---
<details><summary><b><a href="https://github.com/hashgraph/hedera-sdk-js">hedera-sdk-js</a></b> (🥇24 ·  ⭐ 210) - Hedera Hashgraph SDK for JavaScript/TypeScript. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/hashgraph/hedera-sdk-js) (👨‍💻 32 · 🔀 99 · 📦 800 · 📋 610 - 11% open · ⏱️ 06.02.2023):

	```
	git clone https://github.com/hashgraph/hedera-sdk-js
	```
</details>

<br>

 _11 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/oasislabs">ROSE - Oasis Network</a></b> (🥈31 ·  ⭐ 620) - Cryptocurrency launched in 2020and operates on the.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/oasisprotocol/oasis-core">oasis-core</a></b> (🥇26 ·  ⭐ 290) - Performant and Confidentiality-Preserving Smart Contracts +.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/oasisprotocol/oasis-core) (👨‍💻 44 · 🔀 88 · 📥 35K · 📋 1.8K - 15% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/oasisprotocol/oasis-core
	```
</details>

<br>

 _6 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/ethereumclassic">ETC - Ethereum Classic</a></b> (🥈30 ·  ⭐ 910) - Cryptocurrency . Users are able to generate ETC.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/etclabscore/core-geth">core-geth</a></b> (🥇25 ·  ⭐ 220) - A highly configurable Go implementation of the Ethereum protocol. <code><a href="http://bit.ly/37RvQcA">LGPL-3.0</a></code></summary>

- [GitHub](https://github.com/etclabscore/core-geth) (👨‍💻 780 · 🔀 100 · 📥 100K · 📦 7 · 📋 190 - 26% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/etclabscore/core-geth
	```
</details>

<br>

 _7 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/waves-enterprise">WAVES</a></b> (🥈29 ·  ⭐ 740) - Open blockchain protocol and development toolset for Web 3.0 applications.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _7 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/Zilliqa">ZIL - Zilliqa</a></b> (🥈27 ·  ⭐ 1.6K) - The highly scalable and secure public blockchain platform. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/Zilliqa/Zilliqa">Zilliqa</a></b> (🥇23 ·  ⭐ 1.1K) - Zilliqa is the worlds first high-throughput public blockchain.. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/Zilliqa/Zilliqa) (👨‍💻 80 · 🔀 260 · 📋 380 - 30% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/Zilliqa/Zilliqa
	```
</details>
<details><summary><b><a href="https://github.com/Zilliqa/scilla">scilla</a></b> (🥈20 ·  ⭐ 240) - Scilla - A Smart Contract Intermediate Level Language. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/Zilliqa/scilla) (👨‍💻 38 · 🔀 71 · 📥 320 · 📋 400 - 31% open · ⏱️ 01.02.2023):

	```
	git clone https://github.com/Zilliqa/scilla
	```
</details>

<br>

 _4 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/ontio">ONT - Ontology</a></b> (🥈27 ·  ⭐ 1.2K) - New high-performance public blockchain project & a distributed trust..</summary>


---
<details><summary><b><a href="https://github.com/ontio/ontology">ontology</a></b> (🥇26 ·  ⭐ 800) - Official Go implementation of the Ontology protocol. https://dev-.. <code><a href="http://bit.ly/37RvQcA">LGPL-3.0</a></code></summary>

- [GitHub](https://github.com/ontio/ontology) (👨‍💻 73 · 🔀 320 · 📥 19K · 📦 220 · 📋 260 - 24% open · ⏱️ 11.01.2023):

	```
	git clone https://github.com/ontio/ontology
	```
</details>

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/DeFiCh">DFI - DeFiChain</a></b> (🥈27 ·  ⭐ 660) - Decentralized blockchain platform dedicated to enable fast,.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/DeFiCh/ain">ain</a></b> (🥇27 ·  ⭐ 360) - DeFi Blockchain - enabling decentralized finance on Bitcoin. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/DeFiCh/ain) (👨‍💻 830 · 🔀 100 · 📥 34K · 📋 420 - 25% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/DeFiCh/ain
	```
</details>

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/KomodoPlatform">KMD - Komodo</a></b> (🥈27 ·  ⭐ 500) - End-to-end blockchain infrastructure solutions provider... <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/KomodoPlatform/atomicDEX-Desktop">atomicDEX-Desktop</a></b> (🥇24 ·  ⭐ 220) - atomicDEX Desktop app - project codename Dextop. <code><a href="http://bit.ly/2KucAZR">GPL-2.0</a></code></summary>

- [GitHub](https://github.com/KomodoPlatform/atomicDEX-Desktop) (👨‍💻 44 · 🔀 140 · 📥 59K · 📋 830 - 20% open · ⏱️ 09.01.2023):

	```
	git clone https://github.com/KomodoPlatform/atomicDEX-Desktop
	```
</details>

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/casper-ecosystem">CSPR - Casper Network</a></b> (🥈27 ·  ⭐ 480) - First live proof-of-stake blockchain built off the Casper CBC..</summary>


---
<details><summary><b><a href="https://github.com/casper-network/casper-node">casper-node</a></b> (🥇29 ·  ⭐ 330) - Casper node. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/casper-network/casper-node) (👨‍💻 49 · 🔀 170 · 📥 1.1K · 📦 170 · 📋 1.2K - 17% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/casper-network/casper-node
	```
</details>

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/icon-project">ICX - ICON</a></b> (🥈27 ·  ⭐ 410) - ICONs goal is to hyperconnect the world using blockchain.</summary>


---

<br>

 _9 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/poanetwork">POA - POA Network</a></b> (🥈26 ·  ⭐ 1.7K) - Cryptocurrency launched in 2017. POA Network has a current.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/poanetwork/solidity-flattener">solidity-flattener</a></b> (🥈16 ·  ⭐ 260) - Utility to combine Solidity project to a flat file. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/poanetwork/solidity-flattener) (👨‍💻 6 · 🔀 73 · 📦 450 · 📋 29 - 37% open · ⏱️ 08.08.2022):

	```
	git clone https://github.com/poanetwork/solidity-flattener
	```
</details>

<br>

 _11 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/radixdlt">XRD - Radix</a></b> (🥈26 ·  ⭐ 680) - Radix is building an open, interconnected platform where the full range of..</summary>


---
<details><summary><b><a href="https://github.com/radixdlt/radixdlt-scrypto">radixdlt-scrypto</a></b> (🥈20 ·  ⭐ 340) - Scrypto is the asset-oriented smart contract.. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/radixdlt/radixdlt-scrypto) (👨‍💻 33 · 🔀 87 · 📋 88 - 37% open · ⏱️ 20.01.2023):

	```
	git clone https://github.com/radixdlt/radixdlt-scrypto
	```
</details>

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/vitelabs">VITE</a></b> (🥈26 ·  ⭐ 600) - Vite has built the worlds first DAG (Directed Acyclic Graph) based smart-.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/vitelabs/go-vite">go-vite</a></b> (🥇23 ·  ⭐ 310) - Official Go implementation of the Vite protocol. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/vitelabs/go-vite) (👨‍💻 37 · 🔀 140 · 📥 60K · 📦 3 · 📋 81 - 28% open · ⏱️ 19.12.2022):

	```
	git clone https://github.com/vitelabs/go-vite
	```
</details>

<br>

 _7 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/PureStake">MOVR - Moonriver</a></b> (🥉25 ·  ⭐ 970) - Fully Ethereum-compatible smart contract parachain on Kusama. Due..</summary>


---
<details><summary><b><a href="https://github.com/PureStake/moonbeam">moonbeam</a></b> (🥇26 ·  ⭐ 780) - An Ethereum-compatible smart contract parachain on Polkadot. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/PureStake/moonbeam) (👨‍💻 38 · 🔀 240 · 📥 17K · 📦 17 · 📋 300 - 9% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/PureStake/moonbeam
	```
</details>

<br>

 _4 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/AstarNetwork">SDN - Shiden Network</a></b> (🥉25 ·  ⭐ 840) - Multi-chain decentralized application layer on Kusama Network...</summary>


---
<details><summary><b><a href="https://github.com/AstarNetwork/Astar">Astar</a></b> (🥇22 ·  ⭐ 610) - The dApp hub for blockchains of the future. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/AstarNetwork/Astar) (👨‍💻 23 · 🔀 150 · 📥 14K · 📋 210 - 4% open · ⏱️ 31.01.2023):

	```
	git clone https://github.com/AstarNetwork/Astar
	```
</details>

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/OmniLayer">OMNI</a></b> (🥉24 ·  ⭐ 1.6K) - Digital currency built on top of the Bitcoin blockchain. Its features include the..</summary>


---
<details><summary><b><a href="https://github.com/OmniLayer/omniwallet">omniwallet</a></b> (🥈18 ·  ⭐ 310 · 💤) - Omni Protocol Hybrid Web-Wallet. <code><a href="http://bit.ly/3pwmjO5">AGPL-3.0</a></code></summary>

- [GitHub](https://github.com/OmniLayer/omniwallet) (👨‍💻 33 · 🔀 190 · 📋 770 - 30% open · ⏱️ 19.05.2022):

	```
	git clone https://github.com/OmniLayer/omniwallet
	```
</details>

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/stratisproject">STRAX - Stratis</a></b> (🥉24 ·  ⭐ 850) - Powerful and flexible Blockchain Development Platform designed for..</summary>


---

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/CosmosContracts">JUNO</a></b> (🥉24 ·  ⭐ 480) - Global, open source, permission-less network for decentralized interoperable.. <code><img src="https://cosmos.network/favicon.ico" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/CosmosContracts/juno">CosmosContracts/juno</a></b> (🥇23 ·  ⭐ 260) - Open Source Platform for Interoperable Smart Contracts. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/CosmosContracts/juno) (👨‍💻 28 · 🔀 120 · 📥 2.7K · 📦 3 · 📋 150 - 20% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/CosmosContracts/juno
	```
</details>

<br>

 _6 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/nebulasio">NAS - Nebulas</a></b> (🥉23 ·  ⭐ 1.2K · 💤) - Next generation public blockchain, aiming for a continuously.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/kadena-io">KDA - Kadena</a></b> (🥉23 ·  ⭐ 970) - Kadena is launching one of the worlds first true scalable blockchains..</summary>


---
<details><summary><b><a href="https://github.com/kadena-io/pact">pact</a></b> (🥇22 ·  ⭐ 540) - The Pact Smart Contract Language. <code><a href="http://bit.ly/3aKzpTv">BSD-3</a></code></summary>

- [GitHub](https://github.com/kadena-io/pact) (👨‍💻 44 · 🔀 91 · 📥 5.4K · 📋 350 - 25% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/kadena-io/pact
	```
</details>
<details><summary><b><a href="https://github.com/kadena-io/chainweb-node">chainweb-node</a></b> (🥈20 ·  ⭐ 220) - Chainweb: A Proof-of-Work Parallel-Chain Architecture for.. <code><a href="http://bit.ly/3aKzpTv">BSD-3</a></code></summary>

- [GitHub](https://github.com/kadena-io/chainweb-node) (👨‍💻 30 · 🔀 81 · 📥 780 · 📋 210 - 48% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/kadena-io/chainweb-node
	```
</details>

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/tomochain">TOMO - TomoChain</a></b> (🥉21 ·  ⭐ 340) - Innovative solution to the scalability problem with the.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _6 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/signum-network">SIGNA - Signum</a></b> (🥉21 ·  ⭐ 330) - Cryptocurrency . Signum has a current supply of 2,130,801,392... <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/elastos">ELA - Elastos</a></b> (🥉21 ·  ⭐ 250) - Founded by OS expert Rong Chen, Elastos is building the blockchain.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/byteball">GBYTE - Obyte</a></b> (🥉20 ·  ⭐ 570) - Launched on Dec 25, 2016, Obyte is a distributed ledger based.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/byteball/obyte-gui-wallet">obyte-gui-wallet</a></b> (🥇23 ·  ⭐ 430) - Smart payments made simple. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/byteball/obyte-gui-wallet) (👨‍💻 36 · 🔀 180 · 📥 340K · 📋 260 - 34% open · ⏱️ 05.12.2022):

	```
	git clone https://github.com/byteball/obyte-gui-wallet
	```
</details>

<br>

 _1 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/Bytom">BTM - Bytom</a></b> (🥉19 ·  ⭐ 1.3K) - Blockchain protocol for financial and digital asset applications. Using..</summary>


---
<details><summary><b><a href="https://github.com/Bytom/bytom">bytom</a></b> (🥈21 ·  ⭐ 1.3K) - Official Go implementation of the Bytom protocol. <code><a href="http://bit.ly/3pwmjO5">AGPL-3.0</a></code></summary>

- [GitHub](https://github.com/Bytom/bytom) (👨‍💻 52 · 🔀 400 · 📥 61K · 📦 1 · 📋 420 - 4% open · ⏱️ 14.10.2022):

	```
	git clone https://github.com/Bytom/bytom
	```
</details>

---
</details>
<details><summary><b><a href="https://github.com/omgnetwork">OMG - OMG Network</a></b> (🥉19 ·  ⭐ 720 · 💤) - The value transfer layer for the Ethereum Network. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/lamden">TAU - Lamden</a></b> (🥉19 ·  ⭐ 210) - Performant Python-based blockchain platform that makes using and.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/CounterpartyXCP">XCP - Counterparty</a></b> (🥉17 ·  ⭐ 500) - Platform for user-created assets on Bitcoin. Its a protocol, set..</summary>


---
<details><summary><b><a href="https://github.com/CounterpartyXCP/counterparty-lib">counterparty-lib</a></b> (🥈21 ·  ⭐ 270) - Counterparty Protocol Reference Implementation. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/CounterpartyXCP/counterparty-lib) (👨‍💻 52 · 🔀 190 · 📋 660 - 14% open · ⏱️ 13.12.2022):

	```
	git clone https://github.com/CounterpartyXCP/counterparty-lib
	```
</details>

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/nuls-io">NULS</a></b> (🥉15 ·  ⭐ 460 · 💤) - Singaporean based project is trying to develop a highly adaptable.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary>Show 20 hidden projects...</summary>

- <b><a href="https://github.com/qtumproject">QTUM</a></b> (🥉22 ·  ⭐ 120) - Decentralized and open-source smart contracts platform and value transfer protocol...
- <b><a href="https://github.com/perlin-network">PERL - PERL.eco</a></b> (🥉20 ·  ⭐ 2K · 💀) - Tokenized real-world ecological assets Perlins latest.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/hicommonwealth">EDG - Edgeware</a></b> (🥉18 ·  ⭐ 180) - High-performance, self-upgrading WASM smart contract platform, in.. <code><img src="https://git.io/J9c3v" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/velas">VLX - Velas</a></b> (🥉18 ·  ⭐ 59) - Velas AG, headquartered in Switzerland, is a new AI-operated dPoS.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/truechain">TRUE - TrueChain</a></b> (🥉16 ·  ⭐ 150 · 💀) - With the digital advertising becoming a preserve for the.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/expanse-org">EXP - Expanse</a></b> (🥉14 ·  ⭐ 55 · 💀) - The Expanse platform now has a two-year history of consistent growth..
- <b><a href="https://github.com/bobanetwork">BOBA - Boba Network</a></b> (🥉14 ·  ⭐ 44) - Blockchain Layer-2 scaling solution and Hybrid Compute.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/ubiq">UBQ - Ubiq</a></b> (🥉13 ·  ⭐ 83 · 💤) - Decentralized platform which allows the creation and implementation.. <code><img src="https://git.io/J9cOx" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/idni">AGRS - Agoras: Currency of Tau</a></b> (🥉12 ·  ⭐ 110) - Application over Tau-Chain, being first and foremost.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/xtrabytes">XBY - XTRABYTES</a></b> (🥉11 ·  ⭐ 87) - XTRABYTES goes beyond being a currency. Its a next-gen blockchain..
- <b><a href="https://github.com/TheLindaProjectInc">MRX - Metrix Coin</a></b> (🥉11 ·  ⭐ 42) - Metrix Coin (formerly Lindacoin) is a Proof of Stake digital.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/coti-io">COTI</a></b> (🥉10 ·  ⭐ 120) - First enterprise-grade digital fintech platform, which eliminates all.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/xdaichain">STAKE - xDAI Stake</a></b> (🥉10 ·  ⭐ 120 · 💀) - New ERC20-type (implemented as an ERC677) token designed.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/Oneledger">OLT - OneLedger</a></b> (🥉10 ·  ⭐ 40 · 💤) - OneLedger enables you to focus building your business.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/thundercore">TT - ThunderCore</a></b> (🥉9 ·  ⭐ 93) - Secure, high-performance, EVM-compatible public blockchain with.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/lightstreams-network">PHT - Phoneum</a></b> (🥉9 ·  ⭐ 54 · 💀) - Decentralized network for building a new generation of applications...
- <b><a href="https://github.com/tezos">XTZ - Tezos</a></b> (🥉8 ·  ⭐ 1.5K · 💤) - Coin created by a former Morgan Stanley analyst, Arthur Breitman... <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/bankex">BKX - BANKEX</a></b> (🥉8 ·  ⭐ 33 · 💀) - Cryptocurrency and operates on the Ethereum platform. BANKEX has.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/nymtech">NYM</a></b> (🥉7 ·  ⭐ 61) - Existing internet protocols leak sensitive data that can be used without users.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/aergoio">AERGO</a></b> (🥉5 ·  ⭐ 79 · 💤) - Open platform that allows businesses to build innovative applications.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
</details>
<br>

## Stablecoins

<a href="#contents"><img align="right" width="15" height="15" src="https://git.io/JtehR" alt="Back to top"></a>

_Cryptocurrencies that are designed to minimize volatility by pegging to a more stable asset._

<details><summary><b><a href="https://github.com/centrehq">USDC - USD Coin</a></b> (🥇17 ·  ⭐ 540) - Fully collateralized US dollar stablecoin. USDC is the bridge.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary>Show 2 hidden projects...</summary>

- <b><a href="https://github.com/beanstalkfarms">BEAN</a></b> (🥉12 ·  ⭐ 88) - The practicality of using Decentralized Finance is limited by the lack of a.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/paxosglobal">BUSD - Binance USD</a></b> (🥉9 ·  ⭐ 300) - Stable coin pegged to USD that has received approval.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
</details>
<br>

## Decentralized Finance (DeFi)

<a href="#contents"><img align="right" width="15" height="15" src="https://git.io/JtehR" alt="Back to top"></a>

_Financial services that are built on top of distributed networks with no central intermediaries._

<details><summary><b><a href="https://github.com/0xProject">ZRX - 0x</a></b> (🥇33 ·  ⭐ 4K) - Cryptocurrency and operates on the Ethereum platform. 0x has a current.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/0xProject/protocol">0xProject/protocol</a></b> (🥈20 ·  ⭐ 270) -  <code>Unlicensed</code></summary>

- [GitHub](https://github.com/0xProject/protocol) (👨‍💻 130 · 🔀 150 · 📦 780 · 📋 25 - 28% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/0xProject/protocol
	```
</details>
<details><summary><b><a href="https://github.com/0xProject/0x-api">0x-api</a></b> (🥈19 ·  ⭐ 340) - An HTTP Interface to 0x liquidity and tooling. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/0xProject/0x-api) (👨‍💻 49 · 🔀 200 · 📋 83 - 61% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/0xProject/0x-api
	```
</details>

<br>

 _11 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/gnosis">GNO - Gnosis</a></b> (🥇32 ·  ⭐ 3.2K) - Cryptocurrency and operates on the Ethereum platform. Gnosis has a.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/gnosis/zodiac">zodiac</a></b> (🥈18 ·  ⭐ 360) - A library for composable DAO tooling built on top of programmable.. <code><a href="http://bit.ly/37RvQcA">LGPL-3.0</a></code></summary>

- [GitHub](https://github.com/gnosis/zodiac) (👨‍💻 18 · 🔀 88 · 📥 7 · 📦 150 · 📋 21 - 42% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/gnosis/zodiac
	```
</details>
<details><summary><b><a href="https://github.com/gnosis/ethcontract-rs">ethcontract-rs</a></b> (🥈18 ·  ⭐ 230) - Generate type-safe bindings for interacting with Ethereum.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/gnosis/ethcontract-rs) (👨‍💻 15 · 🔀 35 · 📦 120 · 📋 130 - 38% open · ⏱️ 28.11.2022):

	```
	git clone https://github.com/gnosis/ethcontract-rs
	```
</details>

<br>

 _22 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/makerdao">MKR - Maker</a></b> (🥇30 ·  ⭐ 5.6K) - Cryptocurrency depicted as a smart contract platform and works.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _34 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/aave">AAVE</a></b> (🥇30 ·  ⭐ 3.8K) - Decentralized money market protocol where users can lend and borrow.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/aave/aave-v3-core">aave-v3-core</a></b> (🥇25 ·  ⭐ 480) - This repository contains the core smart contracts of the.. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/aave/aave-v3-core) (👨‍💻 23 · 🔀 300 · 📦 450 · 📋 370 - 6% open · ⏱️ 31.01.2023):

	```
	git clone https://github.com/aave/aave-v3-core
	```
</details>
<details><summary><b><a href="https://github.com/aave/protocol-v2">protocol-v2</a></b> (🥈21 ·  ⭐ 550) - Aave Protocol V2. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/aave/protocol-v2) (👨‍💻 30 · 🔀 610 · 📦 1.1K · 📋 180 - 70% open · ⏱️ 11.01.2023):

	```
	git clone https://github.com/aave/protocol-v2
	```
</details>
<details><summary><b><a href="https://github.com/aave/interface">aave/interface</a></b> (🥈20 ·  ⭐ 210) - An open source interface for the decentralized liquidity.. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/aave/interface) (👨‍💻 32 · 🔀 180 · 📥 7 · 📋 510 - 23% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/aave/interface
	```
</details>

<br>

 _12 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/AugurProject">REP - Augur</a></b> (🥇28 ·  ⭐ 2K) - Trustless, decentralized platform for prediction markets. Augur is an.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _8 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/Synthetixio">SNX - Synthetix</a></b> (🥇28 ·  ⭐ 1.9K) - Cryptocurrency and operates on the Ethereum platform... <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/Synthetixio/synthetix">synthetix</a></b> (🥇26 ·  ⭐ 1K) - Synthetix Solidity smart contracts. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/Synthetixio/synthetix) (👨‍💻 47 · 🔀 550 · 📦 450 · 📋 130 - 67% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/Synthetixio/synthetix
	```
</details>
<details><summary><b><a href="https://github.com/Synthetixio/synpress">synpress</a></b> (🥇22 ·  ⭐ 310) - Synpress is e2e testing framework based on Cypress.io and playwright with.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/Synthetixio/synpress) (👨‍💻 20 · 🔀 84 · 📦 140 · 📋 120 - 28% open · ⏱️ 04.02.2023):

	```
	git clone https://github.com/Synthetixio/synpress
	```
</details>

<br>

 _7 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/compound-finance">COMP - Compound</a></b> (🥇26 ·  ⭐ 2.7K) - The Compound Governance Token is a governance token on the.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/compound-finance/compound-protocol">compound-protocol</a></b> (🥈18 ·  ⭐ 1.6K · 💤) - The Compound On-Chain Protocol. <code><a href="http://bit.ly/3aKzpTv">BSD-3</a></code></summary>

- [GitHub](https://github.com/compound-finance/compound-protocol) (👨‍💻 20 · 🔀 1.1K · 📥 310 · 📋 85 - 64% open · ⏱️ 07.06.2022):

	```
	git clone https://github.com/compound-finance/compound-protocol
	```
</details>

<br>

 _9 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/yearn">YFI - yearn.finance</a></b> (🥇26 ·  ⭐ 2.5K) - Suite of products in Decentralized Finance (DeFi) that.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/yearn/yearn-vaults">yearn-vaults</a></b> (🥈17 ·  ⭐ 470) - Yearn Vault smart contracts. <code><a href="http://bit.ly/3pwmjO5">AGPL-3.0</a></code></summary>

- [GitHub](https://github.com/yearn/yearn-vaults) (👨‍💻 42 · 🔀 280 · 📋 150 - 7% open · ⏱️ 19.01.2023):

	```
	git clone https://github.com/yearn/yearn-vaults
	```
</details>

<br>

 _19 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/consenlabs">LON - Tokenlon</a></b> (🥇24 ·  ⭐ 1K) - Utility token issued by the Tokenlon DEX, used to align all.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/consenlabs/token-profile">token-profile</a></b> (🥇22 ·  ⭐ 710) - Blockchain coin and token profile collection. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/consenlabs/token-profile) (👨‍💻 1.7K · 🔀 3.6K · 📋 660 - 5% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/consenlabs/token-profile
	```
</details>
<details><summary><b><a href="https://github.com/consenlabs/token-core">token-core</a></b> (🥈15 ·  ⭐ 250) - Next generation core inside imToken Wallet. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/consenlabs/token-core) (👨‍💻 9 · 🔀 92 · 📥 120 · 📋 34 - 55% open · ⏱️ 17.09.2022):

	```
	git clone https://github.com/consenlabs/token-core
	```
</details>

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/bandprotocol">BAND - Band Protocol</a></b> (🥇24 ·  ⭐ 600) - Cryptocurrency and operates on the Ethereum platform... <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/traderjoe-xyz">JOE</a></b> (🥇23 ·  ⭐ 330) - Trader Joe is your one-stop decentralized trading platform on the Avalanche.. <code><img src="https://git.io/J9cOb" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/electroneum">ETN - Electroneum</a></b> (🥈22 ·  ⭐ 420) - Electroneum has coined the phrase enablement currency as it is..</summary>


---
<details><summary><b><a href="https://github.com/electroneum/electroneum">electroneum</a></b> (🥇22 ·  ⭐ 380) - Electroneum: the secure, humanitarian, mobile cryptocurrency. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/electroneum/electroneum) (👨‍💻 290 · 🔀 190 · 📥 630K · 📋 390 - 25% open · ⏱️ 01.02.2023):

	```
	git clone https://github.com/electroneum/electroneum
	```
</details>

<br>

 _1 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/neptune-mutual-blue">NPM - Neptune Mutual</a></b> (🥈21 ·  ⭐ 1.4K) - Neptune mutual follows a parametric insurance model, meaning..</summary>


---
<details><summary><b><a href="https://github.com/neptune-mutual-blue/app.neptunemutual.com">app.neptunemutual.com</a></b> (🥈15 ·  ⭐ 630) - An open source interface for the Neptune Mutual App. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/neptune-mutual-blue/app.neptunemutual.com) (👨‍💻 23 · 🔀 32 · ⏱️ 06.02.2023):

	```
	git clone https://github.com/neptune-mutual-blue/app.neptunemutual.com
	```
</details>
<details><summary><b><a href="https://github.com/neptune-mutual-blue/sdk">neptune-mutual-blue/sdk</a></b> (🥈15 ·  ⭐ 220) -  <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/neptune-mutual-blue/sdk) (👨‍💻 9 · 🔀 19 · 📦 11 · ⏱️ 18.01.2023):

	```
	git clone https://github.com/neptune-mutual-blue/sdk
	```
</details>

<br>

 _1 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/UMAprotocol">UMA</a></b> (🥈21 ·  ⭐ 360) - Decentralized financial contracts platform built to enable Universal Market.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/UMAprotocol/protocol">UMAprotocol/protocol</a></b> (🥇23 ·  ⭐ 310) - UMA Protocol Running on Ethereum. <code><a href="http://bit.ly/3pwmjO5">AGPL-3.0</a></code></summary>

- [GitHub](https://github.com/UMAprotocol/protocol) (👨‍💻 47 · 🔀 160 · 📦 110 · 📋 970 - 10% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/UMAprotocol/protocol
	```
</details>

<br>

 _1 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/enzymefinance">MLN - Enzyme</a></b> (🥈20 ·  ⭐ 1.5K) - A fast & cost-effective way to build, scale and monetize.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/enzymefinance/protocol">enzymefinance/protocol</a></b> (🥈21 ·  ⭐ 310) - Enzyme Protocol Implementation. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/enzymefinance/protocol) (👨‍💻 31 · 🔀 120 · 📥 270 · 📦 14 · ⏱️ 15.12.2022):

	```
	git clone https://github.com/enzymefinance/protocol
	```
</details>

<br>

 _1 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/OlympusDAO">OHM - Olympus</a></b> (🥈20 ·  ⭐ 1K) - The Decentralized Reserve Currency. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/OlympusDAO/olympus-frontend">olympus-frontend</a></b> (🥈21 ·  ⭐ 270) -  <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/OlympusDAO/olympus-frontend) (👨‍💻 60 · 🔀 700 · 📋 280 - 17% open · ⏱️ 03.02.2023):

	```
	git clone https://github.com/OlympusDAO/olympus-frontend
	```
</details>

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/pooltogether">POOL - PoolTogether</a></b> (🥈20 ·  ⭐ 680) - Cryptocurrency and operates on the Ethereum platform... <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/pooltogether/pooltogether-pool-contracts">pooltogether-pool-contracts</a></b> (🥈15 ·  ⭐ 360) - PoolTogether prize-linked savings game Solidity smart.. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/pooltogether/pooltogether-pool-contracts) (👨‍💻 13 · 🔀 160 · 📦 160 · 📋 14 - 21% open · ⏱️ 08.08.2022):

	```
	git clone https://github.com/pooltogether/pooltogether-pool-contracts
	```
</details>

<br>

 _4 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/instadapp">INST - Instadapp</a></b> (🥈20 ·  ⭐ 380) - On Instadapp, users and developers manage and build their DeFi.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/VenusProtocol">XVS - Venus</a></b> (🥈20 ·  ⭐ 220) - Cryptocurrency and operates on the BNB Smart Chain (BEP20) platform... <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/erasureprotocol">NMR - Numeraire</a></b> (🥈19 ·  ⭐ 960) - Cryptocurrency and operates on the Ethereum platform. Numeraire.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/beefyfinance">BIFI - Beefy.Finance</a></b> (🥈19 ·  ⭐ 470) - $BIFI tokens are dividend-eligible revenue shares in.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _4 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/Anchor-Protocol">ANC - Anchor Protocol</a></b> (🥈19 ·  ⭐ 370 · 💤) - Cryptocurrency launched in 2021and operates on the.. <code><img src="https://git.io/J9cOg" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/liquity">LQTY - Liquity</a></b> (🥈19 ·  ⭐ 300) - Token that captures the fee revenue generated by the Liquity.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/liquity/dev">liquity/dev</a></b> (🥈19 ·  ⭐ 210) - Liquity monorepo containing the contracts, SDK and Dev UI frontend. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/liquity/dev) (👨‍💻 15 · 🔀 210 · 📦 20 · 📋 260 - 18% open · ⏱️ 19.01.2023):

	```
	git clone https://github.com/liquity/dev
	```
</details>

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/diadata-org">DIA</a></b> (🥈19 ·  ⭐ 210) - Cryptocurrency and operates on the Ethereum platform. DIA has a current supply.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/diadata-org/diadata">diadata</a></b> (🥈21 ·  ⭐ 210) - DIAdata.org platform. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/diadata-org/diadata) (👨‍💻 66 · 🔀 120 · 📦 9 · 📋 260 - 4% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/diadata-org/diadata
	```
</details>

---
</details>
<details><summary><b><a href="https://github.com/portto">BLT - Bloom</a></b> (🥈18 ·  ⭐ 520) - End-to-end protocol for identity attestation, risk assessment and.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/api3dao">API3</a></b> (🥈18 ·  ⭐ 230) - API3 builds blockchain-native, decentralized APIs with DAO-governance and.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/tornadocash">TORN - Tornado Cash</a></b> (🥈16 ·  ⭐ 1.7K) - Cryptocurrency and operates on the Ethereum platform... <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/tornadocash/tornado-core">tornado-core</a></b> (🥈17 ·  ⭐ 1.1K · 💤) - Tornado cash. Non-custodial private transactions on.. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/tornadocash/tornado-core) (👨‍💻 12 · 🔀 410 · 📥 11K · 📋 46 - 47% open · ⏱️ 24.03.2022):

	```
	git clone https://github.com/tornadocash/tornado-core
	```
</details>

<br>

 _6 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/fei-protocol">TRIBE</a></b> (🥈16 ·  ⭐ 480) - Governance token which will allow community members to vote on the future.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/SetProtocol">DPI - DeFi Pulse Index</a></b> (🥈16 ·  ⭐ 360 · 💤) - Cryptocurrency launched in 2020and operates on the.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/PolymathNetwork">POLY - Polymath</a></b> (🥈16 ·  ⭐ 320 · 💤) - Polymath provides technology to create, issue, and manage.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/PolymathNetwork/polymath-core">polymath-core</a></b> (🥈19 ·  ⭐ 320 · 💤) - Core Ethereum Smart Contracts for Polymath - The.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/PolymathNetwork/polymath-core) (👨‍💻 36 · 🔀 130 · 📥 9 · 📦 17 · 📋 170 - 26% open · ⏱️ 26.05.2022):

	```
	git clone https://github.com/PolymathNetwork/polymath-core
	```
</details>

---
</details>
<details><summary><b><a href="https://github.com/Rari-Capital">RGT - Rari Governance Token</a></b> (🥈15 ·  ⭐ 1K) - The Rari Governance Token is the native token behind.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _7 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/jet-lab">JET - Jetcoin</a></b> (🥈15 ·  ⭐ 250) - Jet Protocol is highly capital efficient DeFi borrowing and.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOa" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/Mirror-Protocol">MIR - Mirror Protocol</a></b> (🥈15 ·  ⭐ 220 · 💤) - What are Mirrored Assets? MIR is the governance token.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary>Show 87 hidden projects...</summary>

- <b><a href="https://github.com/pangolindex">PNG - Pangolin</a></b> (🥇23 ·  ⭐ 180) - Community-driven decentralized exchange for Avalanche and.. <code><img src="https://git.io/J9cOb" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/cowprotocol">COW - CoW Protocol</a></b> (🥇23 ·  ⭐ 180) - Earn by just holding!. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/InjectiveLabs">INJ - Injective Protocol</a></b> (🥈21 ·  ⭐ 110) - Cryptocurrency and operates on the Ethereum platform... <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/reddcoin-project">RDD - ReddCoin</a></b> (🥈19 ·  ⭐ 280 · 💀) - Launched in 2014 as a fork of Litecoin, Reddcoin (RDD) is a..
- <b><a href="https://github.com/ConcealNetwork">CCX - Conceal</a></b> (🥈19 ·  ⭐ 170) - Privacy Protected DeFi & Encrypted Communications.
- <b><a href="https://github.com/depayfi">DEPAY</a></b> (🥈17 ·  ⭐ 160) - DePay pioneers Web3 Payments with the power of DeFi. Driving mass adoption.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/X-CASH-official">XCASH - X-CASH</a></b> (🥈17 ·  ⭐ 120) - Open-source private cryptocurrency with unique features developed for..
- <b><a href="https://github.com/xinfinorg">XDC - XDC Network</a></b> (🥈17 ·  ⭐ 110) - Enterprise-ready hybrid Blockchain technology company.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/pillarwallet">PLR - Pillar</a></b> (🥈17 ·  ⭐ 100) - Community-run, multichain & non-custodial DeFi wallet governed by.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/alpaca-finance">ALPACA - Alpaca Finance</a></b> (🥈16 ·  ⭐ 200) - First leveraged yield farming protocol on Binance.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/Cryptorubic">RBC - Rubic</a></b> (🥈16 ·  ⭐ 50) - No description. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/NexusMutual">NXM</a></b> (🥈15 ·  ⭐ 120) - Decentralized insurance protocol built on Ethereum that currently offers cover.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/yam-finance">YAM</a></b> (🥈14 ·  ⭐ 730) - Cryptocurrency and operates on the Ethereum platform. YAM V3 has a current.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/trusttoken">TRU - TrueFi</a></b> (🥈14 ·  ⭐ 340) - DeFiuncollateralized lending protocol developed by the TrustToken.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/pickle-finance">PICKLE - Pickle Finance</a></b> (🥈14 ·  ⭐ 330) - Yield aggregator that compounds LPs across various.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/ribbon-finance">RBN - Ribbon Finance</a></b> (🥈14 ·  ⭐ 320) - New protocol that helps users access crypto structured.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/fuseio">FUSE</a></b> (🥈14 ·  ⭐ 300) - Open-source, democratized, borderless money. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/solendprotocol">SLND - Solend</a></b> (🥈14 ·  ⭐ 280) - Algorithmic, decentralized protocol for lending and borrowing on.. <code><img src="https://git.io/J9cOa" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/bifrost-finance">BNC - Bifrost Native Coin</a></b> (🥈14 ·  ⭐ 190) - Polkadot Ecological DeFi basic protocol. It is.. <code><img src="https://git.io/J9c3v" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/risevision">RISE - EverRise</a></b> (🥈14 ·  ⭐ 75 · 💀) - Blockchain technology company focused on increasing.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/NEST-Protocol">NEST - Nest Protocol</a></b> (🥉13 ·  ⭐ 980) - Decentralized price fact Oracle protocol network based on.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/EverexIO">EVX - Everex</a></b> (🥉13 ·  ⭐ 840) - Everex enables you to transfer, borrow, and trade in any fiat.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/maple-labs">MPL - Maple</a></b> (🥉13 ·  ⭐ 380) - Cryptocurrency launched in 2021and operates on the Ethereum platform... <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/nash-io">NEX - Nash</a></b> (🥉13 ·  ⭐ 360 · 💤) - Easiest place to grow your wealth. Manage your money alongside crypto..
- <b><a href="https://github.com/reserve-protocol">RSR - Reserve Rights</a></b> (🥉13 ·  ⭐ 87) - Reserve aims to build a stable, decentralized, asset-.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/fraxfinance">FXS - Frax Share</a></b> (🥉12 ·  ⭐ 380) - Governance and value accrual token of the Frax Stablecoin.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/harvest-finance">FARM - Harvest Finance</a></b> (🥉12 ·  ⭐ 370 · 💤) - Harvest automatically farms the highest yields in DeFi. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/ampleforth">AMPL - Ampleforth</a></b> (🥉12 ·  ⭐ 320) - Digital-asset-protocol for smart commodity-money. The smart.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/BarnBridge">BOND - BarnBridge</a></b> (🥉12 ·  ⭐ 160) - A Cross Platform Protocol for Tokenizing Risk. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/webdollar">WEBD - WebDollar</a></b> (🥉12 ·  ⭐ 120 · 💀) - Cryptocurrency fully native to the World Wide Web, entirely..
- <b><a href="https://github.com/SwissBorg">CHSB - SwissBorg</a></b> (🥉12 ·  ⭐ 64) - SwissBorg is decentralising wealth management by making it fun,.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/stafiprotocol">FIS - Stafi</a></b> (🥉12 ·  ⭐ 41) - First DeFi protocol unlocking liquidity of staked assets. Users.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9c3v" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/credmark">CMK - Credmark</a></b> (🥉12 ·  ⭐ 31) - Cryptocurrency launched in 2021and operates on the Ethereum.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/PancakeBunny-finance">BUNNY - Pancake Bunny</a></b> (🥉11 ·  ⭐ 180 · 💀) - Compound Yields on Binance Smart Chain with Bunny... <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/kava-labs">KAVA</a></b> (🥉11 ·  ⭐ 110 · 💀) - Cross-chain DeFi Hub for decentralized financial services and.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/snowblossomcoin">SNOW - SnowSwap</a></b> (🥉11 ·  ⭐ 69) - Cryptocurrency and operates on the Ethereum platform. SnowSwap.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/HathorNetwork">HTR - Hathor</a></b> (🥉11 ·  ⭐ 63) - Natural evolution of Bitcoins blockchain. Instead of arranging the..
- <b><a href="https://github.com/FUSIONFoundation">FSN - Fusion</a></b> (🥉11 ·  ⭐ 32) - Project which consists of an all-inclusive blockchain-based.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/alchemix-finance">ALCX - Alchemix</a></b> (🥉10 ·  ⭐ 240) - Governance token for the Alchemix protocol. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/indexed-finance">NDX - Indexed Finance</a></b> (🥉10 ·  ⭐ 170) - Project focused on the development of passive portfolio.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/Idle-Labs">IDLE</a></b> (🥉10 ·  ⭐ 67) - Governance Token of Idle Protocol. It allows community participants to vote on-.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/pie-dao">DOUGH - PieDAO DOUGH v2</a></b> (🥉10 ·  ⭐ 55 · 💀) - DOUGH is PieDAOs governance token. Anybody can be a.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/premian-labs">PREMIA</a></b> (🥉10 ·  ⭐ 50) - Next-generation options and meta-vaults. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/thales-markets">THALES</a></b> (🥉10 ·  ⭐ 40) - Ethereum protocol that allows the creation of peer-to-peer parimutuel.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/octofi">OCTO - OctoFi</a></b> (🥉10 ·  ⭐ 36 · 💀) - All-in-one DeFi and NFT with cashbacks. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/hector-network">HEC - Hector DAO</a></b> (🥉10 ·  ⭐ 31) - Cryptocurrency launched in 2022and operates on the.. <code><img src="https://git.io/J9cOb" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOi" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/Basis-Cash">BAC - Basis Cash</a></b> (🥉9 ·  ⭐ 260 · 💀) - Fairly distributed & censorship resistant stablecoin with.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/akropolisio">AKRO - Akropolis</a></b> (🥉9 ·  ⭐ 140 · 💤) - Akropolis mission is to give everyone the tools to save,.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/Badger-Finance">BADGER - Badger DAO</a></b> (🥉9 ·  ⭐ 100) - Badger DAO aims to create an ecosystem of DeFi products.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/rtoken-project">RDAI</a></b> (🥉9 ·  ⭐ 87 · 💤) - Cryptocurrency and operates on the Ethereum platform. rDAI has a current.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/laminar-protocol">LAMINAR</a></b> (🥉9 ·  ⭐ 86 · 💀) - ## What Is Laminar (Laminar)? Laminar is an open financial platform founded..
- <b><a href="https://github.com/tranchess">CHESS - Tranchess</a></b> (🥉9 ·  ⭐ 63) - Tokenized asset management and derivatives trading protocol... <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/dopex-io">DPX - Dopex</a></b> (🥉9 ·  ⭐ 41) - Dopex (Decentralized Options Exchange) is a decentralized options.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/tetu-io">TETU</a></b> (🥉9 ·  ⭐ 41 · 💤) - DeFi application built on Polygon that implements automated yield farming.. <code><img src="https://git.io/J9cOx" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/Abracadabra-money">SPELL - Spell Token</a></b> (🥉8 ·  ⭐ 200 · 💤) - Cryptocurrency and operates on the Ethereum platform... <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/NFTX-project">NFTX</a></b> (🥉8 ·  ⭐ 120) - NFT-backed index funds on Ethereum. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/CreamFi">CREAM</a></b> (🥉8 ·  ⭐ 110 · 💤) - Cryptocurrency launched in 2020and operates on the Ethereum platform... <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/Synthetify">SNY - Synthetify</a></b> (🥉8 ·  ⭐ 83 · 💤) - Upcoming synthetic assets platform, fully built on top of.. <code><img src="https://git.io/J9cOa" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/etherisc">DIP - Etherisc DIP Token</a></b> (🥉8 ·  ⭐ 78 · 💤) - Etherisc is building a platform for decentralized.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/tellor-io">TRB - Tellor</a></b> (🥉8 ·  ⭐ 48 · 💀) - Smart contracts on Ethereum are fully self contained and any.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/ProtonProtocol">XPR - Proton</a></b> (🥉8 ·  ⭐ 31) - Launched in San Francisco, Proton (XPR) is a new public blockchain.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/Linear-finance">LINA - Linear</a></b> (🥉8 ·  ⭐ 31) - A Cross-chain Decentralized Delta-One Asset Protocol with Unlimited.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/gammastrategies">GAMMA - Gamma Strategies</a></b> (🥉7 ·  ⭐ 420 · 💤) - Gamma offers non-custodial, automated, and active.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/step-finance">STEP - Step Finance</a></b> (🥉7 ·  ⭐ 120) - Front page of Solana. Visualise, Analyse, Execute and.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOa" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/88mphapp">MPH - 88mph</a></b> (🥉7 ·  ⭐ 77 · 💤) - No description. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/benddao">BEND - BendDAO</a></b> (🥉7 ·  ⭐ 77) - NFT liquidity and lending protocol with reactive interest rates for.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/unitprotocol">DUCK - Unit Protocol</a></b> (🥉7 ·  ⭐ 47) - Decentralized borrowing protocol that allows using a.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/yfii">YFII - DFI.money</a></b> (🥉7 ·  ⭐ 46 · 💀) - Fork of YFI project with YIP-8 implementation. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/sirenmarkets">SI - Siren</a></b> (🥉7 ·  ⭐ 42 · 💤) - Distributed protocol for creating, trading, and redeeming fully-.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/AlphaFinanceLab">ALPHA - Alpha Venture DAO</a></b> (🥉6 ·  ⭐ 110 · 💀) - Alpha Finance Lab is focused on researching and.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/InverseFinance">INV - Inverse Finance</a></b> (🥉6 ·  ⭐ 65 · 💀) - Yield aggregator and a lending protocol. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/OpiumProtocol">OPIUM</a></b> (🥉6 ·  ⭐ 39 · 💤) - Decentralized derivative protocol. It offers two products: USDT de-pegging.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/spectrumprotocol">SPEC - Spectrum Token</a></b> (🥉6 ·  ⭐ 38) - The first and innovative yield optimizer on Terra.. <code><img src="https://git.io/J9cOg" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/banklessdao">BED - Bankless BED Index</a></b> (🥉6 ·  ⭐ 38 · 💀) - Cryptocurrency and operates on the Ethereum platform... <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/yaxis-project">YAXIS</a></b> (🥉6 ·  ⭐ 32 · 💀) - Experiment in DAO-directed yield farming. Users will deposit stable coins.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/vesperfi">VSP - Vesper Finance</a></b> (🥉6 ·  ⭐ 31 · 💤) - Cryptocurrency launched in 2021and operates on the.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/nexofinance">NEXO</a></b> (🥉5 ·  ⭐ 95 · 💀) - Asset-backed token and is backed by the underlying assets of Nexos loan.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/visorfinance">VISR - Visor</a></b> (🥉5 ·  ⭐ 53 · 💀) - Interact with DeFi protocols through an NFT Enhancing the.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/powerpool-finance">CVP - PowerPool Concentrated Voting Power</a></b> (🥉5 ·  ⭐ 52 · 💀) - Cryptocurrency and operates on the Ethereum platform... <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/mtpelerin">MPS - Mt Pelerin Shares</a></b> (🥉5 ·  ⭐ 40) - MPS tokens are the tokenized shares of Mt Pelerin Group.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/saffron-finance">SFI - saffron.finance</a></b> (🥉5 ·  ⭐ 39 · 💀) - Cryptocurrency and operates on the Ethereum platform... <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/defidollar">DFD - DefiDollar DAO</a></b> (🥉4 ·  ⭐ 48 · 💤) - DFD is DefiDollars governance token in which holders.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/Popsicle-Finance">ICE - Popsicle Finance</a></b> (🥉4 ·  ⭐ 40 · 💀) - A next-gen cross-chain yield enhancement platform.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/0xcap">CAP</a></b> (🥉4 ·  ⭐ 35 · 💀) - Protocol to trade the markets with stablecoins. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/sol-farm">TULIP - Tulip Protocol</a></b> (🥉4 ·  ⭐ 34 · 💀) - First yield aggregator on Solana, averaging around.. <code><img src="https://git.io/J9cOa" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/daoventures">DVD - DAOventures</a></b> (🥉4 ·  ⭐ 33 · 💀) - DeFi robo-advisor and automated money manager. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/frontierdotxyz">FRONT - Frontier</a></b> (🥉3 ·  ⭐ 63 · 💀) - Chain-agnostic DeFi aggregation layer. With our.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
</details>
<br>

## Exchange-based Tokens

<a href="#contents"><img align="right" width="15" height="15" src="https://git.io/JtehR" alt="Back to top"></a>

_Tokens associated with centralized or decentralized exchanges._

<details><summary><b><a href="https://github.com/trustwallet">TWT - Trust Wallet Token</a></b> (🥇36 ·  ⭐ 7.7K) - Trust Wallet Token (TWT) 511 is a utility token that.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/trustwallet/wallet-core">wallet-core</a></b> (🥇32 ·  ⭐ 2.1K) - Cross-platform, cross-blockchain wallet library. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/trustwallet/wallet-core) (👨‍💻 110 · 🔀 1.3K · 📥 120K · 📦 300 · 📋 1.1K - 1% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/trustwallet/wallet-core
	```
</details>
<details><summary><b><a href="https://github.com/trustwallet/assets">trustwallet/assets</a></b> (🥇27 ·  ⭐ 3.7K) - A comprehensive, up-to-date collection of information about.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/trustwallet/assets) (👨‍💻 3.9K · 🔀 17K · 📋 770 - 1% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/trustwallet/assets
	```
</details>
<details><summary><b><a href="https://github.com/trustwallet/blockatlas">blockatlas</a></b> (🥈20 ·  ⭐ 360 · 💤) - Clean and lightweight cross-chain transaction API. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/trustwallet/blockatlas) (👨‍💻 44 · 🔀 210 · 📥 33 · 📦 11 · 📋 500 - 1% open · ⏱️ 16.03.2022):

	```
	git clone https://github.com/trustwallet/blockatlas
	```
</details>
<details><summary><b><a href="https://github.com/trustwallet/trust-web3-provider">trust-web3-provider</a></b> (🥈19 ·  ⭐ 560) - Web3 javascript wrapper provider for iOS and Android.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/trustwallet/trust-web3-provider) (👨‍💻 16 · 🔀 370 · 📦 2 · 📋 190 - 13% open · ⏱️ 11.01.2023):

	```
	git clone https://github.com/trustwallet/trust-web3-provider
	```
</details>
<details><summary><b><a href="https://github.com/trustwallet/developer">developer</a></b> (🥈15 ·  ⭐ 230) - Trust Developer documentation: developer.trustwallet.com. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/trustwallet/developer) (👨‍💻 33 · 🔀 140 · 📋 59 - 8% open · ⏱️ 22.01.2023):

	```
	git clone https://github.com/trustwallet/developer
	```
</details>

<br>

 _7 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/bnb-chain">BNB</a></b> (🥇36 ·  ⭐ 7.5K) - Cryptocurrency of the Binance platform. It is a trading platform exclusively for..</summary>


---
<details><summary><b><a href="https://github.com/bnb-chain/bsc">bsc</a></b> (🥇27 ·  ⭐ 2K) - A BNB Smart Chain client based on the go-ethereum fork. <code><a href="http://bit.ly/37RvQcA">LGPL-3.0</a></code></summary>

- [GitHub](https://github.com/bnb-chain/bsc) (👨‍💻 760 · 🔀 900 · 📥 230K · 📦 5 · 📋 870 - 12% open · ⏱️ 03.02.2023):

	```
	git clone https://github.com/bnb-chain/bsc
	```
</details>
<details><summary><b><a href="https://github.com/bnb-chain/tss-lib">tss-lib</a></b> (🥇22 ·  ⭐ 480) - Threshold Signature Scheme, for ECDSA and EDDSA. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/bnb-chain/tss-lib) (👨‍💻 13 · 🔀 170 · 📥 2.9K · 📦 110 · 📋 120 - 48% open · ⏱️ 23.09.2022):

	```
	git clone https://github.com/bnb-chain/tss-lib
	```
</details>
<details><summary><b><a href="https://github.com/bnb-chain/bsc-genesis-contract">bsc-genesis-contract</a></b> (🥈16 ·  ⭐ 410) - The genesis contracts of BNB Smart Chain. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/bnb-chain/bsc-genesis-contract) (👨‍💻 9 · 🔀 530 · 📦 2 · 📋 69 - 53% open · ⏱️ 11.10.2022):

	```
	git clone https://github.com/bnb-chain/bsc-genesis-contract
	```
</details>

<br>

 _23 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/bitshares">BTS - BitShares</a></b> (🥇33 ·  ⭐ 2.1K) - Cryptocurrency . BitShares has a current supply of..</summary>


---
<details><summary><b><a href="https://github.com/bitshares/bitshares-ui">bitshares-ui</a></b> (🥇27 ·  ⭐ 520) - Fully featured Graphical User Interface / Reference Wallet for the.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/bitshares/bitshares-ui) (👨‍💻 170 · 🔀 560 · 📥 220K · 📋 2.3K - 19% open · ⏱️ 04.02.2023):

	```
	git clone https://github.com/bitshares/bitshares-ui
	```
</details>
<details><summary><b><a href="https://github.com/bitshares/bitshares-core">bitshares-core</a></b> (🥇25 ·  ⭐ 1.2K) - BitShares Blockchain node and command-line wallet. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/bitshares/bitshares-core) (👨‍💻 79 · 🔀 650 · 📥 140K · 📋 1.3K - 20% open · ⏱️ 01.12.2022):

	```
	git clone https://github.com/bitshares/bitshares-core
	```
</details>

<br>

 _7 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/republicprotocol">REN</a></b> (🥈27 ·  ⭐ 830) - Open protocol that enables the movement of value between blockchains. Rens core.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _10 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/project-serum">SRM - Serum</a></b> (🥈26 ·  ⭐ 2.9K) - Cryptocurrency launched in 2020. Serum has a current supply of.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/project-serum/serum-ts">serum-ts</a></b> (🥈18 ·  ⭐ 260) - Project Serum TypeScript monorepo. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/project-serum/serum-ts) (👨‍💻 34 · 🔀 240 · 📦 340 · 📋 140 - 81% open · ⏱️ 25.10.2022):

	```
	git clone https://github.com/project-serum/serum-ts
	```
</details>
<details><summary><b><a href="https://github.com/project-serum/serum-dex">serum-dex</a></b> (🥈17 ·  ⭐ 570) - Project Serum Rust Monorepo. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/project-serum/serum-dex) (👨‍💻 30 · 🔀 300 · 📥 25 · 📋 130 - 51% open · ⏱️ 25.10.2022):

	```
	git clone https://github.com/project-serum/serum-dex
	```
</details>
<details><summary><b><a href="https://github.com/project-serum/spl-token-wallet">spl-token-wallet</a></b> (🥈16 ·  ⭐ 870) -  <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/project-serum/spl-token-wallet) (👨‍💻 33 · 🔀 380 · 📥 160 · ⏱️ 02.11.2022):

	```
	git clone https://github.com/project-serum/spl-token-wallet
	```
</details>

<br>

 _7 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/crypto-org-chain">CRO - Cronos</a></b> (🥈25 ·  ⭐ 730) - We propose Crypto.com Chain, the next generation decentralized.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/crypto-org-chain/cronos">cronos</a></b> (🥇24 ·  ⭐ 220) - Cronos is the first Ethereum-compatible blockchain network built.. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/crypto-org-chain/cronos) (👨‍💻 22 · 🔀 190 · 📥 38K · 📦 13 · 📋 360 - 16% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/crypto-org-chain/cronos
	```
</details>
<details><summary><b><a href="https://github.com/crypto-org-chain/chain-main">chain-main</a></b> (🥈21 ·  ⭐ 430) - Crypto.org Chain: Croeseid Testnet and beyond development. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/crypto-org-chain/chain-main) (👨‍💻 20 · 🔀 340 · 📥 130K · 📋 380 - 5% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/crypto-org-chain/chain-main
	```
</details>

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/dydxprotocol">DYDX</a></b> (🥈23 ·  ⭐ 1.6K) - Governance token for the dYdX exchange protocol. Past users of the protocol.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/dydxprotocol/dydx-v3-python">dydx-v3-python</a></b> (🥈17 ·  ⭐ 210) - Python client for dYdX (API v3). <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/dydxprotocol/dydx-v3-python) (👨‍💻 12 · 🔀 130 · 📦 180 · 📋 80 - 58% open · ⏱️ 27.01.2023):

	```
	git clone https://github.com/dydxprotocol/dydx-v3-python
	```
</details>

<br>

 _7 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/loopring">LRC - Loopring</a></b> (🥈23 ·  ⭐ 560) - Decentralized Exchange (DEX) built on an Ethereum Layer-2 (L2).. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/Loopring/protocols">protocols</a></b> (🥈17 ·  ⭐ 240) - A zkRollup DEX & Payment Protocol. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/Loopring/protocols) (👨‍💻 39 · 🔀 55 · 📋 360 - 3% open · ⏱️ 19.10.2022):

	```
	git clone https://github.com/Loopring/protocols
	```
</details>

<br>

 _4 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/kucoin">KCS - KuCoin Token</a></b> (🥈21 ·  ⭐ 790) - International cryptocurrency exchange based out of Seychelle.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _7 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/paraswap">PSP - ParaSwap</a></b> (🥈21 ·  ⭐ 400) - Governance token for ParaSwap with the aim toprovide a.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/airswap">AST - AirSwap</a></b> (🥉18 ·  ⭐ 270) - Decentralized token exchange based on the Swap protocol.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary>Show 12 hidden projects...</summary>

- <b><a href="https://github.com/idexio">IDEX</a></b> (🥉14 ·  ⭐ 110) - Cryptocurrency and operates on the Ethereum platform. IDEX has a current.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/astroport-fi">ASTRO - AstroSwap</a></b> (🥉14 ·  ⭐ 110) - First interstellar DEX, built for the most loyal blockchain.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/diviproject">DIVX - Divi Exchange Token</a></b> (🥉14 ·  ⭐ 73 · 💀) - Divi Exchange Token (DIVX) is a cryptocurrency and.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/universablockchain">UTNP - Universa</a></b> (🥉11 ·  ⭐ 320) - Cryptocurrency launched in 2017and operates on the Ethereum.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/max-coin">MAX - Maxcoin</a></b> (🥉11 ·  ⭐ 77 · 💀) - One of the most important things for MaxCoin was to achieve a.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/dodoex">DODO</a></b> (🥉9 ·  ⭐ 280) - Decentralized Exchange (DEX) running on Ethereum and Binance Smart Chain... <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/WazirX">WRX - WazirX</a></b> (🥉6 ·  ⭐ 190 · 💀) - Cryptocurrency exchange with an advanced trading interface and.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/mcdexio">MCB - MCDex</a></b> (🥉6 ·  ⭐ 33 · 💀) - Decentralized leveraged trading protocol allowing zero price impact.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/hegic">HEGIC</a></b> (🥉5 ·  ⭐ 69 · 💀) - Cryptocurrency and operates on the Ethereum platform. Hegic has a current.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/ellipsis-finance">EPS - Ellipsis</a></b> (🥉5 ·  ⭐ 57 · 💀) - Secure low-slippage stable swapping on BSC. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/mxcdevelop">MX - MX Token</a></b> (🥉4 ·  ⭐ 44 · 💤) - Proof of rights and interests of MXC trading platform itself... <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/dexkit">KIT - DexKit</a></b> (🥉3 ·  ⭐ 42 · 🐣) - Next generation DEX. It uses technology based on ZRX protocol,.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
</details>
<br>

## Meme Tokens

<a href="#contents"><img align="right" width="15" height="15" src="https://git.io/JtehR" alt="Back to top"></a>

_Cryptocurrencies inspired by Internet memes._

<details><summary>Show 1 hidden projects...</summary>

- <b><a href="https://github.com/hodlcoin">HODL - HOdlcoin</a></b> (🥇15 ·  ⭐ 38 · 💀) - Passive Income Made Easy. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
</details>
<br>

## Web 3.0

<a href="#contents"><img align="right" width="15" height="15" src="https://git.io/JtehR" alt="Back to top"></a>

<details><summary><b><a href="https://github.com/filecoin-project">FIL - Filecoin</a></b> (🥇42 ·  ⭐ 9.8K) - The Filecoin network achieves staggering economies of scale by..</summary>


---
<details><summary><b><a href="https://github.com/filecoin-project/lotus">lotus</a></b> (🥇29 ·  ⭐ 2.5K) - Reference implementation of the Filecoin protocol, written in Go. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/filecoin-project/lotus) (👨‍💻 220 · 🔀 1.2K · 📥 11K · 📋 4.1K - 16% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/filecoin-project/lotus
	```
</details>
<details><summary><b><a href="https://github.com/filecoin-project/venus">venus</a></b> (🥇26 ·  ⭐ 2K) - Filecoin Full Node Implementation in Go. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/filecoin-project/venus) (👨‍💻 87 · 🔀 410 · 📥 17K · 📦 9 · 📋 2.9K - 2% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/filecoin-project/venus
	```
</details>
<details><summary><b><a href="https://github.com/filecoin-project/rust-fil-proofs">rust-fil-proofs</a></b> (🥈21 ·  ⭐ 440) - Proofs for Filecoin in Rust. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/filecoin-project/rust-fil-proofs) (👨‍💻 54 · 🔀 280 · 📥 2.7K · 📦 160 · 📋 620 - 9% open · ⏱️ 23.12.2022):

	```
	git clone https://github.com/filecoin-project/rust-fil-proofs
	```
</details>
<details><summary><b><a href="https://github.com/filecoin-project/ref-fvm">ref-fvm</a></b> (🥈20 ·  ⭐ 260) - Reference implementation of the Filecoin Virtual Machine. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/filecoin-project/ref-fvm) (👨‍💻 31 · 🔀 84 · 📦 92 · 📋 890 - 32% open · ⏱️ 06.02.2023):

	```
	git clone https://github.com/filecoin-project/ref-fvm
	```
</details>
<details><summary><b><a href="https://github.com/filecoin-project/specs">specs</a></b> (🥈19 ·  ⭐ 360 · 💤) - The Filecoin protocol specification. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/filecoin-project/specs) (👨‍💻 63 · 🔀 180 · 📦 3 · 📋 610 - 38% open · ⏱️ 25.05.2022):

	```
	git clone https://github.com/filecoin-project/specs
	```
</details>
<details><summary><b><a href="https://github.com/filecoin-project/slate">slate</a></b> (🥈17 ·  ⭐ 510) - WIP - Were building the place you go to discover, share, and sell files on.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/filecoin-project/slate) (👨‍💻 27 · 🔀 64 · 📋 540 - 5% open · ⏱️ 21.01.2023):

	```
	git clone https://github.com/filecoin-project/slate
	```
</details>
<details><summary><b><a href="https://github.com/filecoin-project/devgrants">devgrants</a></b> (🥈17 ·  ⭐ 330) - Apply for a Filecoin devgrant. Help build the Filecoin.. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/filecoin-project/devgrants) (👨‍💻 67 · 🔀 310 · 📋 1.1K - 22% open · ⏱️ 31.01.2023):

	```
	git clone https://github.com/filecoin-project/devgrants
	```
</details>
<details><summary><b><a href="https://github.com/filecoin-project/FIPs">FIPs</a></b> (🥈15 ·  ⭐ 240) - The Filecoin Improvement Proposal repository. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/filecoin-project/FIPs) (👨‍💻 47 · 🔀 110 · 📋 110 - 2% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/filecoin-project/FIPs
	```
</details>

<br>

 _31 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/status-im">SNT - Status</a></b> (🥇40 ·  ⭐ 8.8K) - Mobile operating system that will completely change the way the.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/status-im/status-mobile">status-mobile</a></b> (🥇29 ·  ⭐ 3.6K) - a free (libre) open source, mobile OS for Ethereum. <code><a href="http://bit.ly/3postzC">MPL-2.0</a></code></summary>

- [GitHub](https://github.com/status-im/status-mobile) (👨‍💻 280 · 🔀 940 · 📥 3K · 📋 7.4K - 5% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/status-im/status-mobile
	```
</details>
<details><summary><b><a href="https://github.com/status-im/status-go">status-go</a></b> (🥇26 ·  ⭐ 670) - The Status module that consumes go-ethereum. <code><a href="http://bit.ly/3postzC">MPL-2.0</a></code></summary>

- [GitHub](https://github.com/status-im/status-go) (👨‍💻 97 · 🔀 240 · 📥 1.6K · 📦 11 · 📋 880 - 13% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/status-im/status-go
	```
</details>
<details><summary><b><a href="https://github.com/status-im/nimbus-eth2">nimbus-eth2</a></b> (🥇23 ·  ⭐ 390) - Nim implementation of the Ethereum Beacon Chain. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/status-im/nimbus-eth2) (👨‍💻 66 · 🔀 150 · 📥 10K · 📋 970 - 19% open · ⏱️ 26.01.2023):

	```
	git clone https://github.com/status-im/nimbus-eth2
	```
</details>
<details><summary><b><a href="https://github.com/status-im/nimbus-eth1">nimbus-eth1</a></b> (🥈18 ·  ⭐ 510 · 📉) - Nimbus: an Ethereum Execution Client for Resource-Restricted.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/status-im/nimbus-eth1) (👨‍💻 32 · 🔀 84 · 📥 1 · 📋 310 - 35% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/status-im/nimbus-eth1
	```
</details>
<details><summary><b><a href="https://github.com/status-im/nim-chronos">nim-chronos</a></b> (🥈16 ·  ⭐ 260) - Chronos - An efficient library for asynchronous programming. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/status-im/nim-chronos) (👨‍💻 25 · 🔀 39 · 📋 130 - 55% open · ⏱️ 23.01.2023):

	```
	git clone https://github.com/status-im/nim-chronos
	```
</details>

<br>

 _27 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/smartcontractkit">LINK - Chainlink</a></b> (🥇36 ·  ⭐ 26K) - Framework for building Decentralized Oracle Networks (DONs).. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/smartcontractkit/chainlink">chainlink</a></b> (🥇35 ·  ⭐ 4.2K) - node of the decentralized oracle network, bridging on and off-chain.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/smartcontractkit/chainlink) (👨‍💻 160 · 🔀 1.2K · 📦 14K · 📋 440 - 24% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/smartcontractkit/chainlink
	```
</details>
<details><summary><b><a href="https://github.com/smartcontractkit/external-adapters-js">external-adapters-js</a></b> (🥇23 ·  ⭐ 220) - Monorepo containing JavaScript implementation of external.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/smartcontractkit/external-adapters-js) (👨‍💻 70 · 🔀 160 · 📥 3.7K · 📋 190 - 27% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/smartcontractkit/external-adapters-js
	```
</details>
<details><summary><b><a href="https://github.com/smartcontractkit/documentation">documentation</a></b> (🥈19 ·  ⭐ 220) - https://docs.chain.link The Chainlink developer documentation.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/smartcontractkit/documentation) (👨‍💻 110 · 🔀 250 · 📋 280 - 18% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/smartcontractkit/documentation
	```
</details>
<details><summary><b><a href="https://github.com/smartcontractkit/full-blockchain-solidity-course-py">full-blockchain-solidity-course-py</a></b> (🥈17 ·  ⭐ 9.6K) - Ultimate Solidity, Blockchain, and Smart Contract -.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/smartcontractkit/full-blockchain-solidity-course-py) (👨‍💻 13 · 🔀 2.6K · 📋 480 - 43% open · ⏱️ 30.11.2022):

	```
	git clone https://github.com/smartcontractkit/full-blockchain-solidity-course-py
	```
</details>
<details><summary><b><a href="https://github.com/smartcontractkit/hardhat-starter-kit">hardhat-starter-kit</a></b> (🥈17 ·  ⭐ 910) - A repo for boilerplate code for testing, deploying, and.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/smartcontractkit/hardhat-starter-kit) (👨‍💻 27 · 🔀 320 · 📋 71 - 18% open · ⏱️ 13.01.2023):

	```
	git clone https://github.com/smartcontractkit/hardhat-starter-kit
	```
</details>
<details><summary><b><a href="https://github.com/smartcontractkit/full-blockchain-solidity-course-js">full-blockchain-solidity-course-js</a></b> (🥈15 ·  ⭐ 7.6K) - Learn Blockchain, Solidity, and Full Stack Web3.. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/smartcontractkit/full-blockchain-solidity-course-js) (👨‍💻 23 · 🔀 1.8K · 📋 160 - 21% open · ⏱️ 29.11.2022):

	```
	git clone https://github.com/smartcontractkit/full-blockchain-solidity-course-js
	```
</details>
<details><summary><b><a href="https://github.com/smartcontractkit/chainlink-mix">chainlink-mix</a></b> (🥈15 ·  ⭐ 440) - Working with smart contracts with eth-brownie, python, and.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/smartcontractkit/chainlink-mix) (👨‍💻 11 · 🔀 170 · 📋 29 - 24% open · ⏱️ 15.09.2022):

	```
	git clone https://github.com/smartcontractkit/chainlink-mix
	```
</details>

<br>

 _24 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/graphprotocol">GRT - The Graph</a></b> (🥇35 ·  ⭐ 4K) - Indexing protocol and global API for organizing blockchain data.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/graphprotocol/graph-tooling">graph-tooling</a></b> (🥇27 ·  ⭐ 310) - Monorepo for various tools used by subgraph developers. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/graphprotocol/graph-tooling) (👨‍💻 70 · 🔀 140 · 📦 15K · 📋 340 - 34% open · ⏱️ 02.02.2023):

	```
	git clone https://github.com/graphprotocol/graph-tooling
	```
</details>
<details><summary><b><a href="https://github.com/graphprotocol/graph-node">graph-node</a></b> (🥇25 ·  ⭐ 2.4K) - Graph Node indexes data from blockchains such as Ethereum and.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/graphprotocol/graph-node) (👨‍💻 95 · 🔀 680 · 📥 660 · 📋 1.5K - 22% open · ⏱️ 06.02.2023):

	```
	git clone https://github.com/graphprotocol/graph-node
	```
</details>
<details><summary><b><a href="https://github.com/graphprotocol/contracts">graphprotocol/contracts</a></b> (🥇22 ·  ⭐ 270) - The Graph Protocol. <code><a href="http://bit.ly/2KucAZR">GPL-2.0</a></code></summary>

- [GitHub](https://github.com/graphprotocol/contracts) (👨‍💻 18 · 🔀 100 · 📦 78 · 📋 180 - 36% open · ⏱️ 31.01.2023):

	```
	git clone https://github.com/graphprotocol/contracts
	```
</details>
<details><summary><b><a href="https://github.com/graphprotocol/graph-ts">graph-ts</a></b> (🥈21 ·  ⭐ 210) - TypeScript/AssemblyScript library for writing mappings for The Graph. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/graphprotocol/graph-ts) (👨‍💻 20 · 🔀 74 · 📦 15K · 📋 120 - 30% open · ⏱️ 23.12.2022):

	```
	git clone https://github.com/graphprotocol/graph-ts
	```
</details>

<br>

 _10 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/stacks-network">STX - Stacks</a></b> (🥇34 ·  ⭐ 7.4K) - Stacks brings Apps and Smart Contracts to Bitcoin. Apps built on Stacks..</summary>


---
<details><summary><b><a href="https://github.com/stacks-network/stacks-blockchain">stacks-blockchain</a></b> (🥇27 ·  ⭐ 2.7K) - The Stacks blockchain implementation. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/stacks-network/stacks-blockchain) (👨‍💻 94 · 🔀 550 · 📥 17K · 📦 4 · 📋 2.1K - 9% open · ⏱️ 01.12.2022):

	```
	git clone https://github.com/stacks-network/stacks-blockchain
	```
</details>
<details><summary><b><a href="https://github.com/stacks-network/gaia">stacks-network/gaia</a></b> (🥈19 ·  ⭐ 750) - A decentralized high-performance storage system. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/stacks-network/gaia) (👨‍💻 23 · 🔀 150 · 📋 180 - 18% open · ⏱️ 17.10.2022):

	```
	git clone https://github.com/stacks-network/gaia
	```
</details>
<details><summary><b><a href="https://github.com/stacks-network/stacks">stacks</a></b> (🥈18 ·  ⭐ 2K) - Overview of Bitcoins Stacks layer. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/stacks-network/stacks) (👨‍💻 38 · 🔀 230 · 📋 310 - 8% open · ⏱️ 13.01.2023):

	```
	git clone https://github.com/stacks-network/stacks
	```
</details>

<br>

 _15 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/holochain">HOT - Holo</a></b> (🥇34 ·  ⭐ 1.2K) - Holochain enables a distributed web with user autonomy built directly.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/holochain/holochain">holochain</a></b> (🥇23 ·  ⭐ 750) - The current, performant & industrial strength version of.. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/holochain/holochain) (👨‍💻 43 · 🔀 110 · 📦 220 · 📋 180 - 52% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/holochain/holochain
	```
</details>

<br>

 _9 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/lbryio">LBC - LBRY Credits</a></b> (🥇33 ·  ⭐ 17K) - LBRY (pronounced Library) is a decentralized digital content..</summary>


---
<details><summary><b><a href="https://github.com/lbryio/lbry-desktop">lbry-desktop</a></b> (🥇32 ·  ⭐ 3.6K) - A browser and wallet for LBRY, the decentralized, user-controlled.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/lbryio/lbry-desktop) (👨‍💻 210 · 🔀 450 · 📥 23M · 📋 3.8K - 15% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/lbryio/lbry-desktop
	```
</details>
<details><summary><b><a href="https://github.com/lbryio/lbry-sdk">lbry-sdk</a></b> (🥇29 ·  ⭐ 7.2K) - The LBRY SDK for building decentralized, censorship resistant,.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/lbryio/lbry-sdk) (👨‍💻 100 · 🔀 470 · 📥 88K · 📋 1.8K - 20% open · ⏱️ 02.02.2023):

	```
	git clone https://github.com/lbryio/lbry-sdk
	```
</details>
<details><summary><b><a href="https://github.com/lbryio/lbry-android">lbry-android</a></b> (🥈20 ·  ⭐ 2.5K) - The LBRY Android app. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/lbryio/lbry-android) (👨‍💻 33 · 🔀 110 · 📥 45K · 📋 820 - 19% open · ⏱️ 29.11.2022):

	```
	git clone https://github.com/lbryio/lbry-android
	```
</details>
<details><summary><b><a href="https://github.com/lbryio/lbry.com">lbry.com</a></b> (🥈19 ·  ⭐ 250) - lbry.com, the website for the LBRY protocol. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/lbryio/lbry.com) (👨‍💻 190 · 🔀 240 · 📋 310 - 4% open · ⏱️ 01.12.2022):

	```
	git clone https://github.com/lbryio/lbry.com
	```
</details>

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/maidsafe">MAID - MaidSafeCoin</a></b> (🥇33 ·  ⭐ 1.3K) - Cryptocurrency launched in 2014. MaidSafeCoin has a current..</summary>


---
<details><summary><b><a href="https://github.com/maidsafe/qp2p">qp2p</a></b> (🥇23 ·  ⭐ 340) - peer-to-peer communications library for Rust based on QUIC protocol. <code><a href="http://bit.ly/3aKzpTv">BSD-3</a></code></summary>

- [GitHub](https://github.com/maidsafe/qp2p) (👨‍💻 35 · 🔀 64 · 📦 140 · 📋 45 - 13% open · ⏱️ 25.01.2023):

	```
	git clone https://github.com/maidsafe/qp2p
	```
</details>

<br>

 _7 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/ethersphere">BZZ - Swarm</a></b> (🥈32 ·  ⭐ 1.9K) - System of peer-to-peer networked nodes that create a decentralised.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/ethersphere/bee">ethersphere/bee</a></b> (🥇32 ·  ⭐ 1.4K) - Bee is a Swarm client implemented in Go. Its the basic.. <code><a href="http://bit.ly/3aKzpTv">BSD-3</a></code></summary>

- [GitHub](https://github.com/ethersphere/bee) (👨‍💻 51 · 🔀 320 · 📥 270K · 📦 100 · 📋 1.4K - 9% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/ethersphere/bee
	```
</details>

<br>

 _9 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/aragon">ANT - Aragon</a></b> (🥈31 ·  ⭐ 4.2K) - Decentralized app (dApp) on the Ethereum blockchain that allows.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/aragon/client">client</a></b> (🥇23 ·  ⭐ 820) - (Aragon 1) Create and manage decentralized organizations on Ethereum. <code><a href="http://bit.ly/3pwmjO5">AGPL-3.0</a></code></summary>

- [GitHub](https://github.com/aragon/client) (👨‍💻 69 · 🔀 250 · 📥 34K · 📦 12 · 📋 720 - 29% open · ⏱️ 21.10.2022):

	```
	git clone https://github.com/aragon/client
	```
</details>
<details><summary><b><a href="https://github.com/aragon/use-wallet">use-wallet</a></b> (🥇22 ·  ⭐ 700 · 💤) - useWallet() All-in-one solution to connect a dapp to an Ethereum.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/aragon/use-wallet) (👨‍💻 29 · 🔀 220 · 📦 3.1K · 📋 92 - 52% open · ⏱️ 19.04.2022):

	```
	git clone https://github.com/aragon/use-wallet
	```
</details>
<details><summary><b><a href="https://github.com/aragon/aragon-apps">aragon-apps</a></b> (🥈21 ·  ⭐ 380) - (Aragon 1) Aragon apps developed by Aragon Core Devs (smart.. <code><a href="http://bit.ly/3pwmjO5">AGPL-3.0</a></code></summary>

- [GitHub](https://github.com/aragon/aragon-apps) (👨‍💻 51 · 🔀 220 · 📥 140 · 📦 5 · 📋 390 - 20% open · ⏱️ 12.10.2022):

	```
	git clone https://github.com/aragon/aragon-apps
	```
</details>

<br>

 _17 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/aeternity">AE - Aeternity</a></b> (🥈31 ·  ⭐ 1.7K) - Public, open-source blockchain protocol that enables a platform.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/aeternity/aeternity">aeternity</a></b> (🥇24 ·  ⭐ 1K) - ternity blockchain - scalable blockchain for the people - smart.. <code><a href="http://bit.ly/3hkKRql">ISC</a></code></summary>

- [GitHub](https://github.com/aeternity/aeternity) (👨‍💻 78 · 🔀 230 · 📥 21K · 📋 850 - 29% open · ⏱️ 01.02.2023):

	```
	git clone https://github.com/aeternity/aeternity
	```
</details>

<br>

 _8 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/ArweaveTeam">AR - Arweave</a></b> (🥈30 ·  ⭐ 1.9K) - Welcome to the future of data storage A new data storage blockchain..</summary>


---
<details><summary><b><a href="https://github.com/ArweaveTeam/arweave-js">arweave-js</a></b> (🥇25 ·  ⭐ 510) - Browser and Nodejs client for general interaction with the arweave.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/ArweaveTeam/arweave-js) (👨‍💻 39 · 🔀 110 · 📦 6.6K · 📋 87 - 12% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/ArweaveTeam/arweave-js
	```
</details>
<details><summary><b><a href="https://github.com/ArweaveTeam/arweave">arweave</a></b> (🥈19 ·  ⭐ 750) - The Arweave server and App Developer Toolkit. <code><a href="http://bit.ly/2KucAZR">GPL-2.0</a></code></summary>

- [GitHub](https://github.com/ArweaveTeam/arweave) (👨‍💻 43 · 🔀 150 · 📥 32K · 📋 190 - 34% open · ⏱️ 06.10.2022):

	```
	git clone https://github.com/ArweaveTeam/arweave
	```
</details>
<details><summary><b><a href="https://github.com/ArweaveTeam/SmartWeave">SmartWeave</a></b> (🥈15 ·  ⭐ 210 · 💤) - Simple, scalable smart contracts on the Arweave protocol. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/ArweaveTeam/SmartWeave) (👨‍💻 23 · 🔀 62 · 📦 760 · 📋 29 - 82% open · ⏱️ 23.06.2022):

	```
	git clone https://github.com/ArweaveTeam/SmartWeave
	```
</details>

<br>

 _7 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/oceanprotocol">OCEAN - Ocean Protocol</a></b> (🥈30 ·  ⭐ 680) - Ecosystem for sharing data and associated services. It.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _7 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/levelkdev">DXD - DXdao</a></b> (🥈29 ·  ⭐ 3.6K) - Collective that builds and governs decentralized products and.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/protofire/solhint">solhint</a></b> (🥇27 ·  ⭐ 850) - Solhint is an open source project created by https://protofire.io. Its.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/protofire/solhint) (👨‍💻 46 · 🔀 130 · 📦 32K · 📋 240 - 41% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/protofire/solhint
	```
</details>

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/handshake-org">HNS - Handshake</a></b> (🥈27 ·  ⭐ 3.9K) - Decentralized, permissionless naming protocol where every peer is..</summary>


---
<details><summary><b><a href="https://github.com/handshake-org/hsd">hsd</a></b> (🥇24 ·  ⭐ 1.7K) - Handshake Daemon & Full Node. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/handshake-org/hsd) (👨‍💻 63 · 🔀 270 · 📥 44 · 📦 150 · 📋 350 - 38% open · ⏱️ 19.01.2023):

	```
	git clone https://github.com/handshake-org/hsd
	```
</details>
<details><summary><b><a href="https://github.com/handshake-org/hnsd">hnsd</a></b> (🥈16 ·  ⭐ 260) - Handshake SPV name resolver. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/handshake-org/hnsd) (👨‍💻 15 · 🔀 46 · 📋 46 - 36% open · ⏱️ 04.01.2023):

	```
	git clone https://github.com/handshake-org/hnsd
	```
</details>

<br>

 _9 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/skycoin">SKY - Skycoin</a></b> (🥈27 ·  ⭐ 970) - Most advanced blockchain platform in the world. Developed by early..</summary>


---

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/raiden-network">RDN - Raiden Network Token</a></b> (🥈26 ·  ⭐ 1.9K) - Off-chain scaling solution, enabling near-instant,.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/raiden-network/raiden">raiden</a></b> (🥇24 ·  ⭐ 1.9K) - Raiden Network. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/raiden-network/raiden) (👨‍💻 99 · 🔀 380 · 📥 7.1K · 📦 26 · 📋 2.7K - 13% open · ⏱️ 20.10.2022):

	```
	git clone https://github.com/raiden-network/raiden
	```
</details>

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/radicle-dev">RAD - Radicle</a></b> (🥈26 ·  ⭐ 1.4K) - Cryptocurrency . Radicle has a current supply of 99,998,580 with.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/radicle-dev/radicle-upstream">radicle-upstream</a></b> (🥈16 ·  ⭐ 620) - Desktop client for Radicle. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/radicle-dev/radicle-upstream) (👨‍💻 37 · 🔀 53 · 📥 460 · 📋 1.4K - 12% open · ⏱️ 01.08.2022):

	```
	git clone https://github.com/radicle-dev/radicle-upstream
	```
</details>
<details><summary><b><a href="https://github.com/radicle-dev/radicle-link">radicle-link</a></b> (🥈16 ·  ⭐ 420) - The second iteration of the Radicle code collaboration.. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/radicle-dev/radicle-link) (👨‍💻 24 · 🔀 42 · 📦 110 · 📋 280 - 40% open · ⏱️ 06.09.2022):

	```
	git clone https://github.com/radicle-dev/radicle-link
	```
</details>

<br>

 _7 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/bittorrent">BTT - BitTorrent</a></b> (🥈25 ·  ⭐ 1.5K) - BitTorrent was initially conceived by Bram Cohen, a peer-to-peer..</summary>


---

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/aelfProject">ELF - aelf</a></b> (🥈25 ·  ⭐ 790) - Versatile business blockchain platform powered by cloud computing and.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/AElfProject/AElf">AElf</a></b> (🥇29 ·  ⭐ 790) - A scalable cloud computing blockchain platform. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/AElfProject/AElf) (👨‍💻 77 · 🔀 210 · 📥 2.6K · 📦 35 · 📋 770 - 5% open · ⏱️ 02.02.2023):

	```
	git clone https://github.com/AElfProject/AElf
	```
</details>

---
</details>
<details><summary><b><a href="https://github.com/storj">STORJ</a></b> (🥈24 ·  ⭐ 1.7K) - Open-source platform that leverages the blockchain to provide end-to-end.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/storj/drpc">drpc</a></b> (🥈17 ·  ⭐ 1.2K) - drpc is a lightweight, drop-in replacement for gRPC. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/storj/drpc) (👨‍💻 14 · 🔀 40 · 📋 35 - 37% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/storj/drpc
	```
</details>

<br>

 _6 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/rocket-pool">RPL - Rocket Pool</a></b> (🥈23 ·  ⭐ 1K) - Rocket Pool is Ethereums most decentralised liquid staking.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/thorchain">RUNE - THORChain</a></b> (🥈23 ·  ⭐ 220) - THORChain is building a chain-agnostic bridging protocol that.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/PlatONnetwork">LAT - PlatON Network</a></b> (🥈22 ·  ⭐ 620) - Cryptocurrency launched in 2021. PlatON has a current supply..</summary>


---
<details><summary><b><a href="https://github.com/PlatONnetwork/PlatON-Go">PlatON-Go</a></b> (🥇24 ·  ⭐ 430) - Golang implementation of the PlatON protocol. <code><a href="http://bit.ly/37RvQcA">LGPL-3.0</a></code></summary>

- [GitHub](https://github.com/PlatONnetwork/PlatON-Go) (👨‍💻 450 · 🔀 140 · 📥 510 · 📦 8 · 📋 660 - 1% open · ⏱️ 06.01.2023):

	```
	git clone https://github.com/PlatONnetwork/PlatON-Go
	```
</details>

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/identity-com">CVC - Civic</a></b> (🥈22 ·  ⭐ 210) - Leading provider of identity management tools for Web3, empowering.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/Fantom-Foundation">FTM - Fantom</a></b> (🥉21 ·  ⭐ 760) - New DAG based Smart Contract platform that intends to solve the.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _8 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/axelarnetwork">AXL - Axelar</a></b> (🥉21 ·  ⭐ 240) - Axelar delivers secure cross-chain communication. That means dApp.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _4 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/daostack">GEN - DAOstack</a></b> (🥉20 ·  ⭐ 460 · 💤) - Operational stack for DAOs, a comprehensive toolkit for.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/provable-things">PNT - pNetwork</a></b> (🥉19 ·  ⭐ 1.5K) - Building an Ecosysten on Solana Network. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/provable-things/ethereum-api">ethereum-api</a></b> (🥈18 ·  ⭐ 760) - Provable API for Ethereum smart contracts. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/provable-things/ethereum-api) (👨‍💻 12 · 🔀 410 · 📋 65 - 23% open · ⏱️ 05.12.2022):

	```
	git clone https://github.com/provable-things/ethereum-api
	```
</details>

<br>

 _4 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/NebulousLabs">SC - Siacoin</a></b> (🥉19 ·  ⭐ 550 · 📉) - Decentralized cloud storage platform similar in concept to Dropbox..</summary>


---
<details><summary><b><a href="https://github.com/SkynetLabs/skynet-webportal">skynet-webportal</a></b> (🥈16 ·  ⭐ 260) - A webapp that makes Skynet accessible to web browsers. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/SkynetLabs/skynet-webportal) (👨‍💻 35 · 🔀 55 · 📋 140 - 24% open · ⏱️ 25.10.2022):

	```
	git clone https://github.com/SkynetLabs/skynet-webportal
	```
</details>

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/openhive-network">HIVE</a></b> (🥉18 ·  ⭐ 360) - Cryptocurrency launched in 2020. Hive has a current supply of 467,382,213.663. The..</summary>


---
<details><summary><b><a href="https://github.com/openhive-network/hive">openhive-network/hive</a></b> (🥈19 ·  ⭐ 310) - Fast. Scalable. Powerful. The Blockchain for Web3. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/openhive-network/hive) (👨‍💻 110 · 🔀 88 · ⏱️ 15.12.2022):

	```
	git clone https://github.com/openhive-network/hive
	```
</details>

<br>

 _1 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/gxchain">GXC - GXChain</a></b> (🥉17 ·  ⭐ 470) - From the developers at Gongxinbao (GXB) comes GXChain, a public..</summary>


---

<br>

 _4 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/bonfida">FIDA - Bonfida</a></b> (🥉17 ·  ⭐ 450) - Full product suite that bridges the gap between Serum, Solana,.. <code><img src="https://git.io/J9cOa" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _6 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/lidofinance">LDO - Lido DAO</a></b> (🥉15 ·  ⭐ 330 · 💤) - Cryptocurrency and operates on the Ethereum platform. Lido.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/wanchain">WAN - Wanchain</a></b> (🥉15 ·  ⭐ 280 · 💤) - Wanchain seeks to link the present to the future, through the..</summary>


---
<details><summary><b><a href="https://github.com/wanchain/go-wanchain">go-wanchain</a></b> (🥈18 ·  ⭐ 280 · 💤) - Wanchain Client Source Code. <code><a href="http://bit.ly/37RvQcA">LGPL-3.0</a></code></summary>

- [GitHub](https://github.com/wanchain/go-wanchain) (👨‍💻 37 · 🔀 86 · 📥 77K · 📋 9 - 44% open · ⏱️ 20.06.2022):

	```
	git clone https://github.com/wanchain/go-wanchain
	```
</details>

---
</details>
<details><summary>Show 27 hidden projects...</summary>

- <b><a href="https://github.com/loomnetwork">LOOMOLD - Loom Network (OLD)</a></b> (🥈22 ·  ⭐ 900 · 💀) - The Next-Generation Blockchain Application Platform.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/CortexFoundation">CTXC - Cortex</a></b> (🥈22 ·  ⭐ 140) - Cortex is built on a new public chain called Cortex. The chain includes..
- <b><a href="https://github.com/threefoldtech">TFT - ThreeFold Token</a></b> (🥉19 ·  ⭐ 94) - What is ThreeFold (TFT)? Founded in 2016, ThreeFold is a.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/0chain">ZCN - Zus</a></b> (🥉19 ·  ⭐ 88) - High performance storage network that powers limitless applications. Its.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/docknetwork">DOCK</a></b> (🥉18 ·  ⭐ 130) - Decentralized data exchange protocol that lets people connect their profiles,..
- <b><a href="https://github.com/sonm-io">SNM - SONM</a></b> (🥉16 ·  ⭐ 360 · 💀) - Global operating system that is also a decentralized.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/stratosnet">STOS - Stratos</a></b> (🥉16 ·  ⭐ 97) - Next generation of decentralized Data Mesh that provides scalable,.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/Kylin-Network">KYL - Kylin Network</a></b> (🥉16 ·  ⭐ 43) - Kylin Network offers any applications and blockchains (such.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/aleph-im">ALEPH - Aleph.im</a></b> (🥉15 ·  ⭐ 96) - Decentralized cloud storage, database and computing platform,.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/f-o-a-m">FOAM</a></b> (🥉14 ·  ⭐ 420 · 💤) - Open protocol for proof of location on Ethereum. Our mission is to build a.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/keep3r-network">KP3R - Keep3rV1</a></b> (🥉14 ·  ⭐ 410) - Cryptocurrency and operates on the Ethereum platform. Keep3rV1.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/marlinprotocol">POND - Marlin</a></b> (🥉14 ·  ⭐ 97) - High-performance network infrastructure for modern decentralized.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/Ether1Project">ETHO - Etho Protocol</a></b> (🥉14 ·  ⭐ 41) - Blockchain backed immutable storage via IPFS nodes and.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/kardiachain">KAI - KardiaChain</a></b> (🥉14 ·  ⭐ 32) - We believe Blockchain can create equal opportunities for.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/mathwallet">MATH</a></b> (🥉13 ·  ⭐ 190 · 💤) - One stop crypto solution platform which contains MathWallet, MATH VPOS.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/virtualeconomy">VSYS - V Systems</a></b> (🥉13 ·  ⭐ 110 · 💀) - Blockchain infrastructure provider with a focus on database and..
- <b><a href="https://github.com/orbs-network">ORBS</a></b> (🥉12 ·  ⭐ 41) - Orbs was built to bridge the unoccupied gap between the functionality of a.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/crustio">CRU - Crust Network</a></b> (🥉11 ·  ⭐ 440) - What is Crust Network? Crust Network provides a.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/cartesi">CTSI - Cartesi</a></b> (🥉9 ·  ⭐ 120 · 💀) - What is Cartesi? Ease of Adoption: Developers can work in a.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/Sharders">SS - Sharder</a></b> (🥉9 ·  ⭐ 42 · 💤) - Cryptocurrency launched in 2018and operates on the Ethereum.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/measurabledatatoken">MDT - Measurable Data Token</a></b> (🥉8 ·  ⭐ 100 · 💀) - Measurable Data Token (MDT) is a decentralized data.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/TiesNetwork">TIE - Ties.DB</a></b> (🥉8 ·  ⭐ 59 · 💤) - Cryptocurrency and operates on the Ethereum platform. Ties.DB.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/liquidapps-io">DAPP - LiquidApps</a></b> (🥉8 ·  ⭐ 51) - LiquidApps mission is to promote the adoption of decentralized.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/datahighway-dhx">DHX - DataHighway</a></b> (🥉8 ·  ⭐ 48 · 💀) - DataHighways community members will ultimately be.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/ethereum-push-notification-service">PUSH - Push Protocol</a></b> (🥉7 ·  ⭐ 39) - Cryptocurrency and operates on the Ethereum platform. Push.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/bluzelle">BLZ - Bluzelle</a></b> (🥉6 ·  ⭐ 65 · 💀) - Decentralized storage network for the creator economy. Bluzelle.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/covalenthq">CQT - Covalent</a></b> (🥉5 ·  ⭐ 83) - Covalentleverages big-data technologies to create meaning from.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
</details>
<br>

## Privacy Coins

<a href="#contents"><img align="right" width="15" height="15" src="https://git.io/JtehR" alt="Back to top"></a>

_Cryptocurrencies which allow users to conduct transactions privately and anonymously._

<details><summary><b><a href="https://github.com/decred">DCR - Decred</a></b> (🥇34 ·  ⭐ 2.6K) - Decred aims to build a community-directed digital currency whose..</summary>


---
<details><summary><b><a href="https://github.com/decred/dcrd">dcrd</a></b> (🥈21 ·  ⭐ 680) - Decred daemon in Go (golang). <code><a href="http://bit.ly/3hkKRql">ISC</a></code></summary>

- [GitHub](https://github.com/decred/dcrd) (👨‍💻 130 · 🔀 280 · 📋 500 - 5% open · ⏱️ 21.01.2023):

	```
	git clone https://github.com/decred/dcrd
	```
</details>
<details><summary><b><a href="https://github.com/decred/decrediton">decrediton</a></b> (🥈21 ·  ⭐ 200) - Cross-platform GUI for Decred. <code><a href="http://bit.ly/3hkKRql">ISC</a></code></summary>

- [GitHub](https://github.com/decred/decrediton) (👨‍💻 61 · 🔀 130 · 📦 2 · 📋 1.7K - 9% open · ⏱️ 08.12.2022):

	```
	git clone https://github.com/decred/decrediton
	```
</details>

<br>

 _15 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/scrtlabs">SCRT - Secret</a></b> (🥇32 ·  ⭐ 3.3K) - PRIVACY FOR BLOCKCHAINS.</summary>


---
<details><summary><b><a href="https://github.com/scrtlabs/SecretNetwork">SecretNetwork</a></b> (🥇25 ·  ⭐ 460) - The Secret Network. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/scrtlabs/SecretNetwork) (👨‍💻 57 · 🔀 150 · 📥 57K · 📋 570 - 6% open · ⏱️ 05.02.2023):

	```
	git clone https://github.com/scrtlabs/SecretNetwork
	```
</details>

<br>

 _9 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/zcash">ZEC - Zcash</a></b> (🥇31 ·  ⭐ 5.9K) - Decentralized and open-source cryptocurrency that offers privacy and.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/zcash/zcash">zcash</a></b> (🥇28 ·  ⭐ 4.7K) - Zcash - Internet Money. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/zcash/zcash) (👨‍💻 610 · 🔀 2K · 📥 250 · 📋 3.8K - 30% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/zcash/zcash
	```
</details>
<details><summary><b><a href="https://github.com/zcash/librustzcash">librustzcash</a></b> (🥇24 ·  ⭐ 280) - Rust-language assets for Zcash. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/zcash/librustzcash) (👨‍💻 43 · 🔀 190 · 📦 790 · 📋 320 - 47% open · ⏱️ 02.02.2023):

	```
	git clone https://github.com/zcash/librustzcash
	```
</details>
<details><summary><b><a href="https://github.com/zcash/halo2">halo2</a></b> (🥈20 ·  ⭐ 350) -  <code>Unlicensed</code></summary>

- [GitHub](https://github.com/zcash/halo2) (👨‍💻 29 · 🔀 150 · 📦 220 · 📋 300 - 58% open · ⏱️ 20.01.2023):

	```
	git clone https://github.com/zcash/halo2
	```
</details>
<details><summary><b><a href="https://github.com/zcash/zips">zips</a></b> (🥈18 ·  ⭐ 240) - Zcash Improvement Proposals. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/zcash/zips) (👨‍💻 45 · 🔀 110 · 📋 400 - 34% open · ⏱️ 11.01.2023):

	```
	git clone https://github.com/zcash/zips
	```
</details>

<br>

 _6 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/oxen-io">OXEN</a></b> (🥇30 ·  ⭐ 4.2K) - Oxen combines a cutting-edge privacy and decentralisation software stack with.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/oxen-io/session-desktop">session-desktop</a></b> (🥇27 ·  ⭐ 1.1K) - Session Desktop - Onion routing based messenger. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/oxen-io/session-desktop) (👨‍💻 160 · 🔀 130 · 📥 3.7M · 📋 990 - 24% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/oxen-io/session-desktop
	```
</details>
<details><summary><b><a href="https://github.com/oxen-io/lokinet">lokinet</a></b> (🥇26 ·  ⭐ 1.3K) - Lokinet is an anonymous, decentralized and IP based overlay network.. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/oxen-io/lokinet) (👨‍💻 36 · 🔀 160 · 📥 78K · 📋 560 - 23% open · ⏱️ 06.02.2023):

	```
	git clone https://github.com/oxen-io/lokinet
	```
</details>
<details><summary><b><a href="https://github.com/oxen-io/session-android">session-android</a></b> (🥇26 ·  ⭐ 1.2K) - A private messenger for Android. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/oxen-io/session-android) (👨‍💻 240 · 🔀 100 · 📥 140K · 📋 630 - 42% open · ⏱️ 06.02.2023):

	```
	git clone https://github.com/oxen-io/session-android
	```
</details>
<details><summary><b><a href="https://github.com/oxen-io/oxen-core">oxen-core</a></b> (🥇24 ·  ⭐ 280) - Oxen core repository, containing oxend and oxen cli wallets. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/oxen-io/oxen-core) (👨‍💻 350 · 🔀 100 · 📥 34K · 📋 340 - 33% open · ⏱️ 25.01.2023):

	```
	git clone https://github.com/oxen-io/oxen-core
	```
</details>
<details><summary><b><a href="https://github.com/oxen-io/session-ios">session-ios</a></b> (🥇22 ·  ⭐ 290) - A private messenger for iOS. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/oxen-io/session-ios) (👨‍💻 110 · 🔀 49 · 📥 1.1K · 📋 240 - 25% open · ⏱️ 06.02.2023):

	```
	git clone https://github.com/oxen-io/session-ios
	```
</details>

<br>

 _1 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/monero-project">XMR - Monero</a></b> (🥇29 ·  ⭐ 10K) - Cryptocurrency . Users are able to generate XMR through the process of..</summary>


---
<details><summary><b><a href="https://github.com/monero-project/monero">monero</a></b> (🥇28 ·  ⭐ 7.6K) - Monero: the secure, private, untraceable cryptocurrency. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/monero-project/monero) (👨‍💻 380 · 🔀 3.4K · 📥 31K · 📋 3K - 17% open · ⏱️ 06.02.2023):

	```
	git clone https://github.com/monero-project/monero
	```
</details>
<details><summary><b><a href="https://github.com/monero-project/monero-gui">monero-gui</a></b> (🥇24 ·  ⭐ 1.4K) - Monero: the secure, private, untraceable cryptocurrency. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/monero-project/monero-gui) (👨‍💻 210 · 🔀 740 · 📥 35K · 📋 1.8K - 21% open · ⏱️ 11.01.2023):

	```
	git clone https://github.com/monero-project/monero-gui
	```
</details>
<details><summary><b><a href="https://github.com/monero-project/monero-site">monero-site</a></b> (🥈20 ·  ⭐ 210) -  <code><a href="http://bit.ly/3aKzpTv">BSD-3</a></code></summary>

- [GitHub](https://github.com/monero-project/monero-site) (👨‍💻 220 · 🔀 360 · 📋 720 - 20% open · ⏱️ 02.02.2023):

	```
	git clone https://github.com/monero-project/monero-site
	```
</details>

<br>

 _4 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/HorizenOfficial">ZEN - Horizen</a></b> (🥇29 ·  ⭐ 940) - Zero-knowledge-enabled network of blockchains powered by the largest..</summary>


---
<details><summary><b><a href="https://github.com/HorizenOfficial/zen">zen</a></b> (🥈21 ·  ⭐ 230) - Horizen. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/HorizenOfficial/zen) (👨‍💻 500 · 🔀 100 · 📥 38K · 📋 160 - 19% open · ⏱️ 27.01.2023):

	```
	git clone https://github.com/HorizenOfficial/zen
	```
</details>

<br>

 _10 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/particl">PART - Particl</a></b> (🥇29 ·  ⭐ 330) - Open-source and decentralized privacy platform built on the blockchain..</summary>


---

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/mimblewimble">GRIN</a></b> (🥈28 ·  ⭐ 1.2K) - Private & lightweight open source project based on the mimblewimble blockchain..</summary>


---

<br>

 _10 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/dashevo">DASH</a></b> (🥈28 ·  ⭐ 530) - Your money, your way.</summary>


---

<br>

 _9 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/vergecurrency">XVG - Verge</a></b> (🥈27 ·  ⭐ 1.6K) - Verge coin started its cryptocurrency journey in 2014, going by the name..</summary>


---
<details><summary><b><a href="https://github.com/vergecurrency/verge">verge</a></b> (🥇29 ·  ⭐ 1.4K) - Official Verge Core Source Code Repository. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/vergecurrency/verge) (👨‍💻 73 · 🔀 400 · 📥 610K · 📋 730 - 0% open · ⏱️ 01.12.2022):

	```
	git clone https://github.com/vergecurrency/verge
	```
</details>

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/DimensionDev">MASK - Mask Network</a></b> (🥈27 ·  ⭐ 1.5K) - Core product of Dimension, which is positioned to become.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/DimensionDev/Maskbook">Maskbook</a></b> (🥇28 ·  ⭐ 1.3K) - The portal to the new, open Internet. ([I:b]). <code><a href="http://bit.ly/3pwmjO5">AGPL-3.0</a></code></summary>

- [GitHub](https://github.com/DimensionDev/Maskbook) (👨‍💻 65 · 🔀 290 · 📥 6.6K · 📦 2 · 📋 2.2K - 3% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/DimensionDev/Maskbook
	```
</details>

<br>

 _4 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/mobilecoinfoundation">MOB - MobileCoin</a></b> (🥈27 ·  ⭐ 1.3K) - Cryptocurrency . MobileCoin has a current supply of 250,000,000..</summary>


---
<details><summary><b><a href="https://github.com/mobilecoinfoundation/mobilecoin">mobilecoin</a></b> (🥇24 ·  ⭐ 1.1K) - Private payments for mobile devices. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/mobilecoinfoundation/mobilecoin) (👨‍💻 52 · 🔀 120 · 📥 3K · 📦 13 · 📋 460 - 51% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/mobilecoinfoundation/mobilecoin
	```
</details>

<br>

 _4 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/ergoplatform">ERG - Ergo</a></b> (🥈26 ·  ⭐ 860) - Flexible blockchain protocol. Ergo is designed for developing decentralized..</summary>


---
<details><summary><b><a href="https://github.com/ergoplatform/ergo">ergo</a></b> (🥇24 ·  ⭐ 470) - Ergo protocol description & reference client implementation. <code><a href="https://tldrlegal.com/search?q=CC0-1.0">CC0-1.0</a></code></summary>

- [GitHub](https://github.com/ergoplatform/ergo) (👨‍💻 48 · 🔀 150 · 📥 39K · 📋 830 - 26% open · ⏱️ 16.01.2023):

	```
	git clone https://github.com/ergoplatform/ergo
	```
</details>

<br>

 _7 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/dusk-network">DUSK - Dusk Network</a></b> (🥈25 ·  ⭐ 710) - High throughput permissionless blockchain aimed at.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/dusk-network/plonk">plonk</a></b> (🥇22 ·  ⭐ 340) - Pure Rust implementation of the PLONK ZKProof System done by the Dusk-.. <code><a href="http://bit.ly/3postzC">MPL-2.0</a></code></summary>

- [GitHub](https://github.com/dusk-network/plonk) (👨‍💻 21 · 🔀 80 · 📥 10 · 📦 120 · 📋 390 - 9% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/dusk-network/plonk
	```
</details>

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/vertcoin-project">VTC - Vertcoin</a></b> (🥈25 ·  ⭐ 550) - Decentralized currency owned by its users, a P2P cryptocurrency in the..</summary>


---
<details><summary><b><a href="https://github.com/vertcoin-project/vertcoin-core">vertcoin-core</a></b> (🥇24 ·  ⭐ 340) - A digital currency with mining decentralisation and ASIC.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/vertcoin-project/vertcoin-core) (👨‍💻 1K · 🔀 120 · 📥 370K · 📋 100 - 11% open · ⏱️ 03.01.2023):

	```
	git clone https://github.com/vertcoin-project/vertcoin-core
	```
</details>

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/nucypher">NU - NuCypher</a></b> (🥈24 ·  ⭐ 1.5K) - Privacy layer for distributed systems and decentralized.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/nucypher/nucypher">nucypher</a></b> (🥈21 ·  ⭐ 670) - A decentralized threshold cryptography network focused on proxy.. <code><a href="http://bit.ly/3pwmjO5">AGPL-3.0</a></code></summary>

- [GitHub](https://github.com/nucypher/nucypher) (👨‍💻 47 · 🔀 250 · 📋 1.6K - 15% open · ⏱️ 10.01.2023):

	```
	git clone https://github.com/nucypher/nucypher
	```
</details>

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/keep-network">KEEP - Keep Network</a></b> (🥈24 ·  ⭐ 410) - Off-chain container for private data. Keeps help contracts.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/keep-network/tbtc">tbtc</a></b> (🥈21 ·  ⭐ 210 · 💤) - Trustlessly tokenized Bitcoin on Ethereum ;). <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/keep-network/tbtc) (👨‍💻 21 · 🔀 40 · 📦 60 · 📋 300 - 18% open · ⏱️ 17.06.2022):

	```
	git clone https://github.com/keep-network/tbtc
	```
</details>

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/deroproject">DERO</a></b> (🥉21 ·  ⭐ 860) - General purpose, private and decentralized application platform that allows..</summary>


---

<br>

 _4 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/turtlecoin">TRTL - TurtleCoin</a></b> (🥉20 ·  ⭐ 940 · 💤) - Privacy coin that is forked from Bytecoin. Because of this,..</summary>


---

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/scala-network">XLA - Scala</a></b> (🥉18 ·  ⭐ 430) - Distributed wealth for all devices.</summary>


---
<details><summary><b><a href="https://github.com/scala-network/MobileMiner">MobileMiner</a></b> (🥈18 ·  ⭐ 240) - A mobile application wrapping XLArig to enable mining on Android.. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/scala-network/MobileMiner) (👨‍💻 8 · 🔀 61 · 📥 360K · 📋 40 - 65% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/scala-network/MobileMiner
	```
</details>

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary>Show 22 hidden projects...</summary>

- <b><a href="https://github.com/findoranetwork">FRA - Findora</a></b> (🥉21 ·  ⭐ 130) - Public blockchain with programmable privacy. Originally conceptualized..
- <b><a href="https://github.com/hoprnet">HOPR</a></b> (🥉20 ·  ⭐ 160) - HOPR provides essential and compliant network-level metadata privacy for.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/phoreproject">PHR - Phore</a></b> (🥉20 ·  ⭐ 130 · 💀) - Grounded in sound technology, Phore is composed of a decentralized..
- <b><a href="https://github.com/PirateNetwork">ARRR - Pirate Chain</a></b> (🥉19 ·  ⭐ 120) - 100% private send cryptocurrency. It uses a privacy.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/letheanvpn">LTHN - Lethean</a></b> (🥉18 ·  ⭐ 180) - Web3 Transit & Data Sovereignty Virtual People Network.
- <b><a href="https://github.com/btcz">BTCZ - BitcoinZ</a></b> (🥉18 ·  ⭐ 120) - BitcoinZ is based on Bitcoin + zkSNARKs and is a decentralized.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/NAVCoin">NAV - NavCoin</a></b> (🥉17 ·  ⭐ 120 · 💤) - What is Navcoin (NAV)? Navcoin, launched in 2014, is an open-.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/haven-protocol-org">XHV - Haven Protocol</a></b> (🥉17 ·  ⭐ 31) - Untraceable cryptocurrency with a mix of standard market..
- <b><a href="https://github.com/ApolloFoundation">APL - Apollo Currency</a></b> (🥉15 ·  ⭐ 94) - Utilizing a community of world-class developers, managers,..
- <b><a href="https://github.com/sero-cash">SERO - Super Zero</a></b> (🥉13 ·  ⭐ 140 · 💀) - Worlds first privacy protection platform for decentralised..
- <b><a href="https://github.com/mwcproject">MWC - MimbleWimbleCoin</a></b> (🥉13 ·  ⭐ 31 · 💤) - Mimblewimble applied in the base layer.
- <b><a href="https://github.com/ryo-currency">RYO - Ryo Currency</a></b> (🥉12 ·  ⭐ 78 · 💀) - Ryo is one of very few cryptonote currencies that does actual,..
- <b><a href="https://github.com/masari-project">MSR - Masari</a></b> (🥉12 ·  ⭐ 71) - Masari describes itself as a fungible, secure, and private cryptocurrency..
- <b><a href="https://github.com/aliascash">ALIAS</a></b> (🥉12 ·  ⭐ 58 · 💀) - Cryptocurrency . Alias has a current supply of 27,174,521.61 with 0 in..
- <b><a href="https://github.com/aleph-zero-foundation">AZERO - Aleph Zero</a></b> (🥉11 ·  ⭐ 140) - Enterprise-ready, high-performance blockchain platform with a..
- <b><a href="https://github.com/DAPSCoin">DAPS - DAPS Coin</a></b> (🥉11 ·  ⭐ 60 · 💤) - Privacy coin that aims to be the most private and secure.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/BeamMW">BEAM</a></b> (🥉10 ·  ⭐ 34 · 💀) - Scalable, fungible, and confidential cryptocurrency based on the Mimblewimble..
- <b><a href="https://github.com/CloakProject">CLOAK - CloakCoin</a></b> (🥉9 ·  ⭐ 33 · 💀) - Completely secure, fully private and untraceable digital cash..
- <b><a href="https://github.com/DecentricCorp">COVAL - Circuits of Value</a></b> (🥉6 ·  ⭐ 60 · 💀) - NFT platform that enables cross-chain transfers of.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/firoorg">FIRO</a></b> (🥉4 ·  ⭐ 77) - Firo - formerly known as Zcoin, is a privacy-focused cryptocurrency that deploys.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/PIVX-Project">PIVX</a></b> (🥉1) - PIVX, a cutting edge User Data Protection oriented blockchain project and cryptocurrency,..
- <b><a href="https://github.com/hyle-team">ZANO</a></b> (🥉1) - Development of a scalable and secure coin, designed for use in e-commerce. The technology..
</details>
<br>

## Automated Market Maker (AMM)

<a href="#contents"><img align="right" width="15" height="15" src="https://git.io/JtehR" alt="Back to top"></a>

_Mechanisms used by most decentralized exchanges to facilitate permissionless trading._

<details><summary><b><a href="https://github.com/Uniswap">UNI - Uniswap</a></b> (🥇38 ·  ⭐ 23K) - Governance token for Uniswap, an Automated Market Marker DEX on.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/Uniswap/interface">interface</a></b> (🥇28 ·  ⭐ 3.9K) - An open source interface for the Uniswap protocol. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/Uniswap/interface) (👨‍💻 170 · 🔀 3.8K · 📋 2.1K - 31% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/Uniswap/interface
	```
</details>
<details><summary><b><a href="https://github.com/Uniswap/web3-react">web3-react</a></b> (🥇23 ·  ⭐ 4.7K) - A simple, maximally extensible, dependency minimized framework.. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/Uniswap/web3-react) (👨‍💻 41 · 🔀 1.3K · 📦 1.8K · 📋 420 - 28% open · ⏱️ 02.02.2023):

	```
	git clone https://github.com/Uniswap/web3-react
	```
</details>
<details><summary><b><a href="https://github.com/Uniswap/v2-core">v2-core</a></b> (🥇23 ·  ⭐ 2.3K · 💤) - Core smart contracts of Uniswap V2. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/Uniswap/v2-core) (👨‍💻 8 · 🔀 2.5K · 📦 22K · 📋 100 - 21% open · ⏱️ 13.07.2022):

	```
	git clone https://github.com/Uniswap/v2-core
	```
</details>
<details><summary><b><a href="https://github.com/Uniswap/v3-core">v3-core</a></b> (🥇22 ·  ⭐ 3.3K) - Core smart contracts of Uniswap v3. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/Uniswap/v3-core) (👨‍💻 15 · 🔀 1.8K · 📦 4.2K · 📋 240 - 12% open · ⏱️ 31.10.2022):

	```
	git clone https://github.com/Uniswap/v3-core
	```
</details>
<details><summary><b><a href="https://github.com/Uniswap/default-token-list">default-token-list</a></b> (🥇22 ·  ⭐ 220) - The Uniswap default token list. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/Uniswap/default-token-list) (👨‍💻 11 · 🔀 720 · 📦 4.1K · 📋 1.3K - 86% open · ⏱️ 27.01.2023):

	```
	git clone https://github.com/Uniswap/default-token-list
	```
</details>
<details><summary><b><a href="https://github.com/Uniswap/v2-periphery">v2-periphery</a></b> (🥈20 ·  ⭐ 910 · 💤) - Peripheral smart contracts for interacting with Uniswap V2. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/Uniswap/v2-periphery) (👨‍💻 6 · 🔀 1.4K · 📦 15K · 📋 72 - 55% open · ⏱️ 13.07.2022):

	```
	git clone https://github.com/Uniswap/v2-periphery
	```
</details>
<details><summary><b><a href="https://github.com/Uniswap/v3-periphery">v3-periphery</a></b> (🥈20 ·  ⭐ 900) - Peripheral smart contracts for interacting with Uniswap v3. <code><a href="http://bit.ly/2KucAZR">GPL-2.0</a></code></summary>

- [GitHub](https://github.com/Uniswap/v3-periphery) (👨‍💻 15 · 🔀 710 · 📦 4K · 📋 150 - 34% open · ⏱️ 26.10.2022):

	```
	git clone https://github.com/Uniswap/v3-periphery
	```
</details>
<details><summary><b><a href="https://github.com/Uniswap/v3-sdk">v3-sdk</a></b> (🥈20 ·  ⭐ 450 · 💤) - An SDK for building applications on top of Uniswap V3. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/Uniswap/v3-sdk) (👨‍💻 15 · 🔀 300 · 📦 2.4K · 📋 78 - 39% open · ⏱️ 13.07.2022):

	```
	git clone https://github.com/Uniswap/v3-sdk
	```
</details>
<details><summary><b><a href="https://github.com/Uniswap/v2-sdk">v2-sdk</a></b> (🥈20 ·  ⭐ 380 · 💤) - An SDK for building applications on top of Uniswap V2. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/Uniswap/v2-sdk) (👨‍💻 7 · 🔀 880 · 📦 1.5K · 📋 58 - 36% open · ⏱️ 13.07.2022):

	```
	git clone https://github.com/Uniswap/v2-sdk
	```
</details>
<details><summary><b><a href="https://github.com/Uniswap/token-lists">token-lists</a></b> (🥈19 ·  ⭐ 900) - The Token Lists specification. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/Uniswap/token-lists) (👨‍💻 14 · 🔀 610 · 📦 15K · 📋 100 - 72% open · ⏱️ 12.12.2022):

	```
	git clone https://github.com/Uniswap/token-lists
	```
</details>
<details><summary><b><a href="https://github.com/Uniswap/smart-order-router">smart-order-router</a></b> (🥈17 ·  ⭐ 240) -  <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/Uniswap/smart-order-router) (👨‍💻 17 · 🔀 140 · 📦 860 · 📋 75 - 26% open · ⏱️ 23.01.2023):

	```
	git clone https://github.com/Uniswap/smart-order-router
	```
</details>
<details><summary><b><a href="https://github.com/Uniswap/docs">Uniswap/docs</a></b> (🥈17 ·  ⭐ 220) - Uniswap V3 docs website. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/Uniswap/docs) (👨‍💻 110 · 🔀 350 · 📋 160 - 51% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/Uniswap/docs
	```
</details>
<details><summary><b><a href="https://github.com/Uniswap/universal-router">universal-router</a></b> (🥈17 ·  ⭐ 220) - Uniswaps Universal Router for NFT and ERC20 swapping. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/Uniswap/universal-router) (👨‍💻 8 · 🔀 38 · 📦 120 · 📋 76 - 21% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/Uniswap/universal-router
	```
</details>
<details><summary><b><a href="https://github.com/Uniswap/merkle-distributor">merkle-distributor</a></b> (🥈16 ·  ⭐ 500) - A smart contract that distributes a balance of tokens.. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/Uniswap/merkle-distributor) (👨‍💻 5 · 🔀 300 · 📦 1.7K · 📋 13 - 15% open · ⏱️ 21.11.2022):

	```
	git clone https://github.com/Uniswap/merkle-distributor
	```
</details>

<br>

 _26 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/pancakeswap">CAKE - PancakeSwap</a></b> (🥇31 ·  ⭐ 4.7K) - Automated market maker (AMM) that allows two tokens to be.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/pancakeswap/pancake-frontend">pancake-frontend</a></b> (🥇26 ·  ⭐ 2.1K) - Pancake main features (farms, pools, IFO, lottery,.. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/pancakeswap/pancake-frontend) (👨‍💻 91 · 🔀 3.1K · 📦 14 · 📋 400 - 10% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/pancakeswap/pancake-frontend
	```
</details>
<details><summary><b><a href="https://github.com/pancakeswap/pancake-toolkit">pancake-toolkit</a></b> (🥈17 ·  ⭐ 290 · 💤) - Pancake frontend packages. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/pancakeswap/pancake-toolkit) (👨‍💻 36 · 🔀 780 · 📦 10 · 📋 36 - 58% open · ⏱️ 12.05.2022):

	```
	git clone https://github.com/pancakeswap/pancake-toolkit
	```
</details>

<br>

 _14 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/sushiswap">SUSHI</a></b> (🥇31 ·  ⭐ 3.3K) - DeFi protocol that is completely community-driven, serving up delicious.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/sushiswap/sushiswap">sushiswap</a></b> (🥇28 ·  ⭐ 1.7K) - Sushi 2.0. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/sushiswap/sushiswap) (👨‍💻 36 · 🔀 1.3K · 📦 21 · 📋 210 - 9% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/sushiswap/sushiswap
	```
</details>
<details><summary><b><a href="https://github.com/sushiswap/trident">trident</a></b> (🥈18 ·  ⭐ 220) - Rapid AMM Development Framework. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/sushiswap/trident) (👨‍💻 20 · 🔀 72 · 📦 140 · 📋 41 - 48% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/sushiswap/trident
	```
</details>
<details><summary><b><a href="https://github.com/sushiswap/sushiswap-interface">sushiswap-interface</a></b> (🥈16 ·  ⭐ 320) - An open source interface for the SushiSwap Protocol. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/sushiswap/sushiswap-interface) (👨‍💻 120 · 🔀 580 · 📋 220 - 37% open · ⏱️ 06.02.2023):

	```
	git clone https://github.com/sushiswap/sushiswap-interface
	```
</details>

<br>

 _14 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/osmosis-labs">OSMO - Osmosis</a></b> (🥈30 ·  ⭐ 1.2K) - Token of the Osmosis Hub, first DEX for IBC connected coins.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/osmosis-labs/osmosis">osmosis</a></b> (🥇26 ·  ⭐ 690) - The AMM Laboratory. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/osmosis-labs/osmosis) (👨‍💻 110 · 🔀 330 · 📥 5.9K · 📋 1.4K - 21% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/osmosis-labs/osmosis
	```
</details>

<br>

 _7 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/balancer-labs">BAL - Balancer</a></b> (🥈28 ·  ⭐ 1.2K) - Non-custodial portfolio manager, liquidity provider, and price.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/balancer-labs/balancer-v2-monorepo">balancer-v2-monorepo</a></b> (🥇22 ·  ⭐ 210) - Balancer V2 Monorepo. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/balancer-labs/balancer-v2-monorepo) (👨‍💻 23 · 🔀 230 · 📦 86 · 📋 400 - 16% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/balancer-labs/balancer-v2-monorepo
	```
</details>

<br>

 _10 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/1inch">1INCH</a></b> (🥈26 ·  ⭐ 2.3K) - What is 1inch Network? 1inch Network is a decentralized exchange (DEX).. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/1inch/shieldy">shieldy</a></b> (🥈16 ·  ⭐ 690 · 💤) - @shieldy_bot Telegram bot repository. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/1inch/shieldy) (👨‍💻 42 · 🔀 250 · 📦 2 · 📋 120 - 36% open · ⏱️ 20.04.2022):

	```
	git clone https://github.com/1inch/shieldy
	```
</details>

<br>

 _16 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/curvefi">CRV - Curve DAO Token</a></b> (🥈23 ·  ⭐ 2.3K) - Similar to Uniswap, Curve Finance is an Automated.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _14 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/bancorprotocol">BNT - Bancor</a></b> (🥉21 ·  ⭐ 1K) - What Is Bancor (BNT)? Bancor is the only decentralized staking.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/bancorprotocol/contracts-solidity">contracts-solidity</a></b> (🥈18 ·  ⭐ 800) - Bancor Protocol Contracts. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/bancorprotocol/contracts-solidity) (👨‍💻 25 · 🔀 410 · 📦 14 · 📋 59 - 1% open · ⏱️ 23.11.2022):

	```
	git clone https://github.com/bancorprotocol/contracts-solidity
	```
</details>

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/anyswap">ANY - Anyswap</a></b> (🥉19 ·  ⭐ 490) - Fully decentralized cross chain swap protocol, based on.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOi" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _7 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/orca-so">ORCA</a></b> (🥉17 ·  ⭐ 240) - Most user-friendly DEX on Solana. Orca is one of the first general-purpose.. <code><img src="https://git.io/J9cOa" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary>Show 8 hidden projects...</summary>

- <b><a href="https://github.com/kybernetwork">KNC - Kyber Network</a></b> (🥉18 ·  ⭐ 810 · 💀) - What Is Kyber Network (KNC)? Kyber Network is a hub of.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/Sifchain">EROWAN - Sifchain</a></b> (🥉18 ·  ⭐ 100) - Brainchild of Sif, the Norse goddess of earth, agriculture,.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/QuickSwap">QUICK - QuickSwap</a></b> (🥉15 ·  ⭐ 130) - Decentralized exchange (DEX) on the Polygon Network. It is a.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/apeswapfinance">BANANA - ApeSwap</a></b> (🥉13 ·  ⭐ 130) - Decentralized finance (DeFi) platform offering a full.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/ubeswap">UBE - Ubeswap</a></b> (🥉10 ·  ⭐ 31) - Automated market maker on the Celo blockchain.
- <b><a href="https://github.com/coinfi">COFI - CoinFi</a></b> (🥉8 ·  ⭐ 120) - Cryptocurrency and operates on the Ethereum platform. CoinFi has a.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/SpookySwap">BOO - Spookyswap</a></b> (🥉6 ·  ⭐ 34 · 💀) - Token for an automated market-making (AMM) decentalized.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOi" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/Sakeswap">SAKE - SakeToken</a></b> (🥉6 ·  ⭐ 32 · 💀) - First governance token in DeFi that supports both spot.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
</details>
<br>

## Metaverse, NFTs & Gaming

<a href="#contents"><img align="right" width="15" height="15" src="https://git.io/JtehR" alt="Back to top"></a>

_Blockchains related to Metaverse, NFTs & Gaming._

<details><summary><b><a href="https://github.com/dfinity">ICP - Internet Computer</a></b> (🥇38 ·  ⭐ 4.9K) - Worlds first blockchain that is capable of running at web..</summary>


---
<details><summary><b><a href="https://github.com/dfinity/motoko">motoko</a></b> (🥇25 ·  ⭐ 360) - Simple high-level language for writing Internet Computer canisters. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/dfinity/motoko) (👨‍💻 46 · 🔀 74 · 📥 96K · 📋 820 - 41% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/dfinity/motoko
	```
</details>
<details><summary><b><a href="https://github.com/dfinity/candid">candid</a></b> (🥇25 ·  ⭐ 220) - Candid Library for the Internet Computer. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/dfinity/candid) (👨‍💻 35 · 🔀 69 · 📥 19K · 📦 1.4K · 📋 120 - 48% open · ⏱️ 06.02.2023):

	```
	git clone https://github.com/dfinity/candid
	```
</details>
<details><summary><b><a href="https://github.com/dfinity/ic">ic</a></b> (🥈21 ·  ⭐ 1.2K) - Internet Computer blockchain source: the client/replica software run.. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/dfinity/ic) (👨‍💻 160 · 🔀 210 · 📦 2 · ⏱️ 08.02.2023):

	```
	git clone https://github.com/dfinity/ic
	```
</details>
<details><summary><b><a href="https://github.com/dfinity/internet-identity">internet-identity</a></b> (🥈21 ·  ⭐ 240) - Internet Identity, a blockchain authentication system.. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/dfinity/internet-identity) (👨‍💻 52 · 🔀 90 · 📥 48K · 📋 95 - 35% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/dfinity/internet-identity
	```
</details>
<details><summary><b><a href="https://github.com/dfinity/examples">dfinity/examples</a></b> (🥈19 ·  ⭐ 380) - Example applications, microservices, and code samples for.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/dfinity/examples) (👨‍💻 73 · 🔀 240 · 📋 77 - 80% open · ⏱️ 06.02.2023):

	```
	git clone https://github.com/dfinity/examples
	```
</details>
<details><summary><b><a href="https://github.com/dfinity/motoko-base">motoko-base</a></b> (🥈17 ·  ⭐ 430) - The Motoko base library. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/dfinity/motoko-base) (👨‍💻 25 · 🔀 70 · 📋 150 - 68% open · ⏱️ 06.02.2023):

	```
	git clone https://github.com/dfinity/motoko-base
	```
</details>

<br>

 _21 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/decentraland">MANA - Decentraland</a></b> (🥇34 ·  ⭐ 2.8K) - Ethereum-powered virtual reality platform. In this.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/decentraland/marketplace">marketplace</a></b> (🥈21 ·  ⭐ 970) - Decentralands NFT Marketplace. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/decentraland/marketplace) (👨‍💻 22 · 🔀 560 · 📦 9 · 📋 620 - 18% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/decentraland/marketplace
	```
</details>

<br>

 _21 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/nervosnetwork">CKB - Nervos Network</a></b> (🥇34 ·  ⭐ 2.6K) - Layered crypto-economy network. Nervos separates the..</summary>


---
<details><summary><b><a href="https://github.com/nervosnetwork/ckb">ckb</a></b> (🥇28 ·  ⭐ 1K) - The Nervos CKB is a public permissionless blockchain, and the layer 1 of Nervos.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/nervosnetwork/ckb) (👨‍💻 47 · 🔀 200 · 📥 31K · 📦 180 · 📋 380 - 11% open · ⏱️ 31.01.2023):

	```
	git clone https://github.com/nervosnetwork/ckb
	```
</details>
<details><summary><b><a href="https://github.com/nervosnetwork/ckb-vm">ckb-vm</a></b> (🥈21 ·  ⭐ 300) - CKBs vm, based on open source RISC-V ISA. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/nervosnetwork/ckb-vm) (👨‍💻 17 · 🔀 48 · 📦 690 · 📋 35 - 31% open · ⏱️ 18.01.2023):

	```
	git clone https://github.com/nervosnetwork/ckb-vm
	```
</details>
<details><summary><b><a href="https://github.com/nervosnetwork/rfcs">rfcs</a></b> (🥈16 ·  ⭐ 240) - This repository contains proposals, standards and documentations related to.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/nervosnetwork/rfcs) (👨‍💻 51 · 🔀 150 · 📋 68 - 29% open · ⏱️ 06.02.2023):

	```
	git clone https://github.com/nervosnetwork/rfcs
	```
</details>

<br>

 _14 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/Conflux-Chain">CFX - Conflux Network</a></b> (🥈29 ·  ⭐ 740) - What is Conflux Conflux is a permissionless Layer 1..</summary>


---
<details><summary><b><a href="https://github.com/Conflux-Chain/conflux-rust">conflux-rust</a></b> (🥇24 ·  ⭐ 570) - The official Rust implementation of Conflux protocol... <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/Conflux-Chain/conflux-rust) (👨‍💻 40 · 🔀 160 · 📥 28K · 📋 550 - 16% open · ⏱️ 01.02.2023):

	```
	git clone https://github.com/Conflux-Chain/conflux-rust
	```
</details>

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/rmrk-team">RMRK</a></b> (🥈22 ·  ⭐ 320) - Most advanced nft system in the world, adding eternal liquidity and multi.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/immutable">IMX - Immutable X</a></b> (🥈20 ·  ⭐ 400) - Native token for the Immutable X network, the first zk-rollup.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _7 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/worldwide-asset-exchange">WAXP - WAX</a></b> (🥈19 ·  ⭐ 270) - Purpose-built blockchain and protocol token designed to make e-commerce..</summary>


---

<br>

 _4 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/monacoinproject">MONA - MonaCoin</a></b> (🥈18 ·  ⭐ 320 · 💤) - Native ERC-20 token for the DIGITALAX platform. It is the.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/monacoinproject/monacoin">monacoin</a></b> (🥈21 ·  ⭐ 320 · 💤) -  <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/monacoinproject/monacoin) (👨‍💻 870 · 🔀 89 · 📥 160K · 📋 41 - 9% open · ⏱️ 21.05.2022):

	```
	git clone https://github.com/monacoinproject/monacoin
	```
</details>

---
</details>
<details><summary><b><a href="https://github.com/axieinfinity">AXS - Axie Infinity</a></b> (🥈17 ·  ⭐ 230) - Governance token for the Axie Infinity game. Token holders.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/aavegotchi">GHST - Aavegotchi</a></b> (🥉16 ·  ⭐ 360) - Eco-governance token of Aavegotchi. Use GHST to purchase.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/aavegotchi/aavegotchi-contracts">aavegotchi-contracts</a></b> (🥈16 ·  ⭐ 240) - Aavegotchi diamond and its facets, deployment and.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/aavegotchi/aavegotchi-contracts) (👨‍💻 19 · 🔀 110 · 📋 90 - 14% open · ⏱️ 05.02.2023):

	```
	git clone https://github.com/aavegotchi/aavegotchi-contracts
	```
</details>

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/Neos-Metaverse">NCR - Neos Credits</a></b> (🥉16 ·  ⭐ 230) - Metaverse engineered for the unknown. Its one of the most.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary>Show 16 hidden projects...</summary>

- <b><a href="https://github.com/FeatherCoin">FTC - Feathercoin</a></b> (🥈18 ·  ⭐ 130) - Open source cryptocurrency, published under the license of MIT /..
- <b><a href="https://github.com/gemhq">GEM - NFTmall</a></b> (🥈17 ·  ⭐ 180) - What is NFTmall (GEM)? NFTmall- Pioneers of Multichain GameFi.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/enjin">ENJ - Enjin Coin</a></b> (🥉16 ·  ⭐ 170) - Cryptocurrency for virtual goods created by Enjin. Enjin is.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/thesandboxgame">SAND - The Sandbox</a></b> (🥉15 ·  ⭐ 200) - Community-driven platform where creators can monetize voxel.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/SINOVATEblockchain">SIN - SINOVATE</a></b> (🥉14 ·  ⭐ 62 · 💤) - Metaverse multi-player game built on Blockchain Technology,.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/bosonprotocol">BOSON - Boson Protocol</a></b> (🥉13 ·  ⭐ 66) - Trust minimised and cost minimised protocol that.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/assetmantle">MNTL - AssetMantle</a></b> (🥉13 ·  ⭐ 41) - Multi-tenant NFT marketplace framework that enables creators.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/adshares">ADS - Adshares</a></b> (🥉13 ·  ⭐ 36) - Web3 protocol for monetization space in the Metaverse. Adserver.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/blocklords">CWS - Crowns</a></b> (🥉10 ·  ⭐ 80) - Cryptocurrency launched in 2021and operates on the Ethereum.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/XYOracleNetwork">XYO</a></b> (🥉9 ·  ⭐ 94 · 💀) - Our project is called the XY Oracle Network (XYO Network). The XYO Network.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/phantasma-io">SOUL - Phantasma</a></b> (🥉9 ·  ⭐ 75 · 💀) - Fully interoperable, decentralized feature rich blockchain... <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/allartprotocol">AART - ALL.ART</a></b> (🥉8 ·  ⭐ 260 · 💤) - ALL.ART Protocol brings better NFT standards, proper ownership.. <code><img src="https://git.io/J9cOa" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/staratlasmeta">POLIS - Star Atlas DAO</a></b> (🥉8 ·  ⭐ 36) - Next-gen gaming metaverse emerging from the confluence.. <code><img src="https://git.io/J9cOa" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/dypfinance">DYP - DeFi Yield Protocol</a></b> (🥉7 ·  ⭐ 190 · 💀) - Cryptocurrency launched in 2020and operates on the.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/dopedao">PAPER - Dope Wars Paper</a></b> (🥉5 ·  ⭐ 100 · 💤) - A DAO for the streets. Build a character and develop.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/DefiKingdoms">JEWEL - DeFi Kingdoms</a></b> (🥉4 ·  ⭐ 110 · 💤) - DeFi Kingdoms blends the appeal of decentralized.. <code><img src="https://git.io/J9cOK" style="display:inline;" width="13" height="13"></code>
</details>
<br>

## Top Internet of Things (IOT)

<a href="#contents"><img align="right" width="15" height="15" src="https://git.io/JtehR" alt="Back to top"></a>

_Blockchains designed to facilitate the exchange of data and payment information with other IoT devices._

<details><summary><b><a href="https://github.com/iotaledger">MIOTA - IOTA</a></b> (🥇36 ·  ⭐ 6.4K) - Distributed ledger for the Internet of Things. The first ledger with..</summary>


---
<details><summary><b><a href="https://github.com/iotaledger/wasp">wasp</a></b> (🥇30 ·  ⭐ 260) - Node for IOTA Smart Contracts. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/iotaledger/wasp) (👨‍💻 54 · 🔀 120 · 📥 56K · 📦 43 · 📋 200 - 52% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/iotaledger/wasp
	```
</details>
<details><summary><b><a href="https://github.com/iotaledger/goshimmer">goshimmer</a></b> (🥇28 ·  ⭐ 380) - Prototype implementation of IOTA 2.0. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/iotaledger/goshimmer) (👨‍💻 55 · 🔀 120 · 📥 41K · 📦 92 · 📋 740 - 8% open · ⏱️ 31.01.2023):

	```
	git clone https://github.com/iotaledger/goshimmer
	```
</details>
<details><summary><b><a href="https://github.com/iotaledger/firefly">firefly</a></b> (🥇26 ·  ⭐ 420) - The official IOTA and Shimmer wallet. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/iotaledger/firefly) (👨‍💻 31 · 🔀 86 · 📥 72K · 📋 3K - 16% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/iotaledger/firefly
	```
</details>
<details><summary><b><a href="https://github.com/iotaledger/iota.rs">iota.rs</a></b> (🥇25 ·  ⭐ 220) - Official IOTA Rust library. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/iotaledger/iota.rs) (👨‍💻 67 · 🔀 92 · 📥 25K · 📦 76 · 📋 390 - 5% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/iotaledger/iota.rs
	```
</details>
<details><summary><b><a href="https://github.com/iotaledger/hornet">hornet</a></b> (🥇24 ·  ⭐ 280) - HORNET is a powerful IOTA fullnode software. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/iotaledger/hornet) (👨‍💻 49 · 🔀 140 · 📥 15K · 📦 23 · 📋 470 - 4% open · ⏱️ 02.02.2023):

	```
	git clone https://github.com/iotaledger/hornet
	```
</details>
<details><summary><b><a href="https://github.com/iotaledger/identity.rs">identity.rs</a></b> (🥇23 ·  ⭐ 260) - Implementation of the Decentralized Identity standards such as.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/iotaledger/identity.rs) (👨‍💻 31 · 🔀 59 · 📦 71 · 📋 260 - 23% open · ⏱️ 06.02.2023):

	```
	git clone https://github.com/iotaledger/identity.rs
	```
</details>
<details><summary><b><a href="https://github.com/iotaledger/iota.js">iota.js</a></b> (🥇22 ·  ⭐ 960) - IOTA JavaScript. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/iotaledger/iota.js) (👨‍💻 63 · 🔀 300 · 📥 62 · 📦 20 · 📋 260 - 1% open · ⏱️ 19.01.2023):

	```
	git clone https://github.com/iotaledger/iota.js
	```
</details>
<details><summary><b><a href="https://github.com/iotaledger/bee">bee</a></b> (🥈21 ·  ⭐ 280) - A framework for IOTA nodes, clients and applications in Rust. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/iotaledger/bee) (👨‍💻 30 · 🔀 81 · 📥 12K · 📦 5 · 📋 440 - 21% open · ⏱️ 05.10.2022):

	```
	git clone https://github.com/iotaledger/bee
	```
</details>
<details><summary><b><a href="https://github.com/iotaledger/stronghold.rs">stronghold.rs</a></b> (🥈20 ·  ⭐ 420) - Stronghold is a secret management engine written in rust. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/iotaledger/stronghold.rs) (👨‍💻 29 · 🔀 46 · 📥 24 · 📦 160 · 📋 76 - 7% open · ⏱️ 16.11.2022):

	```
	git clone https://github.com/iotaledger/stronghold.rs
	```
</details>
<details><summary><b><a href="https://github.com/iotaledger/iota.go">iota.go</a></b> (🥈17 ·  ⭐ 340 · 💤) - IOTA Go API Library. Find documentation on.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/iotaledger/iota.go) (👨‍💻 32 · 🔀 100 · 📋 99 - 1% open · ⏱️ 25.02.2022):

	```
	git clone https://github.com/iotaledger/iota.go
	```
</details>
<details><summary><b><a href="https://github.com/iotaledger/streams">streams</a></b> (🥈17 ·  ⭐ 220) - IOTA Streams, a framework for cryptographic protocols called.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/iotaledger/streams) (👨‍💻 19 · 🔀 53 · 📥 12 · 📦 12 · 📋 130 - 47% open · ⏱️ 31.10.2022):

	```
	git clone https://github.com/iotaledger/streams
	```
</details>

<br>

 _26 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/helium">HNT - Helium</a></b> (🥇35 ·  ⭐ 3.3K) - Decentralized machine network powered by a physical blockchain. The..</summary>


---
<details><summary><b><a href="https://github.com/helium/gateway-rs">gateway-rs</a></b> (🥇23 ·  ⭐ 220) - The Helium Light Gateway. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/helium/gateway-rs) (👨‍💻 28 · 🔀 89 · 📥 49K · 📋 160 - 13% open · ⏱️ 04.02.2023):

	```
	git clone https://github.com/helium/gateway-rs
	```
</details>
<details><summary><b><a href="https://github.com/helium/miner">miner</a></b> (🥇22 ·  ⭐ 590) - Miner for the helium blockchain. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/helium/miner) (👨‍💻 47 · 🔀 260 · 📋 510 - 27% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/helium/miner
	```
</details>
<details><summary><b><a href="https://github.com/helium/helium-wallet-rs">helium-wallet-rs</a></b> (🥈20 ·  ⭐ 220) - Rust implementation of a helium wallet CLI. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/helium/helium-wallet-rs) (👨‍💻 24 · 🔀 99 · 📥 20K · 📋 130 - 6% open · ⏱️ 07.01.2023):

	```
	git clone https://github.com/helium/helium-wallet-rs
	```
</details>
<details><summary><b><a href="https://github.com/helium/HIP">HIP</a></b> (🥈18 ·  ⭐ 510) - Helium Improvement Proposals. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/helium/HIP) (👨‍💻 120 · 🔀 340 · 📋 140 - 14% open · ⏱️ 01.02.2023):

	```
	git clone https://github.com/helium/HIP
	```
</details>
<details><summary><b><a href="https://github.com/helium/blockchain-core">blockchain-core</a></b> (🥈17 ·  ⭐ 220) -  <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/helium/blockchain-core) (👨‍💻 30 · 🔀 84 · 📋 110 - 35% open · ⏱️ 02.02.2023):

	```
	git clone https://github.com/helium/blockchain-core
	```
</details>

<br>

 _21 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/iotexproject">IOTX - IoTeX</a></b> (🥈30 ·  ⭐ 1.9K) - Trusted Devices, Trusted Data, Trusted DApps. $IOTX. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/iotexproject/iotex-core">iotex-core</a></b> (🥇29 ·  ⭐ 1.5K) - Official implementation of IoTeX blockchain protocol in Go. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/iotexproject/iotex-core) (👨‍💻 86 · 🔀 300 · 📥 63K · 📦 24 · 📋 1.1K - 8% open · ⏱️ 06.02.2023):

	```
	git clone https://github.com/iotexproject/iotex-core
	```
</details>

<br>

 _6 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/vechain">VET - VeChain</a></b> (🥈30 ·  ⭐ 1.2K) - Launched in 2015 as a private consortium network, the VeChain..</summary>


---
<details><summary><b><a href="https://github.com/vechain/thor">thor</a></b> (🥇22 ·  ⭐ 760) - A general purpose blockchain highly compatible with Ethereums ecosystem. <code><a href="http://bit.ly/37RvQcA">LGPL-3.0</a></code></summary>

- [GitHub](https://github.com/vechain/thor) (👨‍💻 22 · 🔀 220 · 📦 15 · 📋 110 - 13% open · ⏱️ 09.12.2022):

	```
	git clone https://github.com/vechain/thor
	```
</details>

<br>

 _8 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/internxt">INXT - Internxt</a></b> (🥈23 ·  ⭐ 320) - Peer-To-Peer cloud computing network that allows users from all.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _4 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/iost-official">IOST</a></b> (🥉21 ·  ⭐ 620) - IOST is building an ultra-high TPS blockchain infrastructure to meet the security..</summary>


---
<details><summary><b><a href="https://github.com/iost-official/go-iost">go-iost</a></b> (🥈18 ·  ⭐ 560) - Official Go implementation of the IOST blockchain. <code><a href="http://bit.ly/37RvQcA">LGPL-3.0</a></code></summary>

- [GitHub](https://github.com/iost-official/go-iost) (👨‍💻 52 · 🔀 110 · 📋 87 - 49% open · ⏱️ 01.11.2022):

	```
	git clone https://github.com/iost-official/go-iost
	```
</details>

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/streamr-dev">XDATA - Streamr XDATA</a></b> (🥉19 ·  ⭐ 690) - Streamr tokenizes streaming data to enable a new way for.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/streamr-dev/network">network</a></b> (🥈15 ·  ⭐ 480) - Monorepo containing all the main components of Streamr Network. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/streamr-dev/network) (👨‍💻 30 · 🔀 18 · 📦 10 · 📋 23 - 65% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/streamr-dev/network
	```
</details>

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/airalab">XRT - Robonomics Network</a></b> (🥉19 ·  ⭐ 380) - Cryptocurrency and operates on the Ethereum platform... <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/airalab/robonomics">robonomics</a></b> (🥈21 ·  ⭐ 200) - Robonomics node implementation. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/airalab/robonomics) (👨‍💻 25 · 🔀 62 · 📥 18K · 📋 180 - 7% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/airalab/robonomics
	```
</details>

<br>

 _1 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary>Show 5 hidden projects...</summary>

- <b><a href="https://github.com/DigiByte-Core">DGB - DigiByte</a></b> (🥉17 ·  ⭐ 78 · 💀) - What is DigiByte? DigiByte is more than a faster digital currency...
- <b><a href="https://github.com/NodleCode">NODL - Nodle Network</a></b> (🥉14 ·  ⭐ 360) - Nodle Network connects the physical world to Web3 by using..
- <b><a href="https://github.com/alisproject">ALIS</a></b> (🥉14 ·  ⭐ 240) - Cryptocurrency and operates on the Ethereum platform. ALIS has a current.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/CREDITSCOM">CS - Credits</a></b> (🥉12 ·  ⭐ 150 · 💀) - CREDITS platform offers a new and unique technical implementation of..
- <b><a href="https://github.com/ambrosus">AMB - Ambrosus</a></b> (🥉8 ·  ⭐ 41 · 💀) - Decentralized autonomous organization governing the Ambrosus..
</details>
<br>

## Others

<a href="#contents"><img align="right" width="15" height="15" src="https://git.io/JtehR" alt="Back to top"></a>

<details><summary><b><a href="https://github.com/terra-money">LUNA - Terra</a></b> (🥇36 ·  ⭐ 3.3K) - Terra 2.0 which will assume the Terra name is a new blockchain launched..</summary>


---
<details><summary><b><a href="https://github.com/terra-money/terra.js">terra.js</a></b> (🥇24 ·  ⭐ 260) - JavaScript SDK for Terra, written in TypeScript. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/terra-money/terra.js) (👨‍💻 34 · 🔀 160 · 📦 2K · 📋 150 - 3% open · ⏱️ 07.09.2022):

	```
	git clone https://github.com/terra-money/terra.js
	```
</details>
<details><summary><b><a href="https://github.com/terra-money/classic-core">classic-core</a></b> (🥇22 ·  ⭐ 990) - GO implementation of the Terra Protocol. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/terra-money/classic-core) (👨‍💻 36 · 🔀 300 · 📥 27K · 📦 11 · 📋 330 - 8% open · ⏱️ 06.12.2022):

	```
	git clone https://github.com/terra-money/classic-core
	```
</details>
<details><summary><b><a href="https://github.com/terra-money/core">terra-money/core</a></b> (🥈18 ·  ⭐ 340) - GO implementation of the Terra Protocol. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/terra-money/core) (👨‍💻 11 · 🔀 70 · 📥 2.4K · 📦 14 · 📋 63 - 22% open · ⏱️ 01.02.2023):

	```
	git clone https://github.com/terra-money/core
	```
</details>

<br>

 _22 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/ava-labs">AVAX - Avalanche</a></b> (🥇35 ·  ⭐ 3.8K) - High throughput smart contract blockchainplatform. Validators.. <code><img src="https://git.io/J9cOb" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/ava-labs/avalanchego">avalanchego</a></b> (🥇31 ·  ⭐ 1.8K) - Go implementation of an Avalanche node. <code><a href="http://bit.ly/3aKzpTv">BSD-3</a></code></summary>

- [GitHub](https://github.com/ava-labs/avalanchego) (👨‍💻 76 · 🔀 450 · 📥 120K · 📦 67 · 📋 470 - 17% open · ⏱️ 01.02.2023):

	```
	git clone https://github.com/ava-labs/avalanchego
	```
</details>
<details><summary><b><a href="https://github.com/ava-labs/avalanchejs">avalanchejs</a></b> (🥇22 ·  ⭐ 280) - The Avalanche Platform JavaScript Library. <code><a href="http://bit.ly/3aKzpTv">BSD-3</a></code></summary>

- [GitHub](https://github.com/ava-labs/avalanchejs) (👨‍💻 42 · 🔀 110 · 📥 12 · 📦 640 · 📋 110 - 21% open · ⏱️ 26.10.2022):

	```
	git clone https://github.com/ava-labs/avalanchejs
	```
</details>

<br>

 _17 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/metaplex-foundation">MPLX - Metaplex</a></b> (🥇34 ·  ⭐ 5K) - Decentralized protocol for the creation, commerce and use of.. <code><img src="https://git.io/J9cOa" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/metaplex-foundation/metaplex">metaplex</a></b> (🥇22 ·  ⭐ 3.2K · 💤) - A directory of what the Metaplex Foundation works on!. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/metaplex-foundation/metaplex) (👨‍💻 160 · 🔀 6.6K · 📦 1 · 📋 1.2K - 5% open · ⏱️ 13.07.2022):

	```
	git clone https://github.com/metaplex-foundation/metaplex
	```
</details>
<details><summary><b><a href="https://github.com/metaplex-foundation/metaplex-program-library">metaplex-program-library</a></b> (🥈21 ·  ⭐ 500) - Smart contracts maintained by the Metaplex team. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/metaplex-foundation/metaplex-program-library) (👨‍💻 56 · 🔀 460 · 📦 2.7K · 📋 300 - 16% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/metaplex-foundation/metaplex-program-library
	```
</details>
<details><summary><b><a href="https://github.com/metaplex-foundation/js">js</a></b> (🥈21 ·  ⭐ 280) - The new Metaplex JavaScript SDK. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/metaplex-foundation/js) (👨‍💻 32 · 🔀 120 · 📦 2.8K · 📋 240 - 12% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/metaplex-foundation/js
	```
</details>

<br>

 _11 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/AleoHQ">ALEO - aleo.org</a></b> (🥇34 ·  ⭐ 4.2K) - No description.</summary>


---
<details><summary><b><a href="https://github.com/AleoHQ/snarkOS">snarkOS</a></b> (🥇29 ·  ⭐ 2.3K) - A Decentralized Operating System for ZK Applications. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/AleoHQ/snarkOS) (👨‍💻 53 · 🔀 600 · 📥 8.7K · 📦 270 · 📋 540 - 17% open · ⏱️ 08.12.2022):

	```
	git clone https://github.com/AleoHQ/snarkOS
	```
</details>
<details><summary><b><a href="https://github.com/AleoHQ/leo">leo</a></b> (🥇25 ·  ⭐ 390) - The Leo Programming Language. A Programming Language for Formally Verified,.. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/AleoHQ/leo) (👨‍💻 32 · 🔀 52 · 📥 20K · 📦 26 · 📋 860 - 20% open · ⏱️ 05.02.2023):

	```
	git clone https://github.com/AleoHQ/leo
	```
</details>
<details><summary><b><a href="https://github.com/AleoHQ/snarkVM">snarkVM</a></b> (🥇23 ·  ⭐ 340) - A Virtual Machine for Zero-Knowledge Executions. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/AleoHQ/snarkVM) (👨‍💻 31 · 🔀 220 · 📦 340 · 📋 210 - 44% open · ⏱️ 06.02.2023):

	```
	git clone https://github.com/AleoHQ/snarkVM
	```
</details>
<details><summary><b><a href="https://github.com/AleoHQ/aleo">aleo</a></b> (🥇22 ·  ⭐ 280) - A Software Development Kit (SDK) for Zero-Knowledge Transactions. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/AleoHQ/aleo) (👨‍💻 18 · 🔀 270 · 📥 340 · 📦 57 · 📋 50 - 56% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/AleoHQ/aleo
	```
</details>

<br>

 _4 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/aptos-labs">APTOS - AptosLabs</a></b> (🥇33 ·  ⭐ 5.1K) - No description.</summary>


---
<details><summary><b><a href="https://github.com/aptos-labs/aptos-core">aptos-core</a></b> (🥇36 ·  ⭐ 4.9K) - Aptos is a layer 1 blockchain built to support the widespread.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/aptos-labs/aptos-core) (👨‍💻 460 · 🔀 2.9K · 📥 120K · 📦 330 · 📋 1.2K - 23% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/aptos-labs/aptos-core
	```
</details>

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/onflow">FLOW</a></b> (🥇33 ·  ⭐ 3.2K) - Flow by Dapper Labs(The company whichcreated CryptoKitties) is a blockchain.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/onflow/flow-go">flow-go</a></b> (🥇28 ·  ⭐ 470) - A fast, secure, and developer-friendly blockchain built to support.. <code><a href="http://bit.ly/3pwmjO5">AGPL-3.0</a></code></summary>

- [GitHub](https://github.com/onflow/flow-go) (👨‍💻 90 · 🔀 140 · 📦 240 · 📋 470 - 52% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/onflow/flow-go
	```
</details>
<details><summary><b><a href="https://github.com/onflow/cadence">cadence</a></b> (🥇25 ·  ⭐ 460) - Cadence, the resource-oriented smart contract programming language. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/onflow/cadence) (👨‍💻 100 · 🔀 120 · 📦 8 · 📋 670 - 25% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/onflow/cadence
	```
</details>
<details><summary><b><a href="https://github.com/onflow/fcl-js">fcl-js</a></b> (🥇24 ·  ⭐ 310) - FCL (Flow Client Library) - The best tool for building JavaScript.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/onflow/fcl-js) (👨‍💻 69 · 🔀 110 · 📦 810 · 📋 430 - 20% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/onflow/fcl-js
	```
</details>
<details><summary><b><a href="https://github.com/onflow/flow-go-sdk">flow-go-sdk</a></b> (🥇24 ·  ⭐ 200) - Tools for building Go applications on Flow. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/onflow/flow-go-sdk) (👨‍💻 55 · 🔀 72 · 📦 430 · 📋 100 - 26% open · ⏱️ 13.01.2023):

	```
	git clone https://github.com/onflow/flow-go-sdk
	```
</details>
<details><summary><b><a href="https://github.com/onflow/flow-nft">flow-nft</a></b> (🥈18 ·  ⭐ 430) - The non-fungible token standard on the Flow blockchain. <code><a href="http://bit.ly/3rvuUlR">Unlicense</a></code></summary>

- [GitHub](https://github.com/onflow/flow-nft) (👨‍💻 26 · 🔀 160 · 📦 2 · 📋 63 - 26% open · ⏱️ 01.02.2023):

	```
	git clone https://github.com/onflow/flow-nft
	```
</details>
<details><summary><b><a href="https://github.com/onflow/kitty-items">kitty-items</a></b> (🥈16 ·  ⭐ 400) - Kitty Items: CryptoKitties Sample App. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/onflow/kitty-items) (👨‍💻 38 · 🔀 210 · 📦 2 · 📋 150 - 16% open · ⏱️ 28.10.2022):

	```
	git clone https://github.com/onflow/kitty-items
	```
</details>

<br>

 _12 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/ensdomains">ENS - Ethereum Name Service</a></b> (🥇33 ·  ⭐ 3.1K) - The Ethereum Name Service (ENS) is a distributed,.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/ensdomains/ens-contracts">ens-contracts</a></b> (🥈21 ·  ⭐ 340) -  <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/ensdomains/ens-contracts) (👨‍💻 21 · 🔀 230 · 📦 480 · 📋 50 - 74% open · ⏱️ 04.02.2023):

	```
	git clone https://github.com/ensdomains/ens-contracts
	```
</details>

<br>

 _22 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/MixinNetwork">XIN - Mixin</a></b> (🥇32 ·  ⭐ 2.8K) - Free and lightning fast peer-to-peer transactional network for all.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/MixinNetwork/android-app">android-app</a></b> (🥇22 ·  ⭐ 410) - Android private messenger, crypto wallet and light node to Mixin.. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/MixinNetwork/android-app) (👨‍💻 14 · 🔀 86 · 📥 2.5K · 📋 79 - 53% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/MixinNetwork/android-app
	```
</details>
<details><summary><b><a href="https://github.com/MixinNetwork/mixin">mixin</a></b> (🥈20 ·  ⭐ 470) - The Mixin TEE-BFT-DAG network reference implementation. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/MixinNetwork/mixin) (👨‍💻 19 · 🔀 120 · 📥 32K · 📋 62 - 19% open · ⏱️ 29.01.2023):

	```
	git clone https://github.com/MixinNetwork/mixin
	```
</details>
<details><summary><b><a href="https://github.com/MixinNetwork/flutter-plugins">flutter-plugins</a></b> (🥈20 ·  ⭐ 260) - Flutter plugins used in Mixin. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/MixinNetwork/flutter-plugins) (👨‍💻 13 · 🔀 63 · 📦 790 · 📋 150 - 39% open · ⏱️ 05.02.2023):

	```
	git clone https://github.com/MixinNetwork/flutter-plugins
	```
</details>
<details><summary><b><a href="https://github.com/MixinNetwork/ios-app">ios-app</a></b> (🥈19 ·  ⭐ 470) - iOS private messenger, crypto wallet and light node to Mixin Network. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/MixinNetwork/ios-app) (👨‍💻 13 · 🔀 100 · 📋 66 - 50% open · ⏱️ 06.02.2023):

	```
	git clone https://github.com/MixinNetwork/ios-app
	```
</details>

<br>

 _11 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/celo-org">CELO</a></b> (🥇32 ·  ⭐ 1.6K) - Celo enables participation on the Platform, with the opportunity to earn rewards..</summary>


---
<details><summary><b><a href="https://github.com/celo-org/celo-monorepo">celo-monorepo</a></b> (🥇28 ·  ⭐ 630) - Official repository for core projects comprising the Celo.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/celo-org/celo-monorepo) (👨‍💻 210 · 🔀 330 · 📥 52 · 📦 1.7K · 📋 5K - 7% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/celo-org/celo-monorepo
	```
</details>
<details><summary><b><a href="https://github.com/celo-org/celo-blockchain">celo-blockchain</a></b> (🥇26 ·  ⭐ 450) - Official repository for the golang Celo Blockchain. <code><a href="http://bit.ly/37RvQcA">LGPL-3.0</a></code></summary>

- [GitHub](https://github.com/celo-org/celo-blockchain) (👨‍💻 710 · 🔀 150 · 📥 350 · 📦 77 · 📋 840 - 23% open · ⏱️ 06.02.2023):

	```
	git clone https://github.com/celo-org/celo-blockchain
	```
</details>

<br>

 _10 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/ethereum-optimism">OP - Optimism</a></b> (🥇31 ·  ⭐ 4.2K) - Token for the Optimism Collective that governs the Optimism L2..</summary>


---
<details><summary><b><a href="https://github.com/ethereum-optimism/optimism">optimism</a></b> (🥇25 ·  ⭐ 2.6K) - Optimism is Ethereum, scaled. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/ethereum-optimism/optimism) (👨‍💻 120 · 🔀 710 · 📦 2 · 📋 590 - 15% open · ⏱️ 06.02.2023):

	```
	git clone https://github.com/ethereum-optimism/optimism
	```
</details>

<br>

 _14 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/golemfactory">GLM - Golem</a></b> (🥇31 ·  ⭐ 3.5K) - Decentralized supercomputer that is accessible by anyone. The system.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/golemfactory/yagna">yagna</a></b> (🥇26 ·  ⭐ 280) - An open platform and marketplace for distributed computations. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/golemfactory/yagna) (👨‍💻 42 · 🔀 43 · 📥 41K · 📦 40 · 📋 1.2K - 26% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/golemfactory/yagna
	```
</details>

<br>

 _4 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/livepeer">LPT - Livepeer</a></b> (🥇30 ·  ⭐ 1.4K) - The Livepeer project aims to deliver a live video streaming.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/livepeer/go-livepeer">go-livepeer</a></b> (🥇26 ·  ⭐ 490) - Official Go implementation of the Livepeer protocol. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/livepeer/go-livepeer) (👨‍💻 55 · 🔀 150 · 📥 19K · 📦 6 · 📋 1.5K - 16% open · ⏱️ 31.01.2023):

	```
	git clone https://github.com/livepeer/go-livepeer
	```
</details>
<details><summary><b><a href="https://github.com/livepeer/lpms">lpms</a></b> (🥈15 ·  ⭐ 260) - Livepeer media server. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/livepeer/lpms) (👨‍💻 19 · 🔀 60 · 📦 20 · 📋 160 - 26% open · ⏱️ 25.01.2023):

	```
	git clone https://github.com/livepeer/lpms
	```
</details>

<br>

 _7 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/tonlabs">EVER - Everscale</a></b> (🥇30 ·  ⭐ 810) - Fast, secure and scalable network with near-zero fees, which.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _11 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/matter-labs">ZKSYNC - zkSync Coin</a></b> (🥇29 ·  ⭐ 6.7K) - Client driven zk rollup platform from Matter Labs. It is a..</summary>


---
<details><summary><b><a href="https://github.com/matter-labs/zksync">zksync</a></b> (🥈21 ·  ⭐ 2.5K) - zkSync: trustless scaling and privacy engine for Ethereum. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/matter-labs/zksync) (👨‍💻 60 · 🔀 610 · 📋 240 - 11% open · ⏱️ 02.09.2022):

	```
	git clone https://github.com/matter-labs/zksync
	```
</details>

<br>

 _12 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/gitcoinco">GTC - Gitcoin</a></b> (🥇29 ·  ⭐ 2.9K) - Platform to fund builders looking for meaningful, open source work. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/gitcoinco/web">web</a></b> (🥇22 ·  ⭐ 1.6K) - Grow Open Source. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/gitcoinco/web) (👨‍💻 290 · 🔀 810 · 📋 6.1K - 9% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/gitcoinco/web
	```
</details>
<details><summary><b><a href="https://github.com/gitcoinco/passport">passport</a></b> (🥈18 ·  ⭐ 260) - Passport allows users to prove their identity through a secure,.. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/gitcoinco/passport) (👨‍💻 22 · 🔀 92 · 📋 640 - 39% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/gitcoinco/passport
	```
</details>

<br>

 _11 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/JoinColony">CLNY - Colony Network Token</a></b> (🥇29 ·  ⭐ 1.2K) - Hybrid token that contains attributes of government.. <code><img src="https://git.io/J9cOK" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/JoinColony/colonyNetwork">colonyNetwork</a></b> (🥈20 ·  ⭐ 430) - Colony Network smart contracts. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/JoinColony/colonyNetwork) (👨‍💻 38 · 🔀 89 · 📋 370 - 10% open · ⏱️ 06.02.2023):

	```
	git clone https://github.com/JoinColony/colonyNetwork
	```
</details>

<br>

 _7 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/nimiq">NIM - Nimiq</a></b> (🥇28 ·  ⭐ 3.2K) - Nimiq is designed to make cryptocurrencies easy to use for everyone..</summary>


---
<details><summary><b><a href="https://github.com/nimiq/core-js">core-js</a></b> (🥈21 ·  ⭐ 1K) - Official JavaScript implementation of the Nimiq protocol. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/nimiq/core-js) (👨‍💻 44 · 🔀 220 · 📦 93 · 📋 230 - 6% open · ⏱️ 12.11.2022):

	```
	git clone https://github.com/nimiq/core-js
	```
</details>
<details><summary><b><a href="https://github.com/nimiq/qr-scanner">qr-scanner</a></b> (🥈20 ·  ⭐ 1.7K) - Lightweight Javascript QR Code Scanner. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/nimiq/qr-scanner) (👨‍💻 14 · 🔀 430 · 📦 1.4K · 📋 190 - 40% open · ⏱️ 23.11.2022):

	```
	git clone https://github.com/nimiq/qr-scanner
	```
</details>

<br>

 _8 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/MinaProtocol">MINA - Mina Protocol</a></b> (🥇28 ·  ⭐ 1.8K) - The worlds lightest blockchain, powered by participants. Mina..</summary>


---
<details><summary><b><a href="https://github.com/MinaProtocol/mina">mina</a></b> (🥇31 ·  ⭐ 1.7K) - Mina is a new cryptocurrency with a constant size blockchain, improving.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/MinaProtocol/mina) (👨‍💻 93 · 🔀 360 · 📥 990 · 📦 45 · 📋 5.3K - 25% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/MinaProtocol/mina
	```
</details>

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/celestiaorg">CELESTIA</a></b> (🥇28 ·  ⭐ 1.4K) - Modular consensus and data availability layer that allows anyone to build and..</summary>


---
<details><summary><b><a href="https://github.com/celestiaorg/celestia-node">celestia-node</a></b> (🥇24 ·  ⭐ 440) - Celestia Data Availability Nodes. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/celestiaorg/celestia-node) (👨‍💻 33 · 🔀 500 · 📦 10 · 📋 810 - 33% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/celestiaorg/celestia-node
	```
</details>
<details><summary><b><a href="https://github.com/celestiaorg/celestia-core">celestia-core</a></b> (🥇24 ·  ⭐ 280) - Celestia node software based on Tendermint. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/celestiaorg/celestia-core) (👨‍💻 260 · 🔀 100 · 📦 32 · 📋 310 - 13% open · ⏱️ 06.02.2023):

	```
	git clone https://github.com/celestiaorg/celestia-core
	```
</details>

<br>

 _10 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/MysteriumNetwork">MYST - Mysterium</a></b> (🥇28 ·  ⭐ 1.3K) - Launched on 02/03/2022 by a team based in the United States,.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/mysteriumnetwork/node">mysteriumnetwork/node</a></b> (🥇26 ·  ⭐ 960) - Mysterium Network Node - official implementation of.. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/mysteriumnetwork/node) (👨‍💻 45 · 🔀 270 · 📥 49K · 📦 4 · 📋 3.4K - 5% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/mysteriumnetwork/node
	```
</details>

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/cardano-foundation">CML - Camelcoin</a></b> (🥇28 ·  ⭐ 1.1K) - The camelcoin team aims to create and develop a cryptocurrency..</summary>


---
<details><summary><b><a href="https://github.com/cardano-foundation/developer-portal">developer-portal</a></b> (🥈20 ·  ⭐ 300) - The Cardano Developer Portal. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/cardano-foundation/developer-portal) (👨‍💻 180 · 🔀 700 · 📦 2 · 📋 94 - 37% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/cardano-foundation/developer-portal
	```
</details>
<details><summary><b><a href="https://github.com/cardano-foundation/CIPs">CIPs</a></b> (🥈15 ·  ⭐ 390) -  <code><a href="https://tldrlegal.com/search?q=CC-BY-4.0">CC-BY-4.0</a></code></summary>

- [GitHub](https://github.com/cardano-foundation/CIPs) (👨‍💻 71 · 🔀 200 · 📋 110 - 74% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/cardano-foundation/CIPs
	```
</details>

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/bitcoin-abc">XEC - eCash</a></b> (🥇28 ·  ⭐ 1.1K) - Derived from one of the most trusted names in the cryptocurrency space,..</summary>


---
<details><summary><b><a href="https://github.com/Bitcoin-ABC/bitcoin-abc">bitcoin-abc</a></b> (🥇31 ·  ⭐ 1.1K) - Bitcoin ABC develops node software and infrastructure for the eCash.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/Bitcoin-ABC/bitcoin-abc) (👨‍💻 840 · 🔀 670 · 📥 45K · 📦 3 · 📋 360 - 21% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/Bitcoin-ABC/bitcoin-abc
	```
</details>

---
</details>
<details><summary><b><a href="https://github.com/planetarium">WNCG - Wrapped NCG</a></b> (🥇28 ·  ⭐ 820) - Open source, decentralized RPG network powered by the.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/planetarium/libplanet">libplanet</a></b> (🥇27 ·  ⭐ 470) - Blockchain toolkit in C#/.NET for decentralized game. <code><a href="https://tldrlegal.com/search?q=LGPL-2.1">LGPL-2.1</a></code></summary>

- [GitHub](https://github.com/planetarium/libplanet) (👨‍💻 67 · 🔀 130 · 📥 41K · 📦 33 · 📋 760 - 33% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/planetarium/libplanet
	```
</details>
<details><summary><b><a href="https://github.com/planetarium/NineChronicles">NineChronicles</a></b> (🥈19 ·  ⭐ 280) - Unity client application for Nine Chronicles, a fully.. <code><a href="http://bit.ly/3pwmjO5">AGPL-3.0</a></code></summary>

- [GitHub](https://github.com/planetarium/NineChronicles) (👨‍💻 37 · 🔀 110 · 📋 220 - 68% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/planetarium/NineChronicles
	```
</details>

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/opencog">AGI - SingularityNET</a></b> (🥇27 ·  ⭐ 3.7K) - The creators of Singularity also created the citizen robot..</summary>


---
<details><summary><b><a href="https://github.com/opencog/atomspace">atomspace</a></b> (🥇22 ·  ⭐ 670) - The OpenCog (hyper-)graph database and graph rewriting system. <code><a href="http://bit.ly/3pwmjO5">AGPL-3.0</a></code></summary>

- [GitHub](https://github.com/opencog/atomspace) (👨‍💻 150 · 🔀 200 · 📋 620 - 14% open · ⏱️ 04.02.2023):

	```
	git clone https://github.com/opencog/atomspace
	```
</details>
<details><summary><b><a href="https://github.com/opencog/link-grammar">link-grammar</a></b> (🥈21 ·  ⭐ 360) - The CMU Link Grammar natural language parser. <code><a href="https://tldrlegal.com/search?q=LGPL-2.1">LGPL-2.1</a></code></summary>

- [GitHub](https://github.com/opencog/link-grammar) (👨‍💻 39 · 🔀 110 · 📋 340 - 22% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/opencog/link-grammar
	```
</details>
<details><summary><b><a href="https://github.com/opencog/opencog">opencog</a></b> (🥈20 ·  ⭐ 2.2K) - A framework for integrated Artificial Intelligence & Artificial.. <code><a href="http://bit.ly/3pwmjO5">AGPL-3.0</a></code></summary>

- [GitHub](https://github.com/opencog/opencog) (👨‍💻 160 · 🔀 710 · 📋 640 - 8% open · ⏱️ 02.12.2022):

	```
	git clone https://github.com/opencog/opencog
	```
</details>

<br>

 _6 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/superalgos">SA - Superalgos</a></b> (🥇26 ·  ⭐ 3.2K) - Cryptocurrency and operates on the BNB Smart Chain (BEP20).. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/Superalgos/Superalgos">Superalgos</a></b> (🥇29 ·  ⭐ 3.2K) - Free, open-source crypto trading bot, automated bitcoin /.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/Superalgos/Superalgos) (👨‍💻 210 · 🔀 4.9K · 📥 8.1K · 📋 430 - 17% open · ⏱️ 29.12.2022):

	```
	git clone https://github.com/Superalgos/Superalgos
	```
</details>

---
</details>
<details><summary><b><a href="https://github.com/blockstream">BMN - BlockStream Mining Notes</a></b> (🥇26 ·  ⭐ 2.5K) - BlockStream Mining Notes (BMN) is a cryptocurrency ...</summary>


---
<details><summary><b><a href="https://github.com/Blockstream/esplora">esplora</a></b> (🥈17 ·  ⭐ 740) - Explorer for Bitcoin and Liquid. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/Blockstream/esplora) (👨‍💻 27 · 🔀 290 · 📋 280 - 42% open · ⏱️ 31.01.2023):

	```
	git clone https://github.com/Blockstream/esplora
	```
</details>
<details><summary><b><a href="https://github.com/Blockstream/satellite">satellite</a></b> (🥈15 ·  ⭐ 910 · 💤) - Blockstream Satellite. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/Blockstream/satellite) (👨‍💻 5 · 🔀 140 · 📋 60 - 8% open · ⏱️ 30.05.2022):

	```
	git clone https://github.com/Blockstream/satellite
	```
</details>

<br>

 _8 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/AudiusProject">AUDIO - Audius</a></b> (🥇26 ·  ⭐ 1.3K) - Native platform token of the Audius streaming protocol. AUDIO is.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/AudiusProject/audius-protocol">audius-protocol</a></b> (🥈21 ·  ⭐ 480) - The Audius Protocol - Freedom to share, monetize, and.. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/AudiusProject/audius-protocol) (👨‍💻 36 · 🔀 92 · 📦 16 · 📋 85 - 75% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/AudiusProject/audius-protocol
	```
</details>

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/ovrclk">AKT - Akash Network</a></b> (🥇26 ·  ⭐ 1.2K) - Worlds first decentralized cloud computing marketplace, and..</summary>


---
<details><summary><b><a href="https://github.com/akash-network/node">node</a></b> (🥇25 ·  ⭐ 720) - a secure, transparent, and peer-to-peer cloud computing network. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/akash-network/node) (👨‍💻 30 · 🔀 150 · 📥 38K · 📦 3 · 📋 720 - 14% open · ⏱️ 25.01.2023):

	```
	git clone https://github.com/akash-network/node
	```
</details>

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/rsksmart">RBTC - RSK Smart Bitcoin</a></b> (🥇26 ·  ⭐ 930) - First open-source smart contract platform with a 2-way..</summary>


---
<details><summary><b><a href="https://github.com/rsksmart/rskj">rskj</a></b> (🥇23 ·  ⭐ 660) - RSKj is a Java implementation of the RSK protocol. <code><a href="http://bit.ly/37RvQcA">LGPL-3.0</a></code></summary>

- [GitHub](https://github.com/rsksmart/rskj) (👨‍💻 76 · 🔀 260 · 📥 9.3K · 📋 500 - 50% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/rsksmart/rskj
	```
</details>

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/Groestlcoin">GRS - Groestlcoin</a></b> (🥇26 ·  ⭐ 860) - What is Groestlcoin? Groestlcoin is a proof of work cryptocurrency..</summary>


---
<details><summary><b><a href="https://github.com/Groestlcoin/groestlcoin">groestlcoin</a></b> (🥇27 ·  ⭐ 800) - Groestlcoin Core integration/staging tree. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/Groestlcoin/groestlcoin) (👨‍💻 1.1K · 🔀 21 · 📥 22K · ⏱️ 07.02.2023):

	```
	git clone https://github.com/Groestlcoin/groestlcoin
	```
</details>

<br>

 _1 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/klaytn">KLAY - Klaytn</a></b> (🥇26 ·  ⭐ 640) - Public blockchain focused on the metaverse, gamefi, and the creator..</summary>


---
<details><summary><b><a href="https://github.com/klaytn/klaytn">klaytn</a></b> (🥇26 ·  ⭐ 330) - Official Go implementation of the Klaytn protocol. <code><a href="http://bit.ly/37RvQcA">LGPL-3.0</a></code></summary>

- [GitHub](https://github.com/klaytn/klaytn) (👨‍💻 50 · 🔀 160 · 📦 60 · 📋 290 - 26% open · ⏱️ 01.02.2023):

	```
	git clone https://github.com/klaytn/klaytn
	```
</details>

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/witnet">WIT - Witnet</a></b> (🥇26 ·  ⭐ 500) - The Witnet protocol enables smart contracts to realize their true..</summary>


---

<br>

 _6 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/symbol">XYM - Symbol</a></b> (🥇26 ·  ⭐ 310) - Symbol is financial fabric for everyone. The spiritual successor to NEM,..</summary>


---

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/hummingbot">HBOT - Hummingbot</a></b> (🥇25 ·  ⭐ 5.4K) - The Hummingbot Governance Token (HBOT) is a standard ERC-20.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/hummingbot/hummingbot">hummingbot</a></b> (🥇28 ·  ⭐ 5.3K) - Hummingbot is open source software that helps you build trading.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/hummingbot/hummingbot) (👨‍💻 200 · 🔀 1.9K · 📥 30 · 📋 2.7K - 16% open · ⏱️ 26.01.2023):

	```
	git clone https://github.com/hummingbot/hummingbot
	```
</details>

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/starcoinorg">STC - Student Coin</a></b> (🥇25 ·  ⭐ 1.4K) - Starcoin is A Layered Smart Contract and Decentralized.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/starcoinorg/starcoin">starcoin</a></b> (🥇27 ·  ⭐ 1.3K) - Starcoin - A Move smart contract blockchain network that scales by.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/starcoinorg/starcoin) (👨‍💻 49 · 🔀 250 · 📥 10K · 📦 170 · 📋 980 - 16% open · ⏱️ 02.02.2023):

	```
	git clone https://github.com/starcoinorg/starcoin
	```
</details>

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/TP-Lab">TPT - TokenPocket Token</a></b> (🥇25 ·  ⭐ 890) - TPT stands for TokenPocket Token. It is a utility.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/TP-Lab/tokens">TP-Lab/tokens</a></b> (🥇23 ·  ⭐ 220) - Token assets for TokenPocket. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/TP-Lab/tokens) (👨‍💻 3.1K · 🔀 5.5K · 📋 250 - 1% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/TP-Lab/tokens
	```
</details>

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/spacemeshos">SMH</a></b> (🥇25 ·  ⭐ 750) - No description.</summary>


---
<details><summary><b><a href="https://github.com/spacemeshos/go-spacemesh">go-spacemesh</a></b> (🥇24 ·  ⭐ 600) - Go Implementation of the Spacemesh protocol full node. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/spacemeshos/go-spacemesh) (👨‍💻 71 · 🔀 150 · 📥 1.2K · 📦 32 · 📋 2K - 12% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/spacemeshos/go-spacemesh
	```
</details>

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/agoric">IST - ST Project</a></b> (🥇25 ·  ⭐ 590) - No description.</summary>


---
<details><summary><b><a href="https://github.com/Agoric/agoric-sdk">agoric-sdk</a></b> (🥇27 ·  ⭐ 270) - monorepo for the Agoric Javascript smart contract platform. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/Agoric/agoric-sdk) (👨‍💻 67 · 🔀 170 · 📦 52 · 📋 3.4K - 33% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/Agoric/agoric-sdk
	```
</details>

<br>

 _1 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/centrifuge">CFG - Centrifuge</a></b> (🥇25 ·  ⭐ 520) - The centrifuge token model powers centrifuge, providing the..</summary>


---

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/idena-network">IDNA - Idena</a></b> (🥇25 ·  ⭐ 230) - Novel way to formalise people on the blockchain without personally.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/saber-hq">SBR - Saber</a></b> (🥇24 ·  ⭐ 940) - Cross-chain stablecoin exchange on Solana. <code><img src="https://git.io/J9cOa" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/saber-hq/stable-swap">stable-swap</a></b> (🥈17 ·  ⭐ 370) - StableSwap by Saber: an automated market maker for mean-.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/saber-hq/stable-swap) (👨‍💻 7 · 🔀 110 · 📥 1.1K · 📦 96 · 📋 15 - 26% open · ⏱️ 07.12.2022):

	```
	git clone https://github.com/saber-hq/stable-swap
	```
</details>

<br>

 _6 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/AcalaNetwork">KAR - Karura</a></b> (🥇24 ·  ⭐ 870) - All-in-one DeFi hub of Kusama. Founded by the Acala Foundation, Karura..</summary>


---
<details><summary><b><a href="https://github.com/AcalaNetwork/Acala">Acala</a></b> (🥈21 ·  ⭐ 680) - Acala - cross-chain DeFi hub and stablecoin based on Substrate for.. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/AcalaNetwork/Acala) (👨‍💻 35 · 🔀 230 · 📥 4 · 📋 900 - 18% open · ⏱️ 07.02.2023):

	```
	git clone https://github.com/AcalaNetwork/Acala
	```
</details>

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/connext">CNXT - Connext</a></b> (🥇24 ·  ⭐ 650) - No description.</summary>


---
<details><summary><b><a href="https://github.com/connext/monorepo">monorepo</a></b> (🥇25 ·  ⭐ 220) - Connext is a modular stack for trust-minimized, generalized communication.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/connext/monorepo) (👨‍💻 35 · 🔀 81 · 📋 1.3K - 10% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/connext/monorepo
	```
</details>

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/namecoin">NMC - Namecoin</a></b> (🥇24 ·  ⭐ 560) - Domain name registry service and was the first coin to fork Bitcoin...</summary>


---
<details><summary><b><a href="https://github.com/namecoin/namecoin-core">namecoin-core</a></b> (🥇25 ·  ⭐ 440) - Namecoin full node + wallet based on the current Bitcoin Core.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/namecoin/namecoin-core) (👨‍💻 1.1K · 🔀 150 · 📋 320 - 45% open · ⏱️ 06.02.2023):

	```
	git clone https://github.com/namecoin/namecoin-core
	```
</details>

<br>

 _1 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/irisnet">IRIS - IRISnet</a></b> (🥇24 ·  ⭐ 460) - Inter-chain Service Infrastructure and Protocol Technology Foundation..</summary>


---
<details><summary><b><a href="https://github.com/irisnet/irishub">irishub</a></b> (🥈21 ·  ⭐ 270) - A BPoS blockchain that enables cross-chain interoperability through a.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/irisnet/irishub) (👨‍💻 58 · 🔀 120 · 📥 2.6K · 📋 630 - 1% open · ⏱️ 13.01.2023):

	```
	git clone https://github.com/irisnet/irishub
	```
</details>

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/subquery">SQT - Squidgame Token</a></b> (🥇24 ·  ⭐ 260) - Data-as-a-service provider that aggregates and organises data..</summary>


---
<details><summary><b><a href="https://github.com/subquery/subql">subql</a></b> (🥇26 ·  ⭐ 260) -  <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/subquery/subql) (👨‍💻 26 · 🔀 100 · 📦 3.8K · 📋 420 - 17% open · ⏱️ 31.01.2023):

	```
	git clone https://github.com/subquery/subql
	```
</details>

---
</details>
<details><summary><b><a href="https://github.com/dapphub">CHAI</a></b> (🥇23 ·  ⭐ 3.7K) - No description.</summary>


---
<details><summary><b><a href="https://github.com/dapphub/dapptools">dapptools</a></b> (🥈19 ·  ⭐ 1.9K) - Dapp, Seth, Hevm, and more. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/dapphub/dapptools) (👨‍💻 99 · 🔀 310 · 📥 550 · 📋 420 - 38% open · ⏱️ 27.12.2022):

	```
	git clone https://github.com/dapphub/dapptools
	```
</details>

<br>

 _10 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/ton-blockchain">TON - Toncoin</a></b> (🥇23 ·  ⭐ 2.9K) - The next gen network to unite all blockchains and the existing.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/ton-blockchain/ton">ton</a></b> (🥈20 ·  ⭐ 1.8K) - Main TON monorepo. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/ton-blockchain/ton) (👨‍💻 29 · 🔀 430 · 📋 340 - 19% open · ⏱️ 31.01.2023):

	```
	git clone https://github.com/ton-blockchain/ton
	```
</details>

<br>

 _16 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/originprotocol">OGN - Origin Protocol</a></b> (🥇23 ·  ⭐ 1.3K) - Governance and value accrual token for Origin Story... <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/OriginProtocol/origin">OriginProtocol/origin</a></b> (🥈21 ·  ⭐ 650 · 💤) - Monorepo for our developer tools and decentralized.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/OriginProtocol/origin) (👨‍💻 190 · 🔀 210 · 📋 1.5K - 28% open · ⏱️ 04.05.2022):

	```
	git clone https://github.com/OriginProtocol/origin
	```
</details>

<br>

 _6 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/okcashpro">OK - OKCash</a></b> (🥇23 ·  ⭐ 1.1K) - OK is open-source; its design is public, nobody owns or.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/okcashpro/okcash">okcash</a></b> (🥇25 ·  ⭐ 1.1K) - OK is the leading multi chain and energy friendly open source electronic.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/okcashpro/okcash) (👨‍💻 160 · 🔀 840 · 📥 8.1K · ⏱️ 06.02.2023):

	```
	git clone https://github.com/okcashpro/okcash
	```
</details>

---
</details>
<details><summary><b><a href="https://github.com/onearmy">OAC - ONE ARMY COIN</a></b> (🥇23 ·  ⭐ 730) - One Army Coin (OAC) is set up to empower green projects and the..</summary>


---
<details><summary><b><a href="https://github.com/ONEARMY/community-platform">community-platform</a></b> (🥇24 ·  ⭐ 520) - A platform to build useful communities that aim to tackle.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/ONEARMY/community-platform) (👨‍💻 85 · 🔀 190 · 📋 740 - 8% open · ⏱️ 01.02.2023):

	```
	git clone https://github.com/ONEARMY/community-platform
	```
</details>

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/skalenetwork">SKL - SKALE</a></b> (🥇23 ·  ⭐ 570) - Cryptocurrency launched in 2020and operates on the Ethereum platform... <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _8 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/nknorg">NKN</a></b> (🥇23 ·  ⭐ 470) - NKN is building the largest blockchain for network sharing. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _8 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/pontem-network">PONT - Pontem Network</a></b> (🥇23 ·  ⭐ 340) - No description.</summary>


---

<br>

 _6 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/regen-network">REGEN</a></b> (🥇23 ·  ⭐ 230) - Token for the Regen Network Platform. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/kaspanet">KAS - Kaspa</a></b> (🥇23 ·  ⭐ 230) - Proof-of-work cryptocurrency which implements the GHOSTDAG protocol...</summary>


---

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/manta-network">MANTA - Manta Network</a></b> (🥇23 ·  ⭐ 210) - Plug-and-play privacy-preservation protocol built to service..</summary>


---

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/forta-network">FORT - Forta</a></b> (🥇23 ·  ⭐ 210) - Real-time detection network for security & operational monitoring.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/developer-dao">CODE - Developer DAO</a></b> (🥇22 ·  ⭐ 2K) - Cryptocurrency and operates on the Ethereum platform. CODE.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/Developer-DAO/web3-ui">web3-ui</a></b> (🥈19 ·  ⭐ 750) - A React UI library for Web3. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/Developer-DAO/web3-ui) (👨‍💻 42 · 🔀 150 · 📦 61 · 📋 130 - 3% open · ⏱️ 10.09.2022):

	```
	git clone https://github.com/Developer-DAO/web3-ui
	```
</details>

<br>

 _10 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/aurora-is-near">AURORA</a></b> (🥇22 ·  ⭐ 970) - EVM built on the NEAR Protocol, delivering a turn-key solution for.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/aurora-is-near/rainbow-bridge">rainbow-bridge</a></b> (🥈17 ·  ⭐ 290) - NEAR Ethereum Decentralized Bridge. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/aurora-is-near/rainbow-bridge) (👨‍💻 29 · 🔀 84 · 📦 9 · 📋 400 - 35% open · ⏱️ 21.01.2023):

	```
	git clone https://github.com/aurora-is-near/rainbow-bridge
	```
</details>
<details><summary><b><a href="https://github.com/aurora-is-near/aurora-engine">aurora-engine</a></b> (🥈16 ·  ⭐ 260) - Aurora Engine implements an Ethereum Virtual Machine (EVM) on.. <code><a href="https://tldrlegal.com/search?q=CC0-1.0">CC0-1.0</a></code></summary>

- [GitHub](https://github.com/aurora-is-near/aurora-engine) (👨‍💻 10 · 🔀 64 · 📥 800 · 📋 170 - 46% open · ⏱️ 07.12.2022):

	```
	git clone https://github.com/aurora-is-near/aurora-engine
	```
</details>

<br>

 _7 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/gridcoin-community">GRC - Gridcoin</a></b> (🥇22 ·  ⭐ 560) - Cryptocurrency . Gridcoin has a current supply of 453,212,614.22992..</summary>


---
<details><summary><b><a href="https://github.com/gridcoin-community/Gridcoin-Research">Gridcoin-Research</a></b> (🥇24 ·  ⭐ 560) - Gridcoin-Research. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/gridcoin-community/Gridcoin-Research) (👨‍💻 93 · 🔀 170 · 📥 81K · 📋 990 - 2% open · ⏱️ 02.02.2023):

	```
	git clone https://github.com/gridcoin-community/Gridcoin-Research
	```
</details>

---
</details>
<details><summary><b><a href="https://github.com/yieldprotocol">YIELD - Yield Protocol</a></b> (🥇22 ·  ⭐ 420) - Cryptocurrency launched in 2021and operates on the.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/Phala-Network">PHA - Phala Network</a></b> (🥇22 ·  ⭐ 350) - Cryptocurrency . Phala Network has a current supply of.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/Phala-Network/phala-blockchain">phala-blockchain</a></b> (🥇24 ·  ⭐ 320) - The Phala Network Blockchain, pRuntime and the bridge. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/Phala-Network/phala-blockchain) (👨‍💻 33 · 🔀 130 · 📥 530 · 📦 4 · 📋 260 - 30% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/Phala-Network/phala-blockchain
	```
</details>

<br>

 _1 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/hop-protocol">HOP - Hop Protocol</a></b> (🥈21 ·  ⭐ 2.5K) - Protocol for sending tokens across rollups and their shared.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/hop-protocol/hop">hop</a></b> (🥇22 ·  ⭐ 2.2K) - Hop Protocol v1 monorepo. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/hop-protocol/hop) (👨‍💻 12 · 🔀 84 · 📋 200 - 5% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/hop-protocol/hop
	```
</details>

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/sunflower-land">SFL - ScarFace Lion</a></b> (🥈21 ·  ⭐ 1.5K) - First meme token with real life utilities Audited KYCed.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/sunflower-land/sunflower-land">sunflower-land</a></b> (🥇22 ·  ⭐ 1.4K) -  <code>Unlicensed</code></summary>

- [GitHub](https://github.com/sunflower-land/sunflower-land) (👨‍💻 73 · 🔀 450 · 📋 640 - 2% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/sunflower-land/sunflower-land
	```
</details>

<br>

 _1 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/blockworks-foundation">MNGO - Mango</a></b> (🥈21 ·  ⭐ 1.1K) - Cryptocurrency launched in 2021and operates on the Solana.. <code><img src="https://git.io/J9cOa" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _10 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/dappnode">NODE - Whole Network</a></b> (🥈21 ·  ⭐ 520) - The DAppNode DAO will be an inclusive hub in which.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/dappnode/DAppNode">DAppNode</a></b> (🥈20 ·  ⭐ 480) - General repository of the project dappnode. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/dappnode/DAppNode) (👨‍💻 18 · 🔀 82 · 📥 55K · 📋 400 - 25% open · ⏱️ 23.01.2023):

	```
	git clone https://github.com/dappnode/DAppNode
	```
</details>

<br>

 _1 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/sentinel-official">DVPN - Sentinel</a></b> (🥈21 ·  ⭐ 490) - Secure and decentralized VPN for the blockchain age. A modern.. <code><img src="https://cosmos.network/favicon.ico" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _4 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/singnet">AGIX - SingularityNET</a></b> (🥈21 ·  ⭐ 280) - Decentralized marketplace for Artificial Intelligence.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _4 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/AmbireTech">ADX - Ambire AdEx</a></b> (🥈21 ·  ⭐ 270) - Ambire AdEx (ADX), previously known as AdEx Network,.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/incognitochain">PRV - Incognito</a></b> (🥈21 ·  ⭐ 220) - Incognito mode for your crypto. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/veruscoin">VRSC - Verus Coin</a></b> (🥈21 ·  ⭐ 210) - Verus Coin aims to be the worlds most advanced technology, zero..</summary>


---

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/ibax-io">IBXC - IBAX Network</a></b> (🥈20 ·  ⭐ 8.2K) - ## What is IBAX Network(IBAX)? IBAX Network is a public..</summary>


---
<details><summary><b><a href="https://github.com/IBAX-io/go-ibax">go-ibax</a></b> (🥈21 ·  ⭐ 7.8K) - An innovative Blockchain Protocol Platform, which everyone can.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/IBAX-io/go-ibax) (👨‍💻 6 · 🔀 6.6K · 📦 6 · 📋 270 - 33% open · ⏱️ 03.02.2023):

	```
	git clone https://github.com/IBAX-io/go-ibax
	```
</details>

<br>

 _6 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/dtube">DTUBE - Dtube Coin</a></b> (🥈20 ·  ⭐ 1.3K) - DTube Coin: a utility token for video, it enables.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/dtube/dtube">dtube</a></b> (🥈17 ·  ⭐ 770) - d.tube app. A full-featured video sharing website, decentralized. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/dtube/dtube) (👨‍💻 23 · 🔀 180 · 📋 300 - 39% open · ⏱️ 09.01.2023):

	```
	git clone https://github.com/dtube/dtube
	```
</details>

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/adamant-im">ADM - ADAMANT Messenger</a></b> (🥈20 ·  ⭐ 1.1K) - True Blockchain messenger, independent from governments,..</summary>


---
<details><summary><b><a href="https://github.com/Adamant-im/adamant">adamant</a></b> (🥈18 ·  ⭐ 400) - ADAMANT Blockchain Node. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/Adamant-im/adamant) (👨‍💻 33 · 🔀 29 · 📦 4 · 📋 4 - 50% open · ⏱️ 14.01.2023):

	```
	git clone https://github.com/Adamant-im/adamant
	```
</details>
<details><summary><b><a href="https://github.com/Adamant-im/adamant-im">adamant-im</a></b> (🥈17 ·  ⭐ 250 · 💤) - ADAMANT Decentralized Messenger. Progressive Web Application.. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/Adamant-im/adamant-im) (👨‍💻 26 · 🔀 42 · 📥 12K · 📋 21 - 14% open · ⏱️ 22.05.2022):

	```
	git clone https://github.com/Adamant-im/adamant-im
	```
</details>

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/concordium">CCD - Concordium</a></b> (🥈20 ·  ⭐ 510) - Concordium offers a trustable state-of-the-science solution with..</summary>


---

<br>

 _4 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/shard-labs">STMATIC - Lido Staked Matic</a></b> (🥈20 ·  ⭐ 380) - Liquid staking solution for MATIC backed by industry-.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/shapeshift">FOX - Shapeshift FOX Token</a></b> (🥈20 ·  ⭐ 370) - ERC-20 token created by ShapeShift which serves as.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/cardstack">CARD - Cardstack</a></b> (🥈20 ·  ⭐ 360) - Open-source framework and consensus protocol that makes.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/cardstack/cardstack">cardstack</a></b> (🥇23 ·  ⭐ 320) - The mono-repo for the core Cardstack framework. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/cardstack/cardstack) (👨‍💻 45 · 🔀 66 · 📦 2 · 📋 530 - 2% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/cardstack/cardstack
	```
</details>

<br>

 _1 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/kleros">PNK - Kleros</a></b> (🥈20 ·  ⭐ 340) - Blockchain Dispute Resolution Layer that provides fast, secure and.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/kleros/kleros">kleros</a></b> (🥈18 ·  ⭐ 230) - Kleros smart contracts. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/kleros/kleros) (👨‍💻 26 · 🔀 69 · 📦 50 · 📋 70 - 20% open · ⏱️ 15.11.2022):

	```
	git clone https://github.com/kleros/kleros
	```
</details>

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/nos">NOS - Nosana</a></b> (🥈20 ·  ⭐ 300) - The Nosana Network will be the leading provider of decentralized.. <code><img src="https://git.io/J9cOa" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/notional-labs">DIG - Dig Chain</a></b> (🥈20 ·  ⭐ 280) - Real estate tokenization, fractionalization, and governance via.. <code><img src="https://cosmos.network/favicon.ico" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/thematters">SPACE - Space Token</a></b> (🥈20 ·  ⭐ 270) - Worlds first NFT pixel art co-creation canvas where.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOx" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/jbx-protocol">JBX - Juicebox</a></b> (🥈20 ·  ⭐ 250) - Build a community around a project, fund it, and program its.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/likecoin">LIKE - LikeCoin</a></b> (🥈20 ·  ⭐ 240) - Decentralized Publishing Infrastructure to empower content.. <code><img src="https://cosmos.network/favicon.ico" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _4 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/energicryptocurrency">NRG - Energi</a></b> (🥈19 ·  ⭐ 2.4K · 💤) - Next-generation proof-of-stake (PoS) cryptocurrency that.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/giveth">GIV - Giveth</a></b> (🥈19 ·  ⭐ 1.2K) - Community focused on Building the Future of Giving using.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/Giveth/giveth-dapp">giveth-dapp</a></b> (🥈19 ·  ⭐ 330 · 💤) - Giveth Dapp for crowdfunding and managing donations on the.. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/Giveth/giveth-dapp) (👨‍💻 55 · 🔀 220 · 📋 1.9K - 18% open · ⏱️ 07.04.2022):

	```
	git clone https://github.com/Giveth/giveth-dapp
	```
</details>

<br>

 _4 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/zeriontech">TBX - Tokenbox</a></b> (🥈19 ·  ⭐ 980) - Platform for professionals only. Traders and Funds participating.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/zeriontech/defi-sdk">defi-sdk</a></b> (🥈16 ·  ⭐ 720) - DeFi SDK Makes Money Lego Work. <code><a href="http://bit.ly/37RvQcA">LGPL-3.0</a></code></summary>

- [GitHub](https://github.com/zeriontech/defi-sdk) (👨‍💻 12 · 🔀 210 · 📋 58 - 56% open · ⏱️ 31.08.2022):

	```
	git clone https://github.com/zeriontech/defi-sdk
	```
</details>

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/cybercongress">BOOT - Bostrom</a></b> (🥈19 ·  ⭐ 780) - The knowledge graph of the Great Web.</summary>


---
<details><summary><b><a href="https://github.com/cybercongress/go-cyber">go-cyber</a></b> (🥈20 ·  ⭐ 330) - Your Superintelligence. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/cybercongress/go-cyber) (👨‍💻 25 · 🔀 75 · 📥 550 · 📦 5 · 📋 360 - 14% open · ⏱️ 23.11.2022):

	```
	git clone https://github.com/cybercongress/go-cyber
	```
</details>

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/unlock-protocol">UDT - Unlock Protocol</a></b> (🥈19 ·  ⭐ 740) - Cryptocurrency launched in 2021and operates on the.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/unlock-protocol/unlock">unlock</a></b> (🥇22 ·  ⭐ 740) - nlock is a protocol for memberships built on a blockchain. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/unlock-protocol/unlock) (👨‍💻 55 · 🔀 190 · 📦 1 · 📋 2K - 7% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/unlock-protocol/unlock
	```
</details>

---
</details>
<details><summary><b><a href="https://github.com/rchain">RHOC - RChain</a></b> (🥈19 ·  ⭐ 730) - Blockchain platform which enables smart contracts. It differs from..</summary>


---
<details><summary><b><a href="https://github.com/rchain/rchain">rchain</a></b> (🥈20 ·  ⭐ 680) - Blockchain (smart contract) platform using CBC-Casper proof of.. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/rchain/rchain) (👨‍💻 100 · 🔀 210 · 📥 6.7K · 📋 290 - 43% open · ⏱️ 30.08.2022):

	```
	git clone https://github.com/rchain/rchain
	```
</details>

<br>

 _1 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/thetatoken">THETA - Theta Network</a></b> (🥈19 ·  ⭐ 680) - Decentralized video streaming network that is powered by..</summary>


---
<details><summary><b><a href="https://github.com/thetatoken/theta-protocol-ledger">theta-protocol-ledger</a></b> (🥈18 ·  ⭐ 350) - Reference implementation of the Theta Blockchain.. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/thetatoken/theta-protocol-ledger) (👨‍💻 9 · 🔀 73 · 📋 65 - 63% open · ⏱️ 31.01.2023):

	```
	git clone https://github.com/thetatoken/theta-protocol-ledger
	```
</details>

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/OrchidTechnologies">OXT - Orchid Protocol</a></b> (🥈19 ·  ⭐ 610) - The Orchid Protocol organizes bandwidth sellers into a.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/OrchidTechnologies/orchid">orchid</a></b> (🥇22 ·  ⭐ 610) - Orchid: VPN, Personal Firewall. <code><a href="http://bit.ly/3pwmjO5">AGPL-3.0</a></code></summary>

- [GitHub](https://github.com/OrchidTechnologies/orchid) (👨‍💻 21 · 🔀 89 · 📥 10K · 📋 64 - 12% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/OrchidTechnologies/orchid
	```
</details>

---
</details>
<details><summary><b><a href="https://github.com/bitcoin-sv">BSV - Bitcoin SV</a></b> (🥈19 ·  ⭐ 580) - Bitcoin SV (BSV) emerged following a hard fork of the Bitcoin Cash..</summary>


---
<details><summary><b><a href="https://github.com/bitcoin-sv/bitcoin-sv">bitcoin-sv</a></b> (🥈21 ·  ⭐ 580) - Bitcoin SV (Satoshi Vision) is the original Bitcoin. This.. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/bitcoin-sv/bitcoin-sv) (👨‍💻 630 · 🔀 270 · 📋 240 - 7% open · ⏱️ 10.11.2022):

	```
	git clone https://github.com/bitcoin-sv/bitcoin-sv
	```
</details>

---
</details>
<details><summary><b><a href="https://github.com/brave-intl">BAT - Basic Attention Token</a></b> (🥈19 ·  ⭐ 360) - Basic Attention Token (BAT) is an open-source,.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _4 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/interlay">KINT - Kintsugi</a></b> (🥈19 ·  ⭐ 230) - ## What Is Kintsugi Network (KINT)? Kintsugi is Interlays canary..</summary>


---

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/factomproject">WFCT - Wrapped FCT</a></b> (🥈19 ·  ⭐ 210) - Cryptocurrency and operates on the Ethereum platform... <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/FactomProject/factomd">factomd</a></b> (🥈21 ·  ⭐ 210) - Factom Daemon. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/FactomProject/factomd) (👨‍💻 96 · 🔀 93 · 📥 250 · 📦 44 · 📋 120 - 25% open · ⏱️ 25.10.2022):

	```
	git clone https://github.com/FactomProject/factomd
	```
</details>

---
</details>
<details><summary><b><a href="https://github.com/DemocracyEarth">UBI - Universal Basic Income</a></b> (🥈18 ·  ⭐ 2.5K · 💤) - First application to be built on top of the Proof of.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/DemocracyEarth/wallet">DemocracyEarth/wallet</a></b> (🥈21 ·  ⭐ 1.5K · 💤) - Censorship resistant democracies. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/DemocracyEarth/wallet) (👨‍💻 38 · 🔀 290 · 📋 390 - 24% open · ⏱️ 21.03.2022):

	```
	git clone https://github.com/DemocracyEarth/wallet
	```
</details>
<details><summary><b><a href="https://github.com/DemocracyEarth/paper">paper</a></b> (🥈15 ·  ⭐ 600 · 💤) - On self sovereign human identity. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/DemocracyEarth/paper) (👨‍💻 56 · 🔀 120 · 📋 100 - 72% open · ⏱️ 23.05.2022):

	```
	git clone https://github.com/DemocracyEarth/paper
	```
</details>

<br>

 _6 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/onyx-protocol">XCN - Onyxcoin</a></b> (🥈18 ·  ⭐ 1.8K) - Web3 blockchain infrastructure protocol that enables developers.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/Onyx-Protocol/Onyx">Onyx</a></b> (🥈20 ·  ⭐ 1.8K) - Onyx. <code><a href="http://bit.ly/3pwmjO5">AGPL-3.0</a></code></summary>

- [GitHub](https://github.com/Onyx-Protocol/Onyx) (👨‍💻 29 · 🔀 360 · 📦 20 · 📋 120 - 0% open · ⏱️ 03.02.2023):

	```
	git clone https://github.com/Onyx-Protocol/Onyx
	```
</details>

---
</details>
<details><summary><b><a href="https://github.com/district0x">DNT - district0x</a></b> (🥈18 ·  ⭐ 1.3K) - Collective of decentralized and autonomous marketplaces and.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _8 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/functionland">FULA - Functionland</a></b> (🥈18 ·  ⭐ 700) - **Functionland** vision is to enable the public to preside over..</summary>


---
<details><summary><b><a href="https://github.com/functionland/fx-fotos">fx-fotos</a></b> (🥈18 ·  ⭐ 620) - Fx Fotos is an opensource gallery app in react native with the same.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/functionland/fx-fotos) (👨‍💻 11 · 🔀 56 · 📋 140 - 51% open · ⏱️ 28.01.2023):

	```
	git clone https://github.com/functionland/fx-fotos
	```
</details>

<br>

 _1 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/iExecBlockchainComputing">RLC - iExec RLC</a></b> (🥈18 ·  ⭐ 460) - Open-source, decentralized cloud computing platform, running on.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/iExecBlockchainComputing/iexec-sdk">iexec-sdk</a></b> (🥈18 ·  ⭐ 400) - CLI and JS library allowing developers to easily interact with.. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/iExecBlockchainComputing/iexec-sdk) (👨‍💻 11 · 🔀 31 · 📦 49 · 📋 67 - 20% open · ⏱️ 13.01.2023):

	```
	git clone https://github.com/iExecBlockchainComputing/iexec-sdk
	```
</details>

<br>

 _1 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/uniquenetwork">QTZ - Quartz</a></b> (🥈18 ·  ⭐ 430) - New user-friendly platform to test extreme innovation in NFTs and build..</summary>


---

<br>

 _4 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/Minds">MINDS</a></b> (🥈18 ·  ⭐ 430) - Cryptocurrency launched in 2018and operates on the Ethereum platform. Minds.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/RequestNetwork">REQ - Request</a></b> (🥈18 ·  ⭐ 360) - What Is Request (REQ)? The Request (REQ) utility token, launched.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/RequestNetwork/requestNetwork">requestNetwork</a></b> (🥇22 ·  ⭐ 360) - A JavaScript library for interacting with the Request Network.. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/RequestNetwork/requestNetwork) (👨‍💻 34 · 🔀 62 · 📦 11 · 📋 78 - 48% open · ⏱️ 06.02.2023):

	```
	git clone https://github.com/RequestNetwork/requestNetwork
	```
</details>

---
</details>
<details><summary><b><a href="https://github.com/BlocknetDX">ABLOCK - ANY Blocknet</a></b> (🥈18 ·  ⭐ 270) - No description. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/bit-country">NUUM - Metaverse.Network</a></b> (🥈18 ·  ⭐ 220) - Metaverse.Network Continuum & Bit.Country is a platform &..</summary>


---

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/naturalselectionlabs">RSS3</a></b> (🥈17 ·  ⭐ 710) - Next-generation feed protocol that powers decentralized social, content, and.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _6 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/cryptoblades">KING - CryptoBlades Kingdoms</a></b> (🥈17 ·  ⭐ 560) - CryptoBlades Kingdoms (KING) is a cryptocurrency.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/CryptoBlades/cryptoblades">cryptoblades</a></b> (🥈17 ·  ⭐ 560) -  <code>Unlicensed</code></summary>

- [GitHub](https://github.com/CryptoBlades/cryptoblades) (👨‍💻 61 · 🔀 260 · 📋 770 - 13% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/CryptoBlades/cryptoblades
	```
</details>

---
</details>
<details><summary><b><a href="https://github.com/overview">WRT - WingRiders</a></b> (🥈17 ·  ⭐ 420) - Automated market maker (AMM) decentralized exchange (DEX) on top of..</summary>


---

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/opynfinance">OSQTH - Opyn Squeeth</a></b> (🥈17 ·  ⭐ 410) - Opyn is building DeFi-native derivatives and options.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/opynfinance/GammaProtocol">GammaProtocol</a></b> (🥈15 ·  ⭐ 210) - The most powerful, capital efficient DeFi options protocol. <code><a href="http://bit.ly/3rvuUlR">Unlicense</a></code></summary>

- [GitHub](https://github.com/opynfinance/GammaProtocol) (👨‍💻 14 · 🔀 81 · 📋 110 - 20% open · ⏱️ 23.08.2022):

	```
	git clone https://github.com/opynfinance/GammaProtocol
	```
</details>

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/nemproject">LYL - LoyalCoin</a></b> (🥈17 ·  ⭐ 290) - Cryptocurrency . LoyalCoin has a current supply of 9,000,000,000..</summary>


---
<details><summary><b><a href="https://github.com/NemProject/NanoWallet">NanoWallet</a></b> (🥈21 ·  ⭐ 290) -  <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/NemProject/NanoWallet) (👨‍💻 40 · 🔀 6 · 📥 790K · 📋 530 - 21% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/NemProject/NanoWallet
	```
</details>

---
</details>
<details><summary><b><a href="https://github.com/darwinia-network">RING - Darwinia Network</a></b> (🥈17 ·  ⭐ 260) - Native token of Darwinia Network. Its used to pay for.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/darwinia-network/darwinia">darwinia</a></b> (🥈20 ·  ⭐ 230) - Universal cross-chain message network. | We are hiring,.. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/darwinia-network/darwinia) (👨‍💻 25 · 🔀 51 · 📥 8.3K · 📋 410 - 3% open · ⏱️ 03.11.2022):

	```
	git clone https://github.com/darwinia-network/darwinia
	```
</details>

<br>

 _1 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/hypersign-protocol">HID - Hypersign identity</a></b> (🥈17 ·  ⭐ 240) - Decentralised Identity & Access Management Security.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/hypersign-protocol/hid-node">hid-node</a></b> (🥈17 ·  ⭐ 210) - A permissionless blockchain network to manage digital identity and.. <code><a href="http://bit.ly/3nYMfla">Apache-2</a></code></summary>

- [GitHub](https://github.com/hypersign-protocol/hid-node) (👨‍💻 2 · 🔀 20 · 📥 370 · 📋 210 - 18% open · ⏱️ 13.12.2022):

	```
	git clone https://github.com/hypersign-protocol/hid-node
	```
</details>

<br>

 _1 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/ao-libre">AOLB - Argentum Online Libre Token B</a></b> (🥈17 ·  ⭐ 230) - Collaborative project using open-source as a banner...</summary>


---

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/newton-blockchain">TONCOIN - The Open Network</a></b> (🥈16 ·  ⭐ 430) - No description.</summary>


---

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/tierion">TNT - Tierion</a></b> (🥈16 ·  ⭐ 380) - Tierion is launching a universal platform for data verification.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _5 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/mstable">MTA - Meta Finance</a></b> (🥈16 ·  ⭐ 340) - Subsidiary of a mother company that has been operating.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/mstable/mStable-contracts">mStable-contracts</a></b> (🥈15 ·  ⭐ 280) - Smart Contracts that make up the core of the mStable.. <code><a href="http://bit.ly/3pwmjO5">AGPL-3.0</a></code></summary>

- [GitHub](https://github.com/mstable/mStable-contracts) (👨‍💻 15 · 🔀 74 · 📦 35 · 📋 22 - 9% open · ⏱️ 06.12.2022):

	```
	git clone https://github.com/mstable/mStable-contracts
	```
</details>

<br>

 _1 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/red">RED</a></b> (🥈16 ·  ⭐ 260) - The Red community is building the worlds first fullstack and blockchain.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _3 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/scryInfo">DDD - Scry.info</a></b> (🥈16 ·  ⭐ 260) - Worlds first blockchain-based quantitative data providing and.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _4 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/Polkadex-Substrate">PDEX - Polkadex</a></b> (🥈16 ·  ⭐ 250) - Cryptocurrency launched in 2021and operates on the Ethereum.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/Polkadex-Substrate/Polkadex">Polkadex</a></b> (🥈19 ·  ⭐ 250) - An Orderbook-based Decentralized Exchange using the Substrate.. <code><a href="http://bit.ly/2M0xdwT">GPL-3.0</a></code></summary>

- [GitHub](https://github.com/Polkadex-Substrate/Polkadex) (👨‍💻 20 · 🔀 82 · 📥 6.5K · 📋 270 - 15% open · ⏱️ 17.01.2023):

	```
	git clone https://github.com/Polkadex-Substrate/Polkadex
	```
</details>

---
</details>
<details><summary><b><a href="https://github.com/EveripediaNetwork">IQ - Everipedia</a></b> (🥈16 ·  ⭐ 200) - Cryptocurrency dedicated to the future of knowledge. The token.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _2 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/celer-network">CELR - Celer Network</a></b> (🥈15 ·  ⭐ 460) - No description. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _6 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/QuarkChain">QKC - QuarkChain</a></b> (🥈15 ·  ⭐ 260 · 💤) - Secure, permission-less, scalable, and decentralized.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/QuarkChain/pyquarkchain">pyquarkchain</a></b> (🥈18 ·  ⭐ 210 · 💤) - Python implementation of QuarkChain. <code><a href="http://bit.ly/34MBwT8">MIT</a></code></summary>

- [GitHub](https://github.com/QuarkChain/pyquarkchain) (👨‍💻 23 · 🔀 110 · 📋 250 - 16% open · ⏱️ 18.04.2022):

	```
	git clone https://github.com/QuarkChain/pyquarkchain
	```
</details>

<br>

 _1 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/gearbox-protocol">GEAR - Gearbox</a></b> (🥈15 ·  ⭐ 260) - Generalized leverage protocol: it allows you to take leverage in.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---

<br>

 _4 projects are hidden because they don't fulfill the minimal requirements._

---
</details>
<details><summary><b><a href="https://github.com/snowfork">BTRST - Braintrust</a></b> (🥈15 ·  ⭐ 230) - First decentralized talent network that connects skilled,.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code></summary>


---
<details><summary><b><a href="https://github.com/Snowfork/snowbridge">snowbridge</a></b> (🥈16 ·  ⭐ 230) - A trustless bridge between Polkadot and Ethereum. <code>Unlicensed</code></summary>

- [GitHub](https://github.com/Snowfork/snowbridge) (👨‍💻 21 · 🔀 86 · 📦 4 · 📋 150 - 14% open · ⏱️ 08.02.2023):

	```
	git clone https://github.com/Snowfork/snowbridge
	```
</details>

---
</details>
<details><summary>Show 322 hidden projects...</summary>

- <b><a href="https://github.com/simpleledger">HONK - Honk Honk</a></b> (🥇24 ·  ⭐ 190 · 💀) - The Clown World meme has been used to state the situation we..
- <b><a href="https://github.com/syscoin">SYS - Syscoin</a></b> (🥇23 ·  ⭐ 190) - Syscoin offers fast, low-cost tokens, assets, and (fractional) NFTs..
- <b><a href="https://github.com/alephium">ALPH - Alephium</a></b> (🥇23 ·  ⭐ 160) - First operational sharded blockchain bringing scalability, ETH-..
- <b><a href="https://github.com/iov-one">IOV - Starname</a></b> (🥈21 ·  ⭐ 1.3K · 💀) - Starname: Reserve your crypto *name. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/gravity-bridge">G-WETH - Gravity Bridge WETH</a></b> (🥈21 ·  ⭐ 170) - No description.
- <b><a href="https://github.com/galacticcouncil">XHDX</a></b> (🥈20 ·  ⭐ 180) - No description.
- <b><a href="https://github.com/lukso-network">LYXE - LUKSO</a></b> (🥈20 ·  ⭐ 160) - Blockchain platform specifically created for the lifestyle.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/peggyjv">SOMM - Sommelier</a></b> (🥈20 ·  ⭐ 150) - Sommelier consists of the Cosmos Stargate SDK, its.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://cosmos.network/favicon.ico" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/composablefi">PICA</a></b> (🥈20 ·  ⭐ 130) - PICA Price Live Data Composable Finance aspires to build an entire ecosystem.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/gridplus">GRID - Grid+</a></b> (🥈20 ·  ⭐ 93) - Grid+ leverages the Ethereum blockchain to give consumers direct.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/provenance-io">HASH - HASH Token</a></b> (🥈20 ·  ⭐ 67) - Cryptocurrency launched in 2021and operates on the Ethereum.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/integritee-network">TEER - Integritee</a></b> (🥈19 ·  ⭐ 200) - The Integritee platform enables firms and dApps to process their..
- <b><a href="https://github.com/pokt-foundation">POKT - Pocket Network</a></b> (🥈19 ·  ⭐ 180) - Blockchain data platform built for applications that uses..
- <b><a href="https://github.com/origintrail">TRAC - OriginTrail</a></b> (🥈19 ·  ⭐ 170) - Cryptocurrency launched in 2018and operates on the Ethereum.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/santiment">SAN - Santiment Network Token</a></b> (🥈19 ·  ⭐ 160) - Todays crypto-markets are driven by crowd sentiment... <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/stride-labs">STRD - Stride</a></b> (🥈19 ·  ⭐ 160) - Multichain liquid staking zone (appchain) on the Cosmos Blockchain...
- <b><a href="https://github.com/garlicoinorg">GRLC - Garlicoin</a></b> (🥈19 ·  ⭐ 140) - New, freshly baked cryptocurrency, born from the shitposts of.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/desmos-labs">DSM - Desmos</a></b> (🥈19 ·  ⭐ 140) - Native token on the Desmos blockchain. Built with Cosmos SDK,.. <code><img src="https://cosmos.network/favicon.ico" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/litentry">LIT - Litentry</a></b> (🥈19 ·  ⭐ 120) - Network that supports DID aggregation protocol and a.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/archethic-foundation">UCO - Archethic</a></b> (🥈19 ·  ⭐ 94) - Layer 1 aiming to create a new Decentralized Internet. Its.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/comdex-official">CMDX - Comdex</a></b> (🥈19 ·  ⭐ 58) - Comdex - DeFi Infrastructure Layer for Cosmos.
- <b><a href="https://github.com/Neufund">NEU - Neumark</a></b> (🥈18 ·  ⭐ 480 · 💀) - Community-owned fundraising platform bridging the worlds of.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/appstorefoundation">APPC - AppCoins</a></b> (🥈18 ·  ⭐ 250 · 💀) - Protocol for the App Economy created by the Aptoide App.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/bloxapp">SSV - SSV Network</a></b> (🥈18 ·  ⭐ 140) - SSV promotes decentralization, security, and liveness across.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/KILTprotocol">KILT - KILT Protocol</a></b> (🥈18 ·  ⭐ 130) - Open-source blockchain protocol for issuing verifiable,.. <code><img src="https://git.io/J9c3v" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/ingenuity-build">QCK - Quicksilver</a></b> (🥈18 ·  ⭐ 92) - Permissionless, sovereign Cosmos SDK zone providing liquid staking..
- <b><a href="https://github.com/aviannetwork">AVN - AVIAN</a></b> (🥈18 ·  ⭐ 46) - Avian Network (AVN) is driven by the community, which is fully.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/dadi">DADI</a></b> (🥈17 ·  ⭐ 240 · 💀) - DADI decentralized web services: a new era of cloud computing services, powered..
- <b><a href="https://github.com/DistributedCollective">SOV - Sovryn</a></b> (🥈17 ·  ⭐ 170) - Non-custodial and permission-less smart contract based system for.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/energywebfoundation">EWT - Energy Web Token</a></b> (🥈17 ·  ⭐ 130) - Native token of the Energy Web Chain, a public, Proof-of-..
- <b><a href="https://github.com/confio">TGD - Tgrade</a></b> (🥈17 ·  ⭐ 120) - Tgrade is built using revolutionary blockchain technology, smart..
- <b><a href="https://github.com/qlcchain">QLC - QLC Chain</a></b> (🥈17 ·  ⭐ 100 · 💤) - Next generation public Blockchain for decentralized Network-as-..
- <b><a href="https://github.com/X9Developers">XSN - Stakenet</a></b> (🥈17 ·  ⭐ 94 · 💀) - Cryptocurrency launched in 2016. Stakenet has a current supply of..
- <b><a href="https://github.com/HTMLCOIN">HTML - HTMLCOIN</a></b> (🥈17 ·  ⭐ 86) - Base coin and gas that powers the Althash hybrid blockchain. The..
- <b><a href="https://github.com/cheqd">CHEQ - CHEQD Network</a></b> (🥈17 ·  ⭐ 53) - CHEQD NETWORK are building the trusted/authentic data.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/qortal">QORT - Qortal</a></b> (🥈17 ·  ⭐ 53) - Qortal is infrastructure that utilizes blockchain technology with a..
- <b><a href="https://github.com/gobytecoin">GBX - GoByte</a></b> (🥈17 ·  ⭐ 34 · 💤) - Fastest, most secure & most affordable Proof-of-Work.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/lux-core">LUX - LUXCoin</a></b> (🥈16 ·  ⭐ 4K · 💀) - Financial tools to grow your wealth - stake and earn.. <code><img src="https://git.io/J9cOi" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/breadwallet">BRD - Bread</a></b> (🥈16 ·  ⭐ 1.4K · 💀) - Blockchains are quickly becoming the modern equivalent of what.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/DAVFoundation">DAV - DAV Coin</a></b> (🥈16 ·  ⭐ 330 · 💀) - Open source software platform that allows anyone to buy or.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/everfinance">WAR - WeStarter</a></b> (🥈16 ·  ⭐ 160) - Wrapped AR introduces a new storage-based paradigm to Ethereum.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/MASQ-Project">MASQ</a></b> (🥈16 ·  ⭐ 150) - Complete open source ecosystem for internet freedom. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/zeitgeistpm">ZTG - Zeitgeist</a></b> (🥈16 ·  ⭐ 140) - Decentralized prediction market protocol. Built in Substrate, it is..
- <b><a href="https://github.com/emercoin">EMC - Emercoin</a></b> (🥈16 ·  ⭐ 120) - Open-source cryptocurrency which originated from Bitcoin, Peercoin and..
- <b><a href="https://github.com/openstfoundation">OST</a></b> (🥈16 ·  ⭐ 120 · 💀) - Setting up a blockchain infrastructure is an expensive and complicated.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/netboxglobal">NBX - Netbox Coin</a></b> (🥈16 ·  ⭐ 98 · 💀) - Cryptocurrency launched in 2019. Netbox Coin has a current.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/Medibloc">MED - MediBloc</a></b> (🥈16 ·  ⭐ 91) - MediBlocs healthcare information platform is a personal data ecosystem..
- <b><a href="https://github.com/viacoin">VIA - Viacoin</a></b> (🥈16 ·  ⭐ 80) - Script-based cryptocurrency derived from Bitcoin. Similar to Bitcoin and..
- <b><a href="https://github.com/graft-project">GRFT - Graft</a></b> (🥈16 ·  ⭐ 79 · 💀) - GRAFT Blockchain aims to bring a lasting solution to most of the..
- <b><a href="https://github.com/vegaprotocol">VEGA - Vega Protocol</a></b> (🥈16 ·  ⭐ 66) - Proof-of-stake blockchain, built on top of Tendermint,.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/duality-solutions">DYN - Dynamic</a></b> (🥈16 ·  ⭐ 60 · 💀) - Cryptocurrency and operates on the BNB Smart Chain (BEP20).. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/fioprotocol">FIO - FIO Protocol</a></b> (🥈16 ·  ⭐ 52) - Decentralized usability layer for the entire blockchain ecosystem..
- <b><a href="https://github.com/ainblockchain">AIN - AI Network</a></b> (🥈16 ·  ⭐ 52) - Blockchain-based, cloud computing network for an open,.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/TokenMarketNet">AMLT</a></b> (🥈15 ·  ⭐ 1.4K · 💀) - Created by the recognized Blockchain Regtech leader Coinfirm, AMLT is the.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/0xcert">ZXC - 0xcert</a></b> (🥈15 ·  ⭐ 340 · 💀) - Open source, a permission-less protocol for certified non-.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/primecoin">XPM - Primecoin</a></b> (🥈15 ·  ⭐ 320 · 💀) - Rather interesting digital currency. It was developed by Sunny..
- <b><a href="https://github.com/exchangeunion">XUC - Exchange Union</a></b> (🥈15 ·  ⭐ 210 · 💀) - Decentralized network that connects individual.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/1hive">HNY - Honey</a></b> (🥈15 ·  ⭐ 180) - DAO that issues and distributes a digital currency called Honey. Honey..
- <b><a href="https://github.com/capsule-corp-ternoa">CAPS - Ternoa</a></b> (🥈15 ·  ⭐ 150) - Memories are an essential part of who we are. If memory plays an.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/subsquid">SQD - Subsquid</a></b> (🥈15 ·  ⭐ 150) - Motivation Decentralized applications require complex and versatile..
- <b><a href="https://github.com/metafam">SEED - MetaGame</a></b> (🥈15 ·  ⭐ 110) - Community currency for MetaGame. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/boggedfinance">BOG - Bogged Finance</a></b> (🥈15 ·  ⭐ 110) - Bogged Finance: Limit Orders, Stop Losses, Sniper, Swaps.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/liqwid-labs">LQ - Liqwid Finance</a></b> (🥈15 ·  ⭐ 100) - Algorithmic and non-custodial liquidity protocol for earning..
- <b><a href="https://github.com/CoinBlack">BLK - BlackCoin</a></b> (🥈15 ·  ⭐ 81 · 💀) - BlackCoin started of as a Proof of Work coin but has evolved.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/jito-foundation">JITOSOL - Jito Staked SOL</a></b> (🥈15 ·  ⭐ 80) - The first liquid staking derivative on Solana to.. <code><img src="https://git.io/J9cOa" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/wagerr">WGR - Wagerr</a></b> (🥈15 ·  ⭐ 70 · 💀) - Wagerr uses distributed blockchain technology to execute.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/crescent-network">BCRE - Liquid Staking Crescent</a></b> (🥈15 ·  ⭐ 57) - No description. <code><img src="https://cosmos.network/favicon.ico" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/cpchain">CPC - CPChain</a></b> (🥈15 ·  ⭐ 50 · 💀) - New distributed infrastructure for next generation.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/mangata-finance">MGX - Mangata X</a></b> (🥈15 ·  ⭐ 50) - DEX parachain in the Polkadot ecosystem with the goal to make.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/SelfKeyFoundation">KEY - SelfKey</a></b> (🥈15 ·  ⭐ 49) - SelfKey provides organizations and individuals with complete.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/muteio">MUTE</a></b> (🥈15 ·  ⭐ 40 · 💀) - Mute Switch: Coming Soon. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/blckchnd">GPH - GraphCoin</a></b> (🥈15 ·  ⭐ 39) - Cryptocurrency . GraphCoin has a current supply of 0. The last known..
- <b><a href="https://github.com/pkt-cash">WPKT - Wrapped PKT</a></b> (🥈15 ·  ⭐ 34) - New blockchain powered by PacketCrypt, the worlds first.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/cocos-bcx">COCOS - COCOS BCX</a></b> (🥉14 ·  ⭐ 2.1K · 💀) - Cocos Blockchain Expedition is a DAO to enable the.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/shenanigandapp">PRTCLE - Particle</a></b> (🥉14 ·  ⭐ 850) - Shenanigan embraces the hard work and dedication it takes to reach..
- <b><a href="https://github.com/multichain">XFC - Football Coin</a></b> (🥉14 ·  ⭐ 820 · 💤) - Digital currency built using technology similar to that of..
- <b><a href="https://github.com/gelatodigital">GEL - Gelato</a></b> (🥉14 ·  ⭐ 540) - Cryptocurrency and operates on the Ethereum platform. Gelato has a.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/solidproof">CRYP - CrypticCoin</a></b> (🥉14 ·  ⭐ 360) - Cryptocurrency . Users are able to generate CRYP through the..
- <b><a href="https://github.com/bcnmy">BICO - Biconomy</a></b> (🥉14 ·  ⭐ 310) - Biconomy provides plug-n-play APIs to make web3.0 user-friendly.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/zigzagexchange">ZZ - ZigZag</a></b> (🥉14 ·  ⭐ 270) - Native, easy-to-use, reliable, fully secure and low fee Decentralized.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/kulupu">KLP - Kulupu</a></b> (🥉14 ·  ⭐ 190) - Cryptocurrency . Kulupu has a current supply of 28,857,458 with 0 in..
- <b><a href="https://github.com/saddle-finance">SDL - Saddle Finance</a></b> (🥉14 ·  ⭐ 170) - Decentralized automated market maker (AMM) on the.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/firmachain">FCT - Firmachain</a></b> (🥉14 ·  ⭐ 150) - Unlocking new limits of electronic contracts with blockchain.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/hermeznetwork">HEZ - Hermez Network</a></b> (🥉14 ·  ⭐ 150 · 💀) - Decentralized zk-rollup focused on scaling payments.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/looksrare">LOOKS - LooksRare</a></b> (🥉14 ·  ⭐ 150) - Community-first NFT marketplace that actively rewards.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/dev-protocol">DEV - Dev Protocol</a></b> (🥉14 ·  ⭐ 130) - Dev Protocol lets GitHub users tokenize their OSS projects.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/novacoin-project">NVC - Novacoin</a></b> (🥉14 ·  ⭐ 110 · 💤) - Hybrid Proof of work (POW) / Proof of stake (POS).. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/SmartCash">SMART - SmartCash</a></b> (🥉14 ·  ⭐ 85 · 💤) - Easy to use, fast, and secure cryptocurrency that supports..
- <b><a href="https://github.com/bitsongofficial">BTSG - BitSong</a></b> (🥉14 ·  ⭐ 72) - Distributed (open source) blockchain music ecosystem born in.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/projectpai">PAI - Parrot USD</a></b> (🥉14 ·  ⭐ 64 · 💀) - DeFi network built on Solana that will include the.. <code><img src="https://git.io/J9cOa" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/tokelplatform">TKL - Tokel</a></b> (🥉14 ·  ⭐ 38) - The future of tokenization.
- <b><a href="https://github.com/kryptokrona">XKR - Kryptokrona</a></b> (🥉14 ·  ⭐ 37) - Decentralized blockchain based on CryptoNote, which forms the basis..
- <b><a href="https://github.com/SyntropyNet">NOIA - Syntropy</a></b> (🥉14 ·  ⭐ 36 · 💀) - Syntropy is transforming the public internet into a secure.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/persistenceOne">XPRT - Persistence</a></b> (🥉14 ·  ⭐ 33) - Interoperable protocol that provides sovereign environments to..
- <b><a href="https://github.com/prepo-io">PPO - prePO</a></b> (🥉13 ·  ⭐ 1.1K) - Decentralized trading platform allowing anyone to gain synthetic.. <code><img src="https://git.io/J9cOh" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/dragonchain">DRGN - Dragonchain</a></b> (🥉13 ·  ⭐ 700 · 💤) - Cryptocurrency and operates on the Ethereum platform... <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/crypto-expert">HBN - HoboNickels</a></b> (🥉13 ·  ⭐ 350 · 💀) - Digital cryptocurrency that was cloned from the.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOx" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/dappradar">RADAR - DappRadar</a></b> (🥉13 ·  ⭐ 240) - The Worlds Dapp Store - tracking and ranking all.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/yoyow-org">YOYOW</a></b> (🥉13 ·  ⭐ 240 · 💀) - Blockchain-based social media platform developed by senior members of.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/HausDAO">HAUS - DAOhaus</a></b> (🥉13 ·  ⭐ 180) - This HAUS doesnt build itself It takes a community, and HausDAO.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/ChorusOne">STSOL - Lido Staked SOL</a></b> (🥉13 ·  ⭐ 180) - Lido-DAO governed liquid staking protocol for the.. <code><img src="https://git.io/J9cOa" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/mailchain">MAILCHAIN</a></b> (🥉13 ·  ⭐ 170 · 💤) - Mailchain claims to be a Multi-Blockchain Messaging Application. Send..
- <b><a href="https://github.com/shardeum">SHM - Shardeum</a></b> (🥉13 ·  ⭐ 130) - EVM-based, linearly scalable smart contract platform that provides low..
- <b><a href="https://github.com/vision-consensus">VS - Vision Metaverse</a></b> (🥉13 ·  ⭐ 120) - Vision has a complete incentive and promotion.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/fundrequest">FND - Rare FND</a></b> (🥉13 ·  ⭐ 110 · 💀) - The first end-to-end crowdfunding platform in blockchain... <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/goldfinch-eng">FIDU</a></b> (🥉13 ·  ⭐ 90) - No description. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/PalletOne">PTN - PalletOne</a></b> (🥉13 ·  ⭐ 86 · 💤) - Cryptocurrency and operates on the Ethereum platform... <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/semuxproject">SEM - Semux</a></b> (🥉13 ·  ⭐ 77 · 💀) - Cryptocurrency . Users are able to generate SEM through the process of..
- <b><a href="https://github.com/Nexusoft">NXS - Nexus</a></b> (🥉13 ·  ⭐ 56) - Community-driven project with the common vision of a world inspired by..
- <b><a href="https://github.com/qitmeer">PMEER - Qitmeer</a></b> (🥉13 ·  ⭐ 52) - Cryptocurrency launched in 2019. Qitmeer has a current supply of..
- <b><a href="https://github.com/vcashorg">VCASH - void.cash</a></b> (🥉13 ·  ⭐ 48 · 💀) - VoidCash provides transaction privacy on the blockchain by.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/joystream">JOYSTREAM</a></b> (🥉13 ·  ⭐ 48) - No description.
- <b><a href="https://github.com/etherzero-org">ETZ - Ether Zero</a></b> (🥉13 ·  ⭐ 45 · 💤) - Cryptocurrency launched in 2019. Users are able to generate ETZ..
- <b><a href="https://github.com/pegnet">PEG - PegNet</a></b> (🥉13 ·  ⭐ 41 · 💀) - Decentralized/DAO, noncustodial, stable coin and crypto token..
- <b><a href="https://github.com/kcc-community">WKCS - Wrapped KCS</a></b> (🥉13 ·  ⭐ 41) - KRC-20 wrapped version of the Kucoin Token (KCS) used in Kucoin..
- <b><a href="https://github.com/pigeoncoin">PGN - Pigeoncoin</a></b> (🥉13 ·  ⭐ 39) - Pigeoncoin was launched on March 21, 2018 with an immediate goal to..
- <b><a href="https://github.com/safex">SFT - Safex Token</a></b> (🥉13 ·  ⭐ 31 · 💀) - New digital asset for sport lovers, designed to be used.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/numbersprotocol">NUM - Numbers Protocol</a></b> (🥉12 ·  ⭐ 4.9K) - Native protocol token of Numbers Protocol. Numbers.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/quillhash">AMPT - Amplify Protocol</a></b> (🥉12 ·  ⭐ 660) - Cryptocurrency launched in 2020and operates on the.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/mosaicnetworks">LDXG - LondonCoinGold</a></b> (🥉12 ·  ⭐ 450 · 💀) - Cryptocurrency launched in 2021and operates on the.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/exzonetwork">EXZO - ExzoCoin 2.0</a></b> (🥉12 ·  ⭐ 330 · 🐣) - Cryptocurrency launched in 2021and operates on the BNB.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/fibosio">FO - FIBOS</a></b> (🥉12 ·  ⭐ 310) - Platform for creating and developing blockchain application ecosystems..
- <b><a href="https://github.com/DOSNetwork">DOS - DOS Network</a></b> (🥉12 ·  ⭐ 270 · 💀) - DOS Network - A Decentralized Oracle Service supporting.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/yieldyak">YAK - Yield Yak</a></b> (🥉12 ·  ⭐ 210) - Easy-to-use tool to earn more yield from DeFi farming on the.. <code><img src="https://git.io/J9cOb" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/MatrixAINetwork">MAN - Matrix AI Network</a></b> (🥉12 ·  ⭐ 200 · 💀) - Global open-source, public, intelligent blockchain-..
- <b><a href="https://github.com/guardaco">GETH - Guarded Ether</a></b> (🥉12 ·  ⭐ 180 · 💀) - Guarded Ethers ($GETH) are ERC20 tokens issued by.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/presearchofficial">PRE - Presearch</a></b> (🥉12 ·  ⭐ 160) - Open, decentralized search engine that rewards community members.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/hifi-finance">MFT - Mainframe</a></b> (🥉12 ·  ⭐ 150) - Platform for decentralized applications. Resistant to.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/octopus-network">OCT - Octopus Network</a></b> (🥉12 ·  ⭐ 120) - Cryptocurrency . OracleChain has a current supply of.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/saitotech">SAITO</a></b> (🥉12 ·  ⭐ 100 · 💤) - Web3 Foundation grant recipient that runs blockchain applications.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/across-protocol">ACX - Across Protocol</a></b> (🥉12 ·  ⭐ 71) - Optimistic cross-chain bridge secured by UMAs optimistic.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/wrkzcoin">WRKZ - WrkzCoin</a></b> (🥉12 ·  ⭐ 64) - Cryptocurrency based on CryptoNote technology. The teams goal is to..
- <b><a href="https://github.com/PascalCoin">PASC - Pascal</a></b> (🥉12 ·  ⭐ 60) - Cryptocurrency launched in 2016. Users are able to generate PASC through..
- <b><a href="https://github.com/aphtoken">APH - Aphroditecoin</a></b> (🥉12 ·  ⭐ 55 · 💀) - Aphelion is used to pay transaction fees on the Aphelion P2P..
- <b><a href="https://github.com/Revolution-Populi">RVP - Revolution Populi</a></b> (🥉12 ·  ⭐ 54) - A decentralized user-controlled layer 1 blockchain.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/EpiK-Protocol">EPK - EpiK Protocol</a></b> (🥉12 ·  ⭐ 50 · 💤) - EpiK Protocol envisions building a decentralized KG.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/project-bitmark">MARKS - Bitmark</a></b> (🥉12 ·  ⭐ 48 · 💀) - Initiative to create an every day use alternative currency. Its..
- <b><a href="https://github.com/bityuan">BTY - Bityuan</a></b> (🥉12 ·  ⭐ 46) - Cryptocurrency . Bityuan has a current supply of 0. The last known price..
- <b><a href="https://github.com/hddcoin-network">HDD - HDDcoin</a></b> (🥉12 ·  ⭐ 40 · 💤) - Cryptocurrency launched in 2021. HDDcoin has a current supply of..
- <b><a href="https://github.com/e-money">NGM - e-Money</a></b> (🥉12 ·  ⭐ 39) - E-Money stablecoins are a further stage in the development of.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/limxtec">BTDX - Bitcloud</a></b> (🥉12 ·  ⭐ 39 · 💤) - Cryptocurrency . Bitcloud has a current supply of..
- <b><a href="https://github.com/ixofoundation">IXO</a></b> (🥉12 ·  ⭐ 39) - The ixo protocol defines a new open standard for producing verifiable claims.. <code><img src="https://cosmos.network/favicon.ico" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/aurarad">AUR - Auroracoin</a></b> (🥉12 ·  ⭐ 33 · 💤) - Cryptocurrency for Iceland that is launched in February.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/euler-xyz">EUL - Euler</a></b> (🥉11 ·  ⭐ 260) - The ability to lend and borrow assets efficiently is a crucial.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/ookitrade">OOKI</a></b> (🥉11 ·  ⭐ 190) - Cryptocurrency launched in 2021and operates on the Ethereum platform. Ooki.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/servalproject">BMH - BlockMesh</a></b> (🥉11 ·  ⭐ 170 · 💤) - Cryptocurrency and operates on the Ethereum platform... <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/bepronetwork">FEVR - RealFevr</a></b> (🥉11 ·  ⭐ 160) - Company established in 2015 in the fantasy markets with a.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/flowchain">DEXG - Dextoken Governance</a></b> (🥉11 ·  ⭐ 140 · 💀) - Decentralized Token Swap Protocol with Speculative AMM. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/arbprotocol">ARB - ARB Protocol</a></b> (🥉11 ·  ⭐ 130) - Decentralized arbitrage protocol. It is designed to conduct.. <code><img src="https://git.io/J9cOa" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/ref-finance">REF - Ref Finance</a></b> (🥉11 ·  ⭐ 130) - Built on top of the cutting-edge protocol NEAR, Ref Finance is the..
- <b><a href="https://github.com/uinb">TAO - Fusotao</a></b> (🥉11 ·  ⭐ 120) - Cryptocurrency launched in 2021and operates on the Near platform...
- <b><a href="https://github.com/effectai">EFX - Effect Network</a></b> (🥉11 ·  ⭐ 100) - The first Blockchain-based framework for the Future-of-.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/cybermiles">CMT - CyberMiles</a></b> (🥉11 ·  ⭐ 74 · 💀) - New blockchain platform designed to facilitate fast.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/windingtree">LIF - Winding Tree</a></b> (🥉11 ·  ⭐ 58 · 💀) - Blockchain-based decentralized open-source travel.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/bulwark-crypto">BWK - Bulwark</a></b> (🥉11 ·  ⭐ 53 · 💀) - Bulwark cryptocurrency (BWK) is a privacy oriented digital currency..
- <b><a href="https://github.com/aresprotocols">ARES - Ares Protocol</a></b> (🥉11 ·  ⭐ 51) - On-chain-verified oracle protocol that provides secure and.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/jsecoin">JSE - JSECOIN</a></b> (🥉11 ·  ⭐ 47 · 💀) - Cryptocurrency and operates on the Ethereum platform. JSECOIN.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/oak-foundation">TUR - Turex</a></b> (🥉11 ·  ⭐ 44) - Decentralized digital currency based on the Binance Smart Chain.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/metahashorg">MHC - #MetaHash</a></b> (🥉11 ·  ⭐ 39 · 💤) - Next-generation network based on the Blockchain 4.0 technology for..
- <b><a href="https://github.com/boxfoundation">BOX - ContentBox</a></b> (🥉11 ·  ⭐ 39 · 💀) - Cryptocurrency and operates on the Ethereum platform... <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/tryvium-travels">TRYV - Tryvium</a></b> (🥉11 ·  ⭐ 39) - Modern booking platform that offers 1.5M accommodations in over.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/marscoin">MARS</a></b> (🥉11 ·  ⭐ 37 · 💀) - The protocol consists of pBTC35A tokens and MARS tokens. Each pBTC35A token.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/razor-network">RAZOR - Razor Network</a></b> (🥉11 ·  ⭐ 35) - Decentralized oracle network, which connects smart.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/qwertycoin-org">QWC - Qwertycoin</a></b> (🥉11 ·  ⭐ 35 · 💤) - Cryptocurrency . Users are able to generate QWC through the..
- <b><a href="https://github.com/arufa-research">BJUNO - StakeEasy bJuno</a></b> (🥉11 ·  ⭐ 35) - Liquid Staking Solution for Juno Network. StakeEasy.. <code><img src="https://cosmos.network/favicon.ico" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/neutroncoin">NTRN - Neutron</a></b> (🥉11 ·  ⭐ 31) - Long-Term, Original SHA256d Proof-of-Stake Coin with Masternode..
- <b><a href="https://github.com/Aircoin-official">AIR - Altair</a></b> (🥉10 ·  ⭐ 2.8K · 💤) - Altair combines the industry-leading infrastructure built by..
- <b><a href="https://github.com/faceterteam">FACE - Faceter</a></b> (🥉10 ·  ⭐ 380 · 💤) - Web 3.0 decentralised platform where people can create.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/digixglobal">DGD - DigixDAO</a></b> (🥉10 ·  ⭐ 210 · 💀) - Digix makes it possible to buy gold in an efficient manner.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/axentro">AXNT - Axentro</a></b> (🥉10 ·  ⭐ 190 · 💀) - No description.
- <b><a href="https://github.com/ddnlink">DDN - Data Delivery Network</a></b> (🥉10 ·  ⭐ 190) - Den Domains lets you register the domain of your.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/reflexer-labs">FLX - Reflexer Ungovernance Token</a></b> (🥉10 ·  ⭐ 180 · 💤) - Flux provides smart contracts and applications with.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/HashEx">TIKI - Tiki Token</a></b> (🥉10 ·  ⭐ 150) - Next evolution of a yield-generating contract on the Binance.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/reef-defi">REEF</a></b> (🥉10 ·  ⭐ 150) - Reef aims to be the future blockchain for DeFi, NFTs, and gaming. Our layer 1.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/cennznet">CENNZ - Centrality</a></b> (🥉10 ·  ⭐ 70) - What is CENNZnet? CENNZnet is a New Zealand built public.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/commerceblock">WDGLD - Wrapped-DGLD</a></b> (🥉10 ·  ⭐ 69) - ERC-20 token pegged with DGLD, wDGLD opens interoperability..
- <b><a href="https://github.com/fractalplatform">FT - Fracton Protocol</a></b> (🥉10 ·  ⭐ 67 · 💀) - Cryptocurrency launched in 2022and operates on the.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/synapsecns">SYN - Synapse</a></b> (🥉10 ·  ⭐ 66) - Cross-chain layer protocol powering frictionless interoperability.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/securesecrets">SHD - ShardingDAO</a></b> (🥉10 ·  ⭐ 60) - Array of connected privacy-preserving DeFi applications built on..
- <b><a href="https://github.com/Scorum">SCR - Scorum Coins</a></b> (🥉10 ·  ⭐ 57 · 💤) - Cryptocurrency . Scorum Coins has a current supply of..
- <b><a href="https://github.com/arkadiko-dao">DIKO - Arkadiko</a></b> (🥉10 ·  ⭐ 54) - Cryptocurrency launched in 2021. Arkadiko Finance has a current supply..
- <b><a href="https://github.com/pixiechain">PIX - Pixie</a></b> (🥉10 ·  ⭐ 50 · 💤) - Pixie is THE WORLDS FIRST FULLY FUNCTIONAL #crypto-based photo and..
- <b><a href="https://github.com/coschain">COS - Contentos</a></b> (🥉10 ·  ⭐ 47) - Decentralized global content ecosystem invested by Binance Labs.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/mithraiclabs">PSY - PsyOptions</a></b> (🥉10 ·  ⭐ 46) - American style options protocol built on the Solana blockchain... <code><img src="https://git.io/J9cOa" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/leisuremeta">LM - LeisureMeta</a></b> (🥉10 ·  ⭐ 45) - LeisureMetaverse issued its own utility token, LM token to.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/kifoundation">XKI - KI</a></b> (🥉10 ·  ⭐ 42) - Ki Foundations mission is about bridging the gap between CeFi and DeFi... <code><img src="https://cosmos.network/favicon.ico" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/Ankr-network">ANKR</a></b> (🥉10 ·  ⭐ 40) - Ankr is building the future of decentralized infrastructure and multi-chain.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/artgobblers">GOO - Art Gobblers Goo</a></b> (🥉9 ·  ⭐ 410) - Primary token used in the game Gooeys! Gooeys is a.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOx" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/sparkleloyalty">SPRKL - Sparkle Loyalty</a></b> (🥉9 ·  ⭐ 380 · 💀) - Cryptocurrency launched in 2017and operates on the.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/wonderland-money">WMEMO - Wonderful Memories</a></b> (🥉9 ·  ⭐ 340) - DeFi protocol focused on venture capital investments,.. <code><img src="https://git.io/J9cOb" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/alchemistcoin">MIST - Alchemist</a></b> (🥉9 ·  ⭐ 260 · 💀) - S one and only purpose is to find the philosophers.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/sudoswap">SUDO - sudoswap</a></b> (🥉9 ·  ⭐ 250) - Governance token of the sudoAMM protocol, a concentrated liquidity AMM..
- <b><a href="https://github.com/bitindi">BNI - Bitindi Chain</a></b> (🥉9 ·  ⭐ 210 · 🐣) - Decentralized, high-efficiency, and energy-saving.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/armors">ARM - Armours</a></b> (🥉9 ·  ⭐ 190 · 💀) - It is claimed that Armors is a blockchain security laboratory.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/faircrypto">XEN - XEN Crypto</a></b> (🥉9 ·  ⭐ 180 · 🐣) - ERC-20 token built on the Ethereum blockchain. Its based on.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/stakewise">SWISE - StakeWise</a></b> (🥉9 ·  ⭐ 110) - Liquid Ethereum staking protocol that tokenizes users deposits.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/kinecosystem">KIN</a></b> (🥉9 ·  ⭐ 96) - Kin is money for the digital world. It is an open micro-transaction platform used.. <code><img src="https://git.io/J9cOa" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/defis-net">DFS - DFSCoin</a></b> (🥉9 ·  ⭐ 95 · 💀) - Cryptocurrency . Users are able to generate DFS through the process..
- <b><a href="https://github.com/cherrynetwork">CHER - Cherry Network</a></b> (🥉9 ·  ⭐ 79) - Decentralized Autonomous Organization developing a.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/linkpoolio">LPL - LinkPool</a></b> (🥉9 ·  ⭐ 78 · 💀) - Leading Chainlink node service provider with the goal of.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/gscsocial">GSC - Global Social Chain</a></b> (🥉9 ·  ⭐ 76 · 💀) - Gunstar Metaverse uses a 2 tokens system, including.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/openleveragedev">OLE - OpenLeverage</a></b> (🥉9 ·  ⭐ 71) - Permissionless margin trading protocol currently deployed on.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/verynifty">MUSE</a></b> (🥉9 ·  ⭐ 67 · 🐣) - Cryptocurrency and operates on the Ethereum platform. Muse has a current.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/ultiledger">ULT - Ultiledger</a></b> (🥉9 ·  ⭐ 56 · 💀) - Shardus is building distributed ledger software to remedy.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/zenprotocol">ZP - Zen Protocol</a></b> (🥉9 ·  ⭐ 56) - Cryptocurrency . Users are able to generate ZP through the process..
- <b><a href="https://github.com/mfactory-lab">JSOL - JPool</a></b> (🥉9 ·  ⭐ 54) - Solana Stake Pool token issued by JPool. It represents the holders.. <code><img src="https://git.io/J9cOa" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/poseidon-network">QQQ - Poseidon Network</a></b> (🥉9 ·  ⭐ 45 · 💀) - We provide decentralized CDN service, the.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/VeriBlock">VBK - VeriBlock</a></b> (🥉9 ·  ⭐ 36) - VeriBlock and its novel Proof-of-Proof (PoP) consensus protocol is.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/koinos">KOIN - Koinos</a></b> (🥉9 ·  ⭐ 32) - KOIN will be the cryptocurrency that powers Koinos; a high.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/decentr-net">DEC - Decentr</a></b> (🥉9 ·  ⭐ 32) - Decentr is allows users to generate, reuse and exchange high quality.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/xtblock">XTT-B20 - XTblock</a></b> (🥉8 ·  ⭐ 1.1K · 💀) - A hyper-capable network, XTblock aims to deliver not only.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/manifoldfinance">FOLD - Manifold Finance</a></b> (🥉8 ·  ⭐ 510) - Manifold Finance provides solutions encompassing the.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/mydexchain">DXC - Dex-Trade Coin</a></b> (🥉8 ·  ⭐ 300) - DXC is not just a coin, it is a backbone payment unit of.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/essentiaone">ESS - Essentia</a></b> (🥉8 ·  ⭐ 120 · 💀) - New governance token in the Empty Set ecosystem. This token.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/ttceco">MARO</a></b> (🥉8 ·  ⭐ 120 · 💀) - Maro provides an efficient easy-to-use platform for value exchange and.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/0xmons">XMON</a></b> (🥉8 ·  ⭐ 110) - NFT platform for summoning neural net generated pixel monsters. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/cryptotwenty">C20 - CRYPTO20</a></b> (🥉8 ·  ⭐ 100 · 💀) - Cryptocurrency and operates on the Ethereum platform. CRYPTO20.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/sukhavati-labs">SKT - Sukhavati Network</a></b> (🥉8 ·  ⭐ 97 · 💤) - Decentralized cloud service network focused on.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/charged-particles">IONX - Charged Particles</a></b> (🥉8 ·  ⭐ 94) - Charged Particles Charged Particles is a protocol that.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/polkafoundry">PKF - Firebird</a></b> (🥉8 ·  ⭐ 92 · 💀) - What Is PolkaFoundry (PKF)? PolkaFoundry is a platform for.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/NerveNetwork">NVT - NerveNetwork</a></b> (🥉8 ·  ⭐ 87) - Decentralized digital asset service network based on the NULS.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/buddy-works">BUD - BunnyDucky</a></b> (🥉8 ·  ⭐ 87 · 💀) - Platform that tokenizes DeFi strategies as yield-bearing.. <code><img src="https://git.io/J9cOa" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/nftfy">NFTFY</a></b> (🥉8 ·  ⭐ 84) - Decentralized Application that provides the fractionalization of Non-.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/smart-chain-fr">CLAP - Cardashift</a></b> (🥉8 ·  ⭐ 84) - Cryptocurrency launched in 2022and operates on the Cardano platform...
- <b><a href="https://github.com/web3gamesofficial">W3G</a></b> (🥉8 ·  ⭐ 83) - Gaming coin, thus, it supports the gaming industry and wants to take it to the next..
- <b><a href="https://github.com/clover-network">CLV - Clover Finance</a></b> (🥉8 ·  ⭐ 82 · 💀) - Substrate-based specialized Layer 1 chain that is EVM.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/denaro-coin">WDNR - Wrapped Denaro</a></b> (🥉8 ·  ⭐ 79) - Decentralized cryptocurrency written in Python which uses..
- <b><a href="https://github.com/HydroBlockchain">HYDRO</a></b> (🥉8 ·  ⭐ 66 · 💀) - Hydro enables new and existing private systems to seamlessly integrate and.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/internet-of-people">HYD - Hydraledger</a></b> (🥉8 ·  ⭐ 57 · 💀) - Cryptocurrency . Users are able to generate HYD through the.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/railgun-privacy">RAIL - Railgun</a></b> (🥉8 ·  ⭐ 51) - RAILGUN secures privacy for DEX trading and lending. Built without.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/fair-exchange">SAFE - SafeCoin</a></b> (🥉8 ·  ⭐ 44 · 💤) - SafeCoin leverages the most advanced safety and privacy based..
- <b><a href="https://github.com/tempus-finance">TEMP - Tempus</a></b> (🥉8 ·  ⭐ 44) - Tempus is building the most important blocks of DeFi and Web3 in a.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/rigelprotocol">RGP - Rigel Protocol</a></b> (🥉8 ·  ⭐ 43) - The Rigel Protocol will be built on both Binance Smart.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/simplechain-org">SIMPLE - SimpleChain</a></b> (🥉8 ·  ⭐ 38 · 💀) - Cryptocurrency launched in 2019. SimpleChain has a current..
- <b><a href="https://github.com/nation3">NATION - Nation3</a></b> (🥉8 ·  ⭐ 32) - Community movement towards creating a sovereign nation on the.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/Arianee">ARIA20 - Arianee</a></b> (🥉8 ·  ⭐ 31) - Consumption behaviors are changing, new generations have a.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/projectchicago">GST2 - Gas Token Two</a></b> (🥉7 ·  ⭐ 680 · 💀) - Cryptocurrency launched in 2017and operates on the.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/ipilabs">IPISTR - IPI Shorter</a></b> (🥉7 ·  ⭐ 200) - 100% on-chain infrastructure from IPI Labs. It comprises.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/cruzbit">CRUZ - Cruzbit</a></b> (🥉7 ·  ⭐ 130 · 💀) - New decentralized peer-to-peer ledger designed to be as simple as..
- <b><a href="https://github.com/abyssfinance">ABYSS</a></b> (🥉7 ·  ⭐ 100 · 💀) - Abyss.Finance provides Decentralized (DeFi) and Centralized (CeFi) Finance.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/sealchain-project">SEAL - Seal Network</a></b> (🥉7 ·  ⭐ 92 · 💀) - Cryptocurrency and operates on the Ethereum platform... <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/risedle">$ETHRISE - ETHRISE</a></b> (🥉7 ·  ⭐ 75) - THRISE is ERC20 tokens that represent 1.7x-2.5x long exposure.. <code><img src="https://git.io/J9cOh" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/kyvenetwork">KYVE - Kyve Network</a></b> (🥉7 ·  ⭐ 69 · 💀) - No description.
- <b><a href="https://github.com/unitedtraders">UTT - United Traders Token</a></b> (🥉7 ·  ⭐ 66) - United Traders, found online at UnitedTraders.com, is.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/notional-finance">NOTE - DNotes</a></b> (🥉7 ·  ⭐ 57) - First decentralized, Ethereum-based protocol for borrowing and.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/team-exor">EXOR</a></b> (🥉7 ·  ⭐ 49) - Cryptocurrency . EXOR has a current supply of 0. The last known price of EXOR is..
- <b><a href="https://github.com/sirin-labs">SRN - SIRIN LABS Token</a></b> (🥉7 ·  ⭐ 45 · 💀) - Crypto token developed by blockchain development.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/flare-foundation">FLR - Flare Network</a></b> (🥉7 ·  ⭐ 43) - Blockchain for building applications that are interoperable with..
- <b><a href="https://github.com/zeta-chain">ZETA - ZetaChain</a></b> (🥉7 ·  ⭐ 38) - What Is ZetaChain (ZETA)? ZetaChain is a novel L1 that has chain-..
- <b><a href="https://github.com/raid-guild">RAID - Ancient Raid</a></b> (🥉7 ·  ⭐ 37 · 💤) - Virtual universe of fantasy heroes for Play-To-Earn.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/bismuthfoundation">BIS - Bismuth</a></b> (🥉7 ·  ⭐ 36) - Bismuth, a digital distributed self-regulating database system.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/2key">2KEY - 2key.network</a></b> (🥉7 ·  ⭐ 36 · 💀) - Protocol: 2nd Layer Protocol Embedding Smart Contracts.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/ajuna-network">BAJU - Ajuna Network</a></b> (🥉7 ·  ⭐ 36 · 💀) - Cryptocurrency launched in 2022. Bajun Network has a current..
- <b><a href="https://github.com/GenaroNetwork">GNX - Genaro Network</a></b> (🥉7 ·  ⭐ 33 · 💀) - First Turing Complete Public Chain with Decentralized.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/gyrostable">GYFI - Gyroscope</a></b> (🥉7 ·  ⭐ 31 · 💀) - ### What is GYFI? GYFI is Gyroscopes native and governance.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/skyward-finance">SKYWARD - Skyward Finance</a></b> (🥉6 ·  ⭐ 780 · 💀) - Fully permissionless open sourced launchpad that..
- <b><a href="https://github.com/bc-game-project">JB - Janta Bazar Coin</a></b> (🥉6 ·  ⭐ 200 · 💀) - Janta Bazar Coin aims to provide user-friendly, efficient,..
- <b><a href="https://github.com/colored-coins">CLN - Colu Local Network</a></b> (🥉6 ·  ⭐ 120 · 💀) - Cryptocurrency and operates on the Ethereum platform... <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/WrappedBTC">WBTC - Wrapped Bitcoin</a></b> (🥉6 ·  ⭐ 110) - Cryptocurrency and operates on the Ethereum platform... <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/cindicator">CND - Cindicator</a></b> (🥉6 ·  ⭐ 92 · 💀) - Founded in 2015, Cindicator builds analytical and trading.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/ongridsystems">CNP - Cryptonia Poker</a></b> (🥉6 ·  ⭐ 91 · 💀) - No description.
- <b><a href="https://github.com/scryptachain">LYRA - Lyra Finance</a></b> (🥉6 ·  ⭐ 88) - Open protocol for trading options built on Ethereum. Lyra.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/automata-network">ATA - Automata Network</a></b> (🥉6 ·  ⭐ 81 · 💤) - Decentralized service protocol that provides.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/l2labs">ZKS - ZKSpace</a></b> (🥉6 ·  ⭐ 75 · 💀) - Based on ZK-Rollups technology, L2 Labs has launched ZKSpace, a.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/PositionExchange">POSI - Position Token</a></b> (🥉6 ·  ⭐ 70) - The Next-Gen Decentralized Futures Trading Protocol. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/animeswap">ANI - Anime Token</a></b> (🥉6 ·  ⭐ 70 · 🐣) - We create professional ANIME artwork & NFTs. ANIME Token is.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/UlordChain">UT - Ulord</a></b> (🥉6 ·  ⭐ 64 · 💀) - Peer-to-peer value transfer public blockchain. Through building its.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/zincwork">ZINC</a></b> (🥉6 ·  ⭐ 56 · 💀) - Cryptocurrency and operates on the Ethereum platform. ZINC has a current.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/coinscope-co">XID - Sphere Identity</a></b> (🥉6 ·  ⭐ 46) - International Diamond (XID) is a cryptocurrency . Users are..
- <b><a href="https://github.com/narwallets">STNEAR - Staked NEAR</a></b> (🥉6 ·  ⭐ 43) - Cryptocurrency and operates on the Aurora platform. Staked NEAR..
- <b><a href="https://github.com/Switcheo">ZWAP - ZilSwap</a></b> (🥉6 ·  ⭐ 42 · 💀) - The DeFi hub of Zilliqa.
- <b><a href="https://github.com/hyperion-hyn">HYN - Hyperion</a></b> (🥉6 ·  ⭐ 39 · 💀) - Cryptocurrency launched in 2018. Hyperion has a current supply.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/bohrweb">BR - BoHr</a></b> (🥉6 ·  ⭐ 34 · 💤) - Cryptocurrency launched in 2020and operates on the BNB Smart Chain.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/apex-protocol">APEX - ApeX Protocol</a></b> (🥉6 ·  ⭐ 34 · 💤) - ApeX Protocol (APEX) Decentralized Derivatives Protocol.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/cryptodemonz-github">LLTH - Lilith Swap</a></b> (🥉6 ·  ⭐ 34 · 💀) - Baseline ecosystem for a bridge between L1 Ethereum.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/dawn-protocol">DAWN - Dawn Protocol</a></b> (🥉6 ·  ⭐ 33 · 💀) - Open-source protocol for gaming and competitions. Its.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/0xguard-com">TROLLER - The Troller Coin</a></b> (🥉6 ·  ⭐ 33) - First competition NFT Marketplace , Tournament gaming.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/banksea-finance">KSE - Banksea</a></b> (🥉6 ·  ⭐ 32 · 💀) - Banksea works to address issues in NFT collateral lending, such as a..
- <b><a href="https://github.com/team-kujira">KUJI - Kujira</a></b> (🥉6 ·  ⭐ 32) - Kujira is committed to levelling the playing field in decentralized.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/moontography">MTGY - Moontography</a></b> (🥉5 ·  ⭐ 160 · 💀) - Cryptocurrency launched in 2021and operates on the BNB.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/dvpnet">DVP - Decentralized Vulnerability Platform</a></b> (🥉5 ·  ⭐ 93 · 💀) - DVP (Decentralized Vulnerability Platform) is an.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/AngleProtocol">ANGLE</a></b> (🥉5 ·  ⭐ 62) - Governance token of the Angle Protocol. The Angle Protocol is a decentralized.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/VideoCoin">VID - Vivid Labs</a></b> (🥉5 ·  ⭐ 50 · 💀) - The VID token powers VIVID (formerly VideoCoin Network), the.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/MetisProtocol">METIS - Metis Token</a></b> (🥉5 ·  ⭐ 49 · 💤) - Based on the spirit of Optimistic Rollup, Metis is.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/potcoin">POT - PotCoin</a></b> (🥉5 ·  ⭐ 47 · 💀) - Digital cryptocurrency aimed to empower, secure, and facilitate.. <code><img src="https://git.io/J9cOx" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/marinade-finance">MNDE - Marinade</a></b> (🥉5 ·  ⭐ 42) - Governance token for Marinade. Marinade helps you to stake Solana.. <code><img src="https://git.io/J9cOa" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/agenorcore">AGE - Agenor</a></b> (🥉5 ·  ⭐ 39 · 💀) - Agenor aims to drive the long-awaited mass adoption of..
- <b><a href="https://github.com/secretblockchain">SIE - Secret</a></b> (🥉5 ·  ⭐ 35 · 💀) - Cryptocurrency launched in 2018and operates on the Ethereum.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/SwingbyProtocol">SWINGBY</a></b> (🥉5 ·  ⭐ 31 · 💀) - A fast proof-of-stake protocol for inter-blockchain swaps, allowing.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code> <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/coinversation">CTO - Crypto</a></b> (🥉4 ·  ⭐ 310 · 💤) - Cryptocurrency . Crypto has a current supply of 13,742,738.4040553...
- <b><a href="https://github.com/oneswap">ONES - OneSwap DAO Token</a></b> (🥉4 ·  ⭐ 120 · 💀) - A fully decentralized exchange protocol on Smart.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/oilernetwork">OIL - Oiler</a></b> (🥉4 ·  ⭐ 88 · 💤) - Cryptocurrency and operates on the Arbitrum platform. Petroleum OIL.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/JustLiquidity">JULD - JulSwap</a></b> (🥉4 ·  ⭐ 87 · 💀) - Community Token from JulSwap which allows you to participate in.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/cre8rdao">CRE8R - CRE8R DAO</a></b> (🥉4 ·  ⭐ 84) - CRE8R DAO was founded in June 2021 by a group of DeFi content.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/cycloneprotocol">CYC - Cyclone Protocol</a></b> (🥉4 ·  ⭐ 56 · 💀) - Multi-chain, non-custodial, privacy-preserving.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/universexyz">XYZ - Universe.XYZ</a></b> (🥉4 ·  ⭐ 48 · 💀) - Initiative, which grants voting rights to the $XYZ holders.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/ray-network">XRAY - Ray Network</a></b> (🥉4 ·  ⭐ 41 · 💀) - Cryptocurrency launched in 2021and operates on the Cardano..
- <b><a href="https://github.com/vntchain">VNT - VNT Chain</a></b> (🥉4 ·  ⭐ 35 · 💀) - Cryptocurrency and operates on the Ethereum platform. VNT.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/etherorcsofficial">ZUG</a></b> (🥉4 ·  ⭐ 35) - Cryptocurrency launched in 2021and operates on the Ethereum platform. ZUG has a.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/cVault-finance">CORE - cVault.finance</a></b> (🥉4 ·  ⭐ 34 · 💤) - Non-inflationary cryptocurrency that is designed to.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/xdao-app">XDAO</a></b> (🥉4 ·  ⭐ 34) - What is XDAO? XDAO was created with the idea to make DAOs the place for crypto.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/pantherswap">PANTHER - PantherSwap</a></b> (🥉4 ·  ⭐ 33 · 💀) - Cryptocurrency launched in 2021and operates on the BNB.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/xxfoundation">XX - XX Network</a></b> (🥉4 ·  ⭐ 33 · 🐣) - Xx network (xx)? The xx network is a full-stack platform which..
- <b><a href="https://github.com/titano-finance">TITANO</a></b> (🥉4 ·  ⭐ 32 · 💀) - Titano is launching the new frontier of staking tokens which will.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/acria-network">ACRIA - Acria.AI</a></b> (🥉4 ·  ⭐ 31 · 💀) - Cryptocurrency launched in 2022and operates on the Ethereum.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/mediafoundation">MEDIA - Media Network</a></b> (🥉3 ·  ⭐ 76) - New protocol that bypasses traditional CDN providers.. <code><img src="https://git.io/J9cOa" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/metacartel">METACARTEL - Metacartel Ventures</a></b> (🥉3 ·  ⭐ 63 · 💀) - Permissioned, for-profit investment DAO coupled with..
- <b><a href="https://github.com/Steemhunt">HUNT</a></b> (🥉3 ·  ⭐ 56 · 💤) - Gamified lifestyle DApp network for digital nomads all around the world. It.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/BNBPay">BPAY - BNBPay</a></b> (🥉3 ·  ⭐ 42 · 💀) - BeyondPay aims to become the next web3 point of sale.. <code><img src="https://git.io/J9cOd" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/solanium-io">SLIM - Solanium</a></b> (🥉3 ·  ⭐ 40 · 💀) - Launch Pad Project (On solana) Put them on LaunchPad Category. <code><img src="https://git.io/J9cOa" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/getprotocol">GET - GET Protocol</a></b> (🥉3 ·  ⭐ 31 · 💤) - The future of ticketing. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/neardefi">BRRR - Burrow</a></b> (🥉3 ·  ⭐ 31 · 💤) - Cryptocurrency launched in 2022. Burrow has a current supply of..
- <b><a href="https://github.com/thinkiumgroup">TKM - Thinkium</a></b> (🥉2 ·  ⭐ 100 · 💀) - Cryptocurrency launched in 2017. Thinkium has a current supply of..
- <b><a href="https://github.com/mylivn-gmbh">MLVC - Mylivn Coin</a></b> (🥉2 ·  ⭐ 52 · 💀) - ## What is Mylivn Coin (MLVC) ? Mylivn Coin (MLVC) is a token..
- <b><a href="https://github.com/entropyfi">ERP - Entropyfi</a></b> (🥉2 ·  ⭐ 37 · 💀) - Lossless prediction market providing user a safer platform to.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/vitaeteam">VITAE</a></b> (🥉1) - Cryptocurrency launched in 2014. Users are able to generate VITAE through the process of..
- <b><a href="https://github.com/umee-network">UMEE</a></b> (🥉1) - Cross chain DeFi hub that interconnects between blockchains. As a base layer DeFi.. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/blur-network">BLUR</a></b> (🥉1) - The NFT marketplace for pro traders.
- <b><a href="https://github.com/minima-global">MINIMA</a></b> (🥉1) - No description.
- <b><a href="https://github.com/naboxwallet">NABOX</a></b> (🥉1) - Cross-Chain DeFi Wallet with Decentralized Identifier built for Web 3.0. <code><img src="https://git.io/J9cO9" style="display:inline;" width="13" height="13"></code>
- <b><a href="https://github.com/kwenta">KWENTA</a></b> (🥉1) - Decentralized derivatives trading platform, live on Optimism, offering real-world and on-..
- <b><a href="https://github.com/canto-network">CANTO</a></b> (🥉1) - Canto presents the radical notion that core DeFi primitives should exist as Free Public..
</details>

---

## Related Resources

- [**Best-of lists**](https://best-of.org): Discover other best-of lists with awesome open-source projects on all kinds of topics.
- [**CoinGecko**](https://coingecko.com): World’s largest independent crypto data aggregator.
- [**CoinMarketCap**](https://coinmarketcap.com): World's most-referenced price-tracking website for cryptoassets.

## Contribution

Contributions are encouraged and always welcome! If you like to add or update projects, choose one of the following ways:

- Open an issue by selecting one of the provided categories from the [issue page](https://github.com/LukasMasuch/best-of-crypto/issues/new/choose) and fill in the requested information.
- Modify the [projects.yaml](https://github.com/LukasMasuch/best-of-crypto/blob/main/projects.yaml) with your additions or changes, and submit a pull request. This can also be done directly via the [Github UI](https://github.com/LukasMasuch/best-of-crypto/edit/main/projects.yaml).

If you like to contribute to or share suggestions regarding the project metadata collection or markdown generation, please refer to the [best-of-generator](https://github.com/best-of-lists/best-of-generator) repository. If you like to create your own best-of list, we recommend to follow [this guide](https://github.com/best-of-lists/best-of/blob/main/create-best-of-list.md).

For more information on how to add or update projects, please read the [contribution guidelines](https://github.com/LukasMasuch/best-of-crypto/blob/main/CONTRIBUTING.md). By participating in this project, you agree to abide by its [Code of Conduct](https://github.com/LukasMasuch/best-of-crypto/blob/main/.github/CODE_OF_CONDUCT.md).

## License

[![CC0](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-sa.svg)](https://creativecommons.org/licenses/by-sa/4.0/)
